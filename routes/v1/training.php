<?php

use App\Http\Controllers\Api\V1\TrainingController;
use Illuminate\Support\Facades\Route;

Route::name('trainings.')
    ->controller(TrainingController::class)
    ->group(function () {
        Route::post('create-training', 'create')->name('create');
        Route::put('update-training/{training}', 'update');
        Route::put('update-status-training', 'updateStatus')->name('update.status');
        Route::get('my-trainings', 'getMyTrainings')->name('my-trainings');
        Route::get('training-schedule', 'getTimesToAllTrainingsScheduled')->name('schedule');
        Route::get('training-metadata', 'getAllTrainingsMetaData')->name('metadata');
        Route::get('stages', 'getStages')->name('stages');
        Route::get('training/{training}', 'getInfoTraining')->name('info');
    });
