<?php

use App\Http\Controllers\Api\V1\TraineeController;
use Illuminate\Support\Facades\Route;

Route::get('trainees', [TraineeController::class, 'index']);
Route::post('create-trainee', [TraineeController::class, 'create']);
Route::post('update-trainee/{trainee}', [TraineeController::class, 'update']);
Route::delete('delete-trainee/{trainee}', [TraineeController::class, 'destroy']);
Route::post('sync-trainee', [TraineeController::class, 'syncTrainee']);
