<?php

use App\Http\Controllers\Api\V1\UserController;
use Illuminate\Support\Facades\Route;

Route::post('create-capturer', [UserController::class, 'createCapturerOrAdmin']);
Route::get('users', [UserController::class, 'indexByRole']);
Route::get('user/{user}', [UserController::class, 'getUserInfo']);
Route::post('create-trainer', [UserController::class, 'createTrainer']);
Route::Post('update-trainer/{user}', [UserController::class, 'updateTrainer']);
Route::Post('update-capturer/{user}', [UserController::class, 'updateCapturerOrAdmin']);
Route::delete('delete/{user}', [UserController::class, 'delete']);
Route::post('sync-user', [UserController::class, 'syncUser']);
Route::get('sync-user-sites', [UserController::class, 'syncUserSites']);
Route::get('sync-user-departments', [UserController::class, 'syncUserDepartments']);
Route::get('sync-user-skills', [UserController::class, 'syncUserSkills']);
Route::get('sync-user-read', [UserController::class, 'syncUsersRead']);
