<?php

use App\Http\Controllers\Api\V1\LRSController;
use App\Http\Controllers\Api\V1\RoleController;
use App\Http\Controllers\Api\V1\SiteCompanyDepartmentController;
use Illuminate\Support\Facades\Route;

Route::get('/companies', [SiteCompanyDepartmentController::class, 'getCompanies']);
Route::get('/companies/{companyId}/sites', [SiteCompanyDepartmentController::class, 'getSitesByCompany']);

Route::post('/media-upload', [\App\Http\Controllers\Api\V1\MediaController::class, 'mediaUpload']);
Route::get('validations-setting', function () {
    return \Response::success(__('validation.info_validations'), [
        'max_keypoint_attachments_count' => config('app.max_keypoint_attachments_count'),
        'auto_sync_time_by_minutes' => config('app.auto_sync_time_by_minutes')
    ]);
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('roles', [RoleController::class, 'index']);

    Route::group([], function () {
        require __DIR__ . '/department.php';
    });

    Route::group([], function () {
        require __DIR__ . '/skill.php';
    });

    Route::group([], function () {
        require __DIR__ . '/user.php';
    });

    Route::group([], function () {
        require __DIR__ . '/training.php';
    });

    Route::group([], function () {
        require __DIR__ . '/trainee.php';
    });

    Route::group([], function () {
        require __DIR__ . '/trainer.php';
    });

    Route::post('/createlrs', [LRSController::class, 'store']);
});

Route::group([
    'prefix' => 'oauth',
], function () {
    require __DIR__ . '/auth.php';
});
