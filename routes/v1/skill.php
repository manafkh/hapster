<?php

use App\Http\Controllers\Api\V1\SkillController;
use Illuminate\Support\Facades\Route;

Route::name('skills.')
    ->controller(SkillController::class)
    ->group(function () {
        Route::get('skills', 'index')->name('index');
        Route::post('create-skill', 'create');
        Route::get('get-skill/{skill}', 'getSkill')->name('show');
        Route::post('update-skill/{skill}', 'update');
        Route::delete('skills/{skill}', 'destroy');
        Route::get('accessible-skills', 'getAccessibleSkills')->name('accessible');
        Route::get('other-skills', 'getOtherSkill');
        Route::get('admin-skills', 'getAdminSkills');
        Route::get('skills-without-departments', 'getSkillsAdminWithoutDepartments');
        Route::get('trainer-skills', 'getTrainerSkills');
        Route::get('translate-skill', 'translateSkill');
        Route::post('sync-skills', 'syncSkills');
        Route::post('share-skills/{fromDepartment}/{toDepartment}', 'shareSkills');
        Route::post('/import', 'importSkills');
    });
