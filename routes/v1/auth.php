<?php

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\PasswordController;
use App\Http\Controllers\Api\V1\RegistryController;
use Illuminate\Support\Facades\Route;

Route::prefix('user')
    ->name('user.')
    ->group(function () {
        Route::post('register', [AuthController::class, 'register'])->name('register');
        Route::post('login', [AuthController::class, 'login'])->name('login');
        Route::post('refresh', [AuthController::class, 'refreshToken']);
        Route::post('send-verify-email', [AuthController::class, 'sendVerificationEmail'])
            ->name('send-verification-email');
        Route::post('verify-email', [AuthController::class, 'verifyEmail'])
            ->name('verify-email');
        Route::group(['middleware' => 'auth:api'], function () {
            Route::post('logout', [AuthController::class, 'logout']);
        });

        Route::prefix('registries')
            ->name('registries.')
            ->controller(RegistryController::class)
            ->withoutMiddleware('auth.registry.token.validate')
            ->group(function () {
                Route::post('token/generate', 'generateToken')->name('generate-token');
                Route::post('token/refresh', 'refreshToken')->name('refresh-token');
            });

        Route::post('password/email', [PasswordController::class, 'sendResetLinkEmail'])
            ->name('password.reset-email.send');
        Route::post('change-password', [PasswordController::class, 'changePassword']);
        Route::post('{user}/change-password-by-admin', [PasswordController::class, 'changePasswordByAdmin']);

    });

Route::get('password/reset/{token}', [PasswordController::class, 'showResetForm'])
    ->name('password.reset')
    ->middleware('web');

Route::post('password/reset', [PasswordController::class, 'reset'])
    ->name('password.update')
    ->middleware('web');
