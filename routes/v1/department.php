<?php

use App\Http\Controllers\Api\V1\DepartmentController;
use Illuminate\Support\Facades\Route;

Route::name('departments.')
    ->controller(DepartmentController::class)
    ->group(function () {
        Route::get('departments', 'index')->name('index');
        Route::get('departments-for-user', 'indexForUser')->name('index.role');
        Route::post('create-department', 'create')->name('create');
        Route::post('reorder-department', 'reorderDepartment')->name('reorder');
        Route::post('import-department', 'importDepartmentFromFile')->name('import');
        Route::post('update-department/{department}', 'update')->name('update');
        Route::delete('delete-department/{department}', 'destroy')->name('destroy');
        Route::post('sync-departments', 'syncDepartments');
    });
