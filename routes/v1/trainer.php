<?php

use App\Http\Controllers\Api\V1\TrainerController;
use Illuminate\Support\Facades\Route;

Route::get('trainers', [TrainerController::class, 'index']);
