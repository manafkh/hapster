<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Password Reset Successful</title>
    <!-- Include your CSS files here -->
</head>
<body>
    <div class="container">
        <h2>Password Reset Successful</h2>
        <p>Your password has been reset successfully. You may now close this window and proceed to login.</p>
    </div>
    <!-- Include your JavaScript files here -->
</body>
</html>

