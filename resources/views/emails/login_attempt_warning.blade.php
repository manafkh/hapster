<!DOCTYPE html>
<html>
<head>
    <title>Excessive Login Attempts</title>
</head>
<body>
    <p>
        Warning! Multiple unsuccessful login attempts detected on your account.
        If this wasn't you, please check your account security.
    </p>
</body>
</html>

