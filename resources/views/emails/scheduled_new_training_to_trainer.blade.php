<!DOCTYPE html>
<html>
<head>
    <style>
        /* Add your custom styles here */
    </style>
</head>
<body>
<h2>Dear {{ $user->username }} ,</h2>
<p>I trust you're doing well.
<p>
<p> As our scheduled training session approaches,</p>
<p> I wanted to send a reminder with the details:</p>
<p>Date: {{date('y/m/d',$training->date)}}</p>
<p>Time: {{date('H:i',$training->from)}} - {{date('H:i',$training->to)}}</p>
<p>Location: {{$training->skill->location}}</p>

<p>Your contributions have been invaluable, and I'm eagerly anticipating another insightful session.</p>
<p>If there are any changes to the schedule or specific resources you need beforehand, please let me know.</p>
<p> Additionally, should you have any preparatory materials for me to review, I'm all ears.</p>
<p>Thank you for your unwavering dedication.</p>
<p> I have every confidence that our upcoming training will be as enriching as our past sessions.</p>
<p>Looking forward to our collaboration.</p>
<p></p>Hapster</p>


<p>Warm regards,</p>
<p>{{config('app.name')}}</p>
<p>{{config('mail.from.address')}}</p>
</body>
</html>

