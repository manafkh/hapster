<!DOCTYPE html>
<html>
<head>
    <style>
        /* Add your custom styles here */
    </style>
</head>
<body>

<h3>Hello {{ $user->first_name }} ,</h3>
@if($hasCredentials)
    <p>We're delighted to have you on board with {{config('app.name')}}! An account has been created for you by our
        admin
        team.
        Your Login Credentials:</p>
    <p>Username: {{$user->username}}</p>
    <p>Password: {{$password}}</p>

    <ol>
        <li>Download the App</li>
        <li>Login with the provided credentials.</li>
        <li>Once you're in, explore the features and get acquainted with the platform .</li>
    </ol>
@endif
<p>Welcome aboard, and we hope you have a great experience with {{config('app.name')}} App!</p>

<p>Best regards,</p>
<p>{{config('app.name')}}</p>
<p>{{config('mail.from.address')}}</p>
</body>
</html>

