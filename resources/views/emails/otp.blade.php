<!DOCTYPE html>
<html>
<head>
    <style>
        /* Add your custom styles here */
    </style>
</head>
<body>
    <h2>Hapster One-Time Password (OTP) Verification</h2>
    <p>Hello {{ $user->name }},</p>
    <p>Your One-Time Password (OTP) for verification is:</p>
    <h3>{{ $otp }}</h3>
    <p>Please enter this OTP on the verification screen to complete your registration.</p>

    <p>If you didn't request this OTP, please ignore this email.</p>

    <p>Thank you!</p>
</body>
</html>

