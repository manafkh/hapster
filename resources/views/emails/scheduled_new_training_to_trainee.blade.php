<!DOCTYPE html>
<html>
<head>
    <style>
        /* Add your custom styles here */
    </style>
</head>
<body>
<h2>Dear {{ $user->first_name }} ,</h2>
<p>I hope this message finds you well.
    I'm excited to inform you about our upcoming training session! Here are the details:</p>
<p>Date: {{date('y/m/d',$training->date)}}</p>
<p>Time: {{date('H:i',$training->from)}} - {{date('H:i',$training->to)}}</p>
<p>Location: {{$training->skill->location}}</p>

<p>This session is part of our ongoing efforts to provide valuable learning experiences, </p>
<p>and I believe it will be a great opportunity for growth and development.</p>
<p>Please ensure you're well-prepared and punctual.</p>
<p>If you require any materials or resources beforehand, or if you have questions regarding the session, </p>
<p>do not hesitate to reach out.</p>
<p> Thank you for your dedication to learning and self-improvement. </p>
<p>I'm confident this training will be a significant step in your journey.</p>
<p> Looking forward to seeing you there.</p>


<p>Warm regards,</p>
<p>{{config('app.name')}}</p>
<p>{{config('mail.from.address')}}</p>
</body>
</html>

