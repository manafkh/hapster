#!/bin/bash
exec 3<>/dev/tcp/localhost/8080

echo -e "GET / HTTP/1.1\nHost: localhost:8080\n\n" >&3

timeout --preserve-status 1 cat <&3 | grep -m 1 "HTTP/1.1 200 OK"
ERROR=$?

exec 3<&-
exec 3>&-

exit $ERROR

