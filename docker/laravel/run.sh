#!/bin/sh

cd /var/www/html
chmod -R 777 .
yes | php artisan migrate --seed
php artisan storage:link
supervisord -c /etc/supervisor.d/supervisord.ini

