<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'failed_register_user' => 'Failed to register user.',
    'no_access_token' => 'Failed to get access token.',
    'wrong_otp_code' => 'Wrong OTP code.',

    'reset_token_invalid' => 'The provided reset token is invalid or has expired.',
    'not_find_user' => 'We can\'t find a user with that email address.',
    'reset_password_failed' => 'Password reset failed.',
    'email_or_username_not_found' => 'email or username not found',
    
    'invalid_credentials_for_auth' => 'Invalid Credentials For Auth',
    'generate_unique_token_error' => 'Error on generating token, please try again later',
    'device_not_found' => 'Device not found',
    'invalid_registry_token' => 'Invalid Registry token',
    'refresh_token_not_found' => 'Refresh token not found',
    'check_verify_email' => 'This account is not verified, Please check your email for the OTP code we sent.',
    'invalid_license' => 'Invalid license',
    'already_verified' => 'User is already verified',

    'success_message' => [
        'register_successfully' => 'register new user successfully',
        'login_successfully' => 'Login Successfully',
        'logout_successfully' => 'Logout Successfully',
        'refresh_token_successfully' => 'refresh Token Successfully',
        'password_change_successfully' => 'Password changed successfully.',
        'email_verified' => 'email is verified successfully.',
        'send_verify_email' => 'send verify email successfully',
        'send_reset_email' => 'send verify email successfully',
    ],

];
