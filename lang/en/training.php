<?php

return [

    'success_message' => [

        'create_successfully' => 'Training Created Successfully',
        'update_status_successfully' => 'Training Status Updated Successfully',
        'update_successfully' => 'Training Updated Successfully',
        'get_my_trainings' => 'This Is The List Of All Trainings Belong To You',
        'get_times_to_all_trainings_scheduled' => 'This Is The List Of All Trainings The Time Scheduled',
        'get_all_trainings_metadata' => 'This Is The List Of All Trainings Metadata',
        'get_info_training' => 'This Is The Training Info',
        'get_stages' => 'This Is The List Of All Stages',

    ],

];
