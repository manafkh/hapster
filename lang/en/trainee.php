<?php

return [

    'success_message' => [
        'index' => 'This Is The List Of All Trainees',
        'create_successfully' => 'Trainee Created Successfully',
        'update_successfully' => 'Trainee Updated Successfully',
        'destroy_successfully' => 'Trainee Deleted Successfully',
        'sync_successfully' => 'Trainee Sync Successfully',
    ],
];
