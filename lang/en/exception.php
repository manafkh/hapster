<?php

return [

    'check_media_uploaded' => 'check media uploaded',
    'unauthorized' => 'you have no permission to admit this action',
    'model_not_found' => 'Item Not Found',
    'no_authenticate_user' => 'you have to login first',
    'unauthorized_user' => 'the token is expired',
    'client_error_keycloak' => 'client error in keycloak',
    'server_error_keycloak' => 'server error in keycloak',
];
