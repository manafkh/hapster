<?php

return [

    'success_message' => [

        'index' => 'This Is The List Of All Skills',
        'create_successfully' => 'Skill Created Successfully',
        'update_successfully' => 'Skill Updated Successfully',
        'destroy_successfully' => 'Skill Deleted Successfully',
        'share_skills_successfully' => 'Skill Shared Successfully',
        'get_skill' => 'This Is The Skill Info',
        'get_trainer_skills' => 'This Is The List Of All Skills The Trainer Can Access',
        'get_accessible_skills' => 'This Is The List Of All Skills The User Can Access',
        'get_other_skill' => 'This Is The List Of All Skills Can\'t Access And Belong To His Department',
        'get_admin_skills' => 'This Is The List Of All Skills The Admin Can Access By His Site',
        'get_skills_admin_without_departments' => 'This Is The List Of All Skills With Out Departments',
        'sync_successfully' => 'skill Sync Successfully',

    ],

];
