<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cant_use_as_trainer' => 'You cannot use this form to add Trainer.',
    'must_be_trainer' => 'You must select trainer role.',

    'success_message' => [
        'index_role' => 'This Is The List Of All Roles',
        'index_by_role' => 'This Is The List Of User Grouping BY Role',
        'get_user_info' => 'This Is User Info.',
        'create_successfully' => 'User Created Successfully',
        'update_successfully' => 'User Updated Successfully',
        'destroy_successfully' => 'User Deleted Successfully.',
        'sync_successfully' => 'User Sync Successfully.',

        'sync_user_sites' => 'his sites belongs To user',
        'sync_user_departments' => 'this departments belongs To user',
        'sync_user_skills' => 'this skills belongs To user',
        'sync_users_read' => 'this Users belongs To sites',
    ],

];
