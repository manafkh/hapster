<?php

return [

    'success_message' => [
        'index' => 'This Is The List Of All Departments',
        'index_for_user_' => 'This Is The List Of All Departments Fo User',
        'create_successfully' => 'Department Created Successfully',
        'update_successfully' => 'Department Updated Successfully',
        'reorder_department_successfully' => 'Reorder Department Successfully.',
        'import_department_from_file_successfully' => 'Department Imported From File Successfully.',
        'destroy_successfully' => 'Department Deleted Successfully',
        'sync_successfully' => 'Department Sync Successfully',
    ],

];
