<?php

return [

    'success_message' => [
        'getCompanies' => 'This Is The List Of All Companies',
        'getSitesByCompany' => 'This Is The List Of All Sites By Company',
    ],
];
