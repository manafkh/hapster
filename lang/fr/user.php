<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cant_use_as_trainer' => 'Vous ne pouvez pas utiliser ce formulaire pour ajouter un formateur.',
    'must_be_trainer' => 'Vous devez sélectionner le rôle de formateur.',

    'success_message' => [
        'index_role' => 'Ceci est la liste de tous les rôles',
        'index_by_role' => 'Ceci est la liste des utilisateurs regroupés PAR rôle',
        'get_user_info' => 'Ceci est une information utilisateur.',
        'create_successfully' => 'Utilisateur créé avec succès',
        'update_successfully' => 'Mise à jour de l\'utilisateur réussie',
        'destroy_successfully' => 'Utilisateur supprimé avec succès.',
        'sync_successfully' => 'Synchronisation de l\'utilisateur réussie.',

        'sync_user_sites' => 'ce site appartient à l\'utilisateur',
        'sync_user_departments' => 'Cette département appartient à l\'utilisateur',
        'sync_user_skills' => 'ces compétences appartiennent à l\'utilisateur',
        'sync_users_read' => 'Ces utilisateurs appartientnt aux sites.',
    ],
];
