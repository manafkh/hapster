<?php

return [

    'success_message' => [
        'index' => 'Voici la liste de tous les départements',
        'index_for_user_' => 'Voici la liste de tous les départements pour l\'utilisateur',
        'create_successfully' => 'Département créé avec succès',
        'update_successfully' => 'Département mis à jour avec succès',
        'reorder_department_successfully' => 'Réorganisation des départements réussie',
        'import_department_from_file_successfully' => 'Département importé avec succès',
        'destroy_successfully' => 'Département supprimé avec succès',
        'sync_successfully' => 'Synchronisation des départements réussie',
    ],

];
