<?php

return [

    'success_message' => [
        'index' => 'Voici la liste des apprenants',
        'create_successfully' => 'Apprenant créé avec succès',
        'update_successfully' => 'Apprenant mis à jour avec succès',
        'destroy_successfully' => 'Apprenant supprimé avec succès',
        'sync_successfully' => 'Synchronisation réussie des apprenants',
    ],
];
