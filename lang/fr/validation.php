<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    'accepted' => 'Le champ :attribute doit être accepté.',
    'active_url' => "Le champ :attribute n'est pas une URL valide.",
    'after' => 'Le champ :attribute doit être une date postérieure au :date.',
    'alpha' => 'Le champ :attribute doit seulement contenir des lettres.',
    'alpha_dash' => 'Le champ :attribute doit seulement contenir des lettres, des chiffres et des tirets.',
    'alpha_num' => 'Le champ :attribute doit seulement contenir des chiffres et des lettres.',
    'array' => 'Le champ :attribute doit être un tableau.',
    'before' => 'Le champ :attribute doit être une date antérieure au :date.',
    'between' => [
        'numeric' => 'La valeur de :attribute doit être comprise entre :min et :max.',
        'file' => 'Le fichier :attribute doit avoir une taille entre :min et :max kilobytes.',
        'string' => 'Le texte :attribute doit avoir entre :min et :max caractères.',
    ],
    'boolean' => 'Le champ :attribute doit être vrai ou faux.',
    'confirmed' => 'Le champ de confirmation :attribute ne correspond pas.',
    'current_password' => 'Le mot de passe est incorrect.',
    'date' => "Le champ :attribute n'est pas une date valide.",
    'date_format' => 'Le champ :attribute ne correspond pas au format :format.',
    'different' => 'Les champs :attribute et :other doivent être différents.',
    'digits' => 'Le champ :attribute doit avoir :digits chiffres.',
    'digits_between' => 'Le champ :attribute doit avoir entre :min and :max chiffres.',
    'email' => 'Le format du champ :attribute est invalide.',
    'exists' => 'Le champ :attribute sélectionné est invalide.',
    'file' => 'Le champ :attribute doit être un fichier.',
    'image' => 'Le champ :attribute doit être une image.',
    'in' => 'Le champ :attribute est invalide.',
    'integer' => 'Le champ :attribute doit être un entier.',
    'ip' => 'Le champ :attribute doit être une adresse IP valide.',
    'max' => [
        'numeric' => 'La valeur de :attribute ne peut être supérieure à :max.',
        'file' => 'Le fichier :attribute ne peut être plus gros que :max kilobytes.',
        'string' => 'Le texte de :attribute ne peut contenir plus de :max caractères.',
    ],
    'mimes' => 'Le champ :attribute doit être un fichier de type : :values.',
    'min' => [
        'numeric' => 'La valeur de :attribute doit être inférieure à :min.',
        'file' => 'Le fichier :attribute doit être plus que gros que :min kilobytes.',
        'string' => 'Le texte :attribute doit contenir au moins :min caractères.',
    ],
    'mac_address' => 'Le champ :attribute doit être une adresse MAC valide.',
    'not_in' => "Le champ :attribute sélectionné n'est pas valide.",
    'numeric' => 'Le champ :attribute doit contenir un nombre.',
    'password' => [
        'letters' => 'Le champ :attribute doit contenir au moins une lettre.',
        'mixed' => 'Le champ :attribute doit contenir au moins une lettre majuscule et une lettre minuscule.',
        'numbers' => 'Le champ :attribute doit contenir au moins un chiffre.',
        'symbols' => 'Le champ :attribute doit contenir au moins un symbole.',
        'uncompromised' => "Le :attribute est apparu dans une fuite de données. Veuillez choisir un autre :attribute.",
    ],
    'regex' => 'Le format du champ :attribute est invalide.',
    'required' => 'Le champ :attribute est obligatoire.',
    'required_if' => 'Le champ :attribute est obligatoire quand la valeur de :other est :value.',
    'required_with' => 'Le champ :attribute est obligatoire quand :values est présent.',
    'required_without' => "Le champ :attribute est obligatoire quand :values n'est pas présent.",
    'same' => 'Les champs :attribute et :other doivent être identiques.',
    'size' => [
        'array' => 'Le champ :attribute doit contenir des éléments :size.',
        'numeric' => 'La taille de la valeur de :attribute doit être :size.',
        'file' => 'La taille du fichier de :attribute doit être de :size kilobytes.',
        'string' => 'Le texte de :attribute doit contenir :size caractères.',
    ],
    'info_validations'=>'les informations sur les paramètres de validation',
    'enum' => "Le :attribute sélectionné n'est pas valide.",
    'unique' => 'La valeur du champ :attribute est déjà utilisée.',
    'url' => "Le format de l'URL de :attribute n'est pas valide.",

    'skill' => [
        'must_belong_to_parent_department' => 'Cette compétence :skill_ids n\'appartient pas au département parent.',
    ],
    'trainee' => [
        'not_access_department' => 'Ce département n\'appartient pas à l\'utilisateur.',
    ],
    'stage' => [
        'this_stage_cannot_signing' => 'Cette étape ne peut pas ajouter de signature',
        'signing_responsible_required' => "Veuillez sélectionner l'utilisateur responsable de la signature avant de soumettre le formulaire.",
    ],
    'training' => [
        'user_not_exists' => 'Cet utilisateur n\'a pas été trouvé',
        'the_trainee_not_access' => 'Ce stagiaire ne peut pas s\'inscrire à la formation',
        'not_access_skill' => 'Cet utilisateur ne peut pas accéder à la compétence',
        'this_user_not_belongsTo_admin_or_trainer' => 'Cet ID utilisateur :attribute n\'
        appartient pas au rôle d\'administrateur ou de formateur',
        'not_access_to_schedule_training' => 'Ce formateur ne peut pas accéder à la planification de la formation',
        'unique_time_training' => 'Conflit détecté.
         Veuillez choisir un autre moment pour éviter le chevauchement des formations et des évaluations.',
    ],

    'site' => [
        'must_belong_to_user' => 'Le site doit appartenir au site de l\'utilisateur.',
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [

        'invalid_action' => 'l\'action sélectionnée n\'est pas valide',
        'keypoint_not_belong_to_skill' => 'le point clé n\'appartient pas à la compétence',
        'step_not_belong_to_skill' => 'l\'étape n\'appartient pas à la compétence',
        'media_not_belongs_to_model' => 'les médias n\'appartiennent pas au modèle :attribute',

        'user_role_error' => 'le rôle d\'utilisateur doit être uniquement :role_ids',
        'department_ids_not_same_level' => 'Les identifiants de département ne sont pas dans le même niveau de département',
        'should_send_all_department_ids_for_this_level' => 'vous devez envoyer tous les identifiants de département pour ce niveau',
        'first_step_must_has_at_least_one_keypoint' => 'la première étape doit comporter au moins un point clé',

    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
