<?php

return [

    'success_message' => [
        'getCompanies' => 'Voici la liste de toutes les entreprises',
        'getSitesByCompany' => 'Voici la liste de tous les sites par entreprise',
    ],
];
