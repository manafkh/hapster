<?php

return [

    'success_message' => [

        'index' => 'Voici la liste des compétences',
        'create_successfully' => 'Compétence créée avec succès',
        'update_successfully' => 'Compétence mise à jour avec succès',
        'destroy_successfully' => 'Compétence supprimée avec succès',
        'share_skills_successfully' => 'Compétence partagée avec succès',
        'get_skill' => 'Voici les informations sur les compétences',
        'get_trainer_skills' => 'Voici la liste de toutes les compétences auxquelles le formateur peut accéder',
        'get_accessible_skills' => 'Voici la liste de toutes les compétences auxquelles l\'utilisateur peut accéder',
        'get_other_skill' => 'Voici la liste de toutes les compétences auxquelles son département ne peut pas accéder et qui lui appartiennent',
        'get_admin_skills' => 'Voici la liste de toutes les compétences auxquelles l\'administrateur peut accéder via son site',
        'get_skills_admin_without_departments' => 'Voici la liste de toutes les compétences sans départements',
        'sync_successfully' => 'synchronisation des compétences avec succès',
    ],

];
