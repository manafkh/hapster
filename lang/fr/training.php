<?php

return [

    'success_message' => [

        'create_successfully' => 'Formation créée avec succès',
        'update_status_successfully' => 'Statut de formation mis à jour avec succès',
        'update_successfully' => 'Formation mise à jour avec succès',
        'get_my_trainings' => 'Voici la liste de toutes les formations qui vous appartiennent',
        'get_times_to_all_trainings_scheduled' => 'Voici la liste de toutes les formations à l\'heure prévue',
        'get_all_trainings_metadata' => 'Voici la liste de toutes les métadonnées des formations',
        'get_info_training' => 'Voici les informations sur la formation',
        'get_stages' => 'Voici la liste de toutes les étapes',

    ],

];
