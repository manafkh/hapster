<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ces informations d\'identification ne correspondent pas à nos dossiers',
    'password' => 'Le mot de passe fourni est incorrect',
    'throttle' => 'Trop de tentatives de connexion. Veuillez réessayer dans :seconds secondes.',

    'failed_register_user' => 'Échec de l\'enregistrement de l\'utilisateur.',
    'no_access_token' => 'Impossible d\'obtenir le jeton d\'accès.',
    'wrong_otp_code' => 'Code OTP erroné.',

    'reset_token_invalid' => 'Le jeton de réinitialisation fourni n\'est pas valide ou a expiré.',
    'not_find_user' => 'Nous ne trouvons pas d\'utilisateur avec cette adresse e-mail.',
    'reset_password_failed' => 'La réinitialisation du mot de passe a échoué.',
    'email_or_username_not_found' => "email ou nom d'utilisateur introuvable",

    'invalid_credentials_for_auth' => "Informations d'identification non valides pour l'authentification",
    'generate_unique_token_error' => 'Erreur lors de la génération du jeton, veuillez réessayer ultérieurement',
    'device_not_found' => 'Appareil non trouvé',
    'invalid_registry_token' => 'Jeton d\'enregistrement non valide',
    'refresh_token_not_found' => 'Le jeton de rafraîchissement n\'a pas été trouvé',
    'check_verify_email' => 'Ce compte n\'est pas vérifié, veuillez vérifier votre courrier électronique pour le code OTP que nous avons envoyé.',
    'invalid_license' => 'Licence invalide',
    'already_verified' => "L'utilisateur est déjà vérifié",

    'success_message' => [
        'register_successfully' => 'Inscription du nouvel utilisateur réussie',
        'login_successfully' => 'Connexion réussie',
        'logout_successfully' => 'Déconnexion réussie',
        'refresh_token_successfully' => 'Rafraîchissement du jeton avec succès',
        'password_change_successfully' => 'Mot de passe changé avec succès.',
        'email_verified' => 'L\'email a été vérifié avec succès..',
        'send_verify_email' => 'envoie du courriel de vérification réussie',
        'send_reset_email' => 'envoie du courriel de vérification réussie',
    ],
];
