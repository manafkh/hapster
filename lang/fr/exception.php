<?php

return [

    'check_media_uploaded' => 'vérifier les médias téléchargés',
    'unauthorized' => "vous n'avez pas la permission pour cette action",
    'model_not_found' => 'Objet non trouvé',
    'no_authenticate_user' => "Vous devez d'abord vous connecter",
    'unauthorized_user' => 'le jeton est expiré',
    'client_error_keycloak' => 'erreur client dans keycloak',
    'server_error_keycloak' => 'erreur de serveur dans keycloak',
];
