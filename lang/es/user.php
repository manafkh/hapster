<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cant_use_as_trainer' => 'No puede usar este formulario para agregar Entrenador.',
    'must_be_trainer' => 'Debe seleccionar el rol de entrenador.',

    'success_message' => [
        'index_role' => 'Esta es la lista de todos los roles',
        'index_by_role' => 'Esta es la lista de agrupación de usuarios POR rol',
        'get_user_info' => 'Esta es la información del usuario',
        'create_successfully' => 'Usuario creado exitosamente',
        'update_successfully' => 'Usuario actualizado correctamente',
        'destroy_successfully' => 'Usuario eliminado exitosamente',
        'sync_successfully' => 'Usuario sincronizado correctamente',

        'sync_user_sites' => 'este sitio pertenece al usuario',
        'sync_user_departments' => 'este departamento pertenece al usuario',
        'sync_user_skills' => 'estas habilidades pertenecen al usuario',
        'sync_users_read' => 'Este usuario pertenece a sitios web',
    ],
];
