<?php

return [

    'success_message' => [

        'create_successfully' => 'Formación creada con éxito',
        'update_status_successfully' => 'Estado de entrenamiento actualizado exitosamente',
        'update_successfully' => 'Capacitación actualizada con éxito',
        'get_my_trainings' => 'Esta es la lista de todos los entrenamientos que te pertenecen',
        'get_times_to_all_trainings_scheduled' => 'Esta es la lista de todos los entrenamientos con el tiempo programado',
        'get_all_trainings_metadata' => 'Esta es la lista de todos los metadatos de entrenamiento',
        'get_info_training' => 'Esta es la información de entrenamiento',
        'get_stages' => 'Esta es la lista de todas las etapas',

    ],

];
