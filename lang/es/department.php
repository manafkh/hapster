<?php

return [

    'success_message' => [
        'index' => 'Esta es la lista de todos los departamentos',
        'index_for_user_' => 'Esta es la lista de todos los departamentos para el usuario',
        'create_successfully' => 'Departamento creado exitosamente',
        'update_successfully' => 'Departamento actualizado exitosamente',
        'reorder_department_successfully' => 'Reorganizar Departamento Exitosamente.',
        'import_department_from_file_successfully' => 'Departamento importado desde archivo exitosamente.',
        'destroy_successfully' => 'Departamento eliminado exitosamente',
        'sync_successfully' => 'Sincronización del departamento exitosamente',
    ],

];
