<?php

return [

    'success_message' => [

        'index' => 'Esta es la lista de todas las habilidades',
        'create_successfully' => 'Habilidad creada con éxito',
        'update_successfully' => 'Habilidad actualizada con éxito',
        'destroy_successfully' => 'Habilidad eliminada exitosamente',
        'share_skills_successfully' => 'Habilidad compartida con éxito',
        'get_skill' => 'Esta es la información de la habilidad',
        'get_trainer_skills' => 'Esta es la lista de todas las habilidades a las que puede acceder el formador',
        'get_accessible_skills' => 'Esta es la lista de todas las habilidades a las que el usuario puede acceder',
        'get_other_skill' => 'Esta es la lista de todas las habilidades a las que no se puede acceder y que pertenecen a su departamento',
        'get_admin_skills' => 'Esta es la lista de todas las habilidades a las que el administrador puede acceder desde su sitio',
        'get_skills_admin_without_departments' => 'Esta es la lista de todas las habilidades sin departamentos',
        'sync_successfully' => 'sincronización de habilidades exitosamente',
    ],

];
