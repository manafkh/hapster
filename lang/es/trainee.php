<?php

return [

    'success_message' => [
        'index' => 'Esta es la lista de todos los aprendices',
        'create_successfully' => 'Aprendiz creado exitosamente',
        'update_successfully' => 'Aprendiz actualizado exitosamente',
        'destroy_successfully' => 'Aprendiz eliminado exitosamente',
        'sync_successfully' => 'Sincronización de aprendices exitosamente',
    ],
];
