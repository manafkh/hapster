<?php

return [

    'success_message' => [
        'getCompanies' => 'Esta es la lista de todas las empresas.',
        'getSitesByCompany' => 'Esta es la lista de todos los sitios por empresa',
    ],
];
