<?php

return [

    'check_media_uploaded' => 'comprobar los medios cargados',
    'unauthorized' => 'no tienes permiso para admitir esta acción',
    'model_not_found' => 'Objeto no encontrado',
    'no_authenticate_user' => 'Tienes que iniciar sesión primero',
    'unauthorized_user' => 'el token ha caducado',
    'client_error_keycloak' => 'error del cliente en keycloak',
    'server_error_keycloak' => 'error del servidor en keycloak',
];
