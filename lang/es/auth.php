<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Las credenciales introducidas son incorrectas.',
    'password' => 'La contraseña es incorrecta.',
    'throttle' => 'Demasiados intentos de acceso. Inténtelo de nuevo en :seconds segundos.',

    'failed_register_user' => 'No se pudo registrar al usuario.',
    'no_access_token' => 'No se pudo obtener el token de acceso.',
    'wrong_otp_code' => 'Código OTP incorrecto.',

    'reset_token_invalid' => 'El token de reinicio proporcionado no es válido o ha caducado.',
    'not_find_user' => 'No podemos encontrar un usuario con esa dirección de correo electrónico.',
    'reset_password_failed' => 'Error al restablecer la contraseña.',
    'email_or_username_not_found' => "correo electrónico o nombre de usuario no encontrado",

    'invalid_credentials_for_auth' => 'Credenciales no válidas para autenticación',
    'generate_unique_token_error' => 'Error al generar el token, por favor intenta de nuevo más tarde',
    'device_not_found' => 'Dispositivo no encontrado',
    'invalid_registry_token' => 'Token de registro no válido',
    'refresh_token_not_found' => 'Token de actualización no encontrado',
    'check_verify_email' => 'Esta cuenta no está verificada. Revise su correo electrónico para ver el código OTP que le enviamos.',
    'invalid_license' => 'Licencia invalida',
    'already_verified' => 'La usuario ya está verificada',

    'success_message' => [
        'register_successfully' => 'registro de nuevo usuario exitoso',
        'login_successfully' => 'Inicio de sesión exitoso',
        'logout_successfully' => 'Salir con éxito',
        'refresh_token_successfully' => 'Actualizar token con éxito',
        'password_change_successfully' => 'Cambio de contraseña realizado exitosamente',
        'email_verified' => 'El correo electrónico se verificó correctamente.',
        'send_verify_email' => 'enviar correo electrónico de verificación exitosamente',
        'send_reset_email' => 'enviar correo electrónico de verificación exitosamente',
    ],

];
