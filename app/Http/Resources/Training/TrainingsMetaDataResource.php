<?php

namespace App\Http\Resources\Training;

use App\Http\Resources\StagesResource;
use App\Http\Resources\TraineeResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TrainingsMetaDataResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'skill_id' => $this->skill_id,
            'skill_name' => $this->skill->name,
            'skill_level' => $this->skill->confidentiality_level,
            'image' => $this->skill->image,
            'stages' => StagesResource::collection($this->stages),
            'date' => $this->date,
            'from' => $this->from,
            'to' => $this->to,
            'status' => $this->status,
            'trainer_full_name' => $this->trainer_full_name,
            'trainer_signature_date'=>$this->trainer_signature_date,
            'responsible_signature_date'=>$this->responsible_signature_date,
            'responsible_full_name'=>$this->responsible_full_name,
            'responsible_signature'=>! empty($this->responsible_signature)?
             $this->responsible_signature: null,
            'trainee_signature'=>! empty($this->trainee_signature)?
             $this->trainee_signature : null,
            'trainees' => TraineeResource::collection($this->trainees),

        ];
    }
}
