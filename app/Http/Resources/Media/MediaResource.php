<?php

namespace App\Http\Resources\Media;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * @mixin Media
 */
class MediaResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'url' => $this->original_url,
            'thumb_url' => $this->getMediaThumb()
        ];
    }


    public function getMediaThumb()
    {
        $thumb = null;

        if ($this->hasGeneratedConversion('thumb')) {
            $thumb = $this->getUrl('thumb');
        } elseif (str_starts_with($this->mime_type, 'video/')) {
            $thumb = \Storage::disk('public')->url('static/Video-thumb.jpg');
        }
        return $thumb;

    }
}
