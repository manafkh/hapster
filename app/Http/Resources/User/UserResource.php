<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\User */
class UserResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'username' => $this->username,
            'phone_number' => $this->phone_number,
            'verify_code' => $this->verify_code,
            'email' => $this->email,
            //            'email_verified_at' => $this->email_verified_at,
            //            'password' => $this->password,
            //            'attempts_count' => $this->attempts_count,
            //            'retry_attempt_time' => $this->retry_attempt_time,
            //            'is_reset' => $this->is_reset,
            //            'remember_token' => $this->remember_token,
            //            'created_at' => $this->created_at,
            //            'updated_at' => $this->updated_at,
            //            'otp' => $this->otp,
            //            'otp_expiry' => $this->otp_expiry,
            'avatar' => $this->avatar,
            'trainings_count' => $this->trainings_count,
            'access_skills_count' => $this->access_skills_count,
            'departments_count' => $this->departments_count,
            'media_count' => $this->media_count,
            'notifications_count' => $this->notifications_count,
            'permissions_count' => $this->permissions_count,
            'read_notifications_count' => $this->read_notifications_count,
            'requests_count' => $this->requests_count,
            'roles_count' => $this->roles_count,
            'sites_count' => $this->sites_count,
            'skills_count' => $this->skills_count,
            'trainees_count' => $this->trainees_count,
            'unread_notifications_count' => $this->unread_notifications_count,
            'registerMediaConversionsUsingModelInstance' => $this->registerMediaConversionsUsingModelInstance,
        ];
    }
}
