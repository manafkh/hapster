<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Media\MediaResource;
use App\Http\Resources\RoleResource;
use App\Http\Resources\SiteResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SyncUserReadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' =>  $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'username' =>  $this->username,
            'phone_number' =>  $this->phone_number,
            'email' =>  $this->email,
            'is_reset' =>  $this->is_reset,
            'created_at' =>  $this->created_at,
            'updated_at' =>  $this->updated_at,
            'avatar' =>  $this->avatar,
            'captured_skills_count' => $this->captured_skills_count,
            'trainings_done_count' =>  $this->trainings_done_count,
            'department_ids' =>  $this->department_ids,
            'site_ids' =>  $this->site_ids,
            'skill_ids' =>  $this->skill_ids,
            'roles' =>  RoleResource::collection($this->whenLoaded('roles')),
            'sites' =>  SiteResource::collection($this->whenLoaded('sites')),
        ];
    }
}
