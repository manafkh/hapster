<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Throwable;

class DBTransactionMiddleware
{
    public function handle($request, Closure $next)
    {
        DB::beginTransaction();

        $response = $next($request);

        if ($this->isErrorResponse($response)) {
            DB::rollBack();
        } else {
            DB::commit();
        }

        return $response;
    }

    private function isErrorResponse($response): bool
    {
        $isErrorResponse = false;

        if (property_exists($response, 'exception') && $response->exception instanceof Throwable) {
            $isErrorResponse = true;
        }

        return $isErrorResponse;
    }
}
