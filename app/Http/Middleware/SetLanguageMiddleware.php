<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Http\Request;

class SetLanguageMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $language = $request->header('Accept-Language');

        if (in_array($language, config('app.available_locales'))) {
            App::setLocale($language);
            \Config::set('translatable.locale', $language);
        } else {
            App::setLocale(config('app.fallback_locale'));
        }

        return $next($request);
    }
}
