<?php

namespace App\Http\Middleware;

use App\Exceptions\AuthException;
use App\Services\RegistryService;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ValidateRegistryTokenMiddleware
{
    public function __construct(protected RegistryService $registryService)
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $token = $request->header('X-Registry-Key');
        if (empty($token) || $this->registryService->checkToken($token) === false) {
            throw AuthException::invalidRegistryToken();
        }

        return $next($request);
    }
}
