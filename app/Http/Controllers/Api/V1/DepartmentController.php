<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Department\CreateDepartmentRequest;
use App\Http\Requests\Department\importDepartmentFromFileRequest;
use App\Http\Requests\Department\ReorderDepartmentRequest;
use App\Http\Requests\Department\UpdateDepartmentRequest;
use App\Http\Traits\RedisCache;
use App\Imports\DepartmentsImport;
use App\Models\Department;
use App\Services\DepartmentService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Response;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class DepartmentController extends ApiController
{
    use RedisCache;

    const KEY = 'departments';

    public function __construct(public DepartmentService $departmentService)
    {
    }

    /**
     * The index function retrieves departments data from the database and returns a JSON response with
     * the departments.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function index(): JsonResponse
    {
        if ($this->existsKey(self::KEY)) {
            $departments = $this->getData(self::KEY);
        } else {
            $departments = QueryBuilder::for(
                auth('api')->user()->departments()
            )->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('search', 'name'),
                AllowedFilter::scope('is_parent'),
            ])
                ->orderBy('sort_order')
                ->with(['children', 'children.skills', 'skills'])->get();
            $this->load(self::KEY, $departments);
        }

        return Response::success(
            __('department.success_message.index'),
            [
                'departments' => $departments
            ]
        );
    }

    /**
     * The function retrieves departments for a user and returns a JSON response with the departments.
     *
     * @return JsonResponse a JsonResponse object.
     */
    public function indexForUser(): JsonResponse
    {
            $departments = QueryBuilder::for(
                auth('api')->user()->departments()
            )->allowedFilters([
                AllowedFilter::exact('id'),
                AllowedFilter::partial('search', 'name'),
                AllowedFilter::scope('is_parent'),
            ])
                ->orderBy('sort_order')
                ->with(['children', 'children.skills', 'skills'])->get();

        return Response::success(
            __('department.success_message.index_for_user_'),
            [
                'departments' => $departments,
            ]
        );
    }

    /**
     * The create function creates a department using the validated department data and skills from the
     * request, loads the department with its children and skills, and returns a success response with
     * the created department.
     *
     * @param CreateDepartmentRequest request The  parameter is an instance of the
     * CreateDepartmentRequest class. It is used to retrieve the validated department data and skills
     * from the request.
     *
     * @return JsonResponse A JsonResponse is being returned.
     */
    public function create(CreateDepartmentRequest $request): JsonResponse
    {

        $department = $this->departmentService->create(
            $request->validatedDepartmentData(),
            $request->validated('skills')
        );

        $department->load([
            'children',
            'skills',
            'children' => ['skills'],
        ]);
        $this->forget(self::KEY);

        return Response::success(
            __('department.success_message.create_successfully'),
            [
                'department' => $department,
            ]
        );
    }

    /**
     * The function updates a department with the provided data and skills, and returns a JSON response
     * with the updated department.
     *
     * @param UpdateDepartmentRequest request The  parameter is an instance of the
     * UpdateDepartmentRequest class. It is used to retrieve and validate the data sent in the request.
     * @param Department department The "department" parameter is an instance of the Department model.
     * It represents the department that needs to be updated.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function update(UpdateDepartmentRequest $request, Department $department): JsonResponse
    {

        $department = $this->departmentService->update(
            $department,
            $request->validatedDepartmentData(),
            $request->validated('skills')
        );

        $department->load([
            'children',
            'skills',
            'children' => ['skills'],
        ]);
        $this->forget(self::KEY);

        return Response::success(
            __('department.success_message.update_successfully'),
            [
                'department' => $department,
            ]
        );
    }

    /**
     * The function `reorderDepartment` reorders a department based on the provided request and returns
     * a success message.
     *
     * @param ReorderDepartmentRequest request The parameter `` is an instance of the
     * `ReorderDepartmentRequest` class. It is used to retrieve the data sent in the request.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function reorderDepartment(ReorderDepartmentRequest $request): JsonResponse
    {
        $this->departmentService->reorder(
            $request->validated('department_sorted'),
        );
        $this->forget(self::KEY);

        return Response::success(__('department.success_message.reorder_department_successfully'));
    }

    /**
     * The function imports department data from a file using Laravel Excel and returns a success
     * message.
     *
     * @param ImportDepartmentFromFileRequest request The  parameter is an instance of the
     * ImportDepartmentFromFileRequest class. It is used to retrieve the file that needs to be
     * imported.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function importDepartmentFromFile(ImportDepartmentFromFileRequest $request): JsonResponse
    {
        Excel::import(new DepartmentsImport, $request->file('file'));

        return Response::success(__('department.success_message.import_department_from_file_successfully'));
    }

    /**
     * The "destroy" function in PHP destroys a department object, removes it from memory, and returns
     * a success message.
     *
     * @param Department department The parameter `` is an instance of the `Department`
     * class.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function destroy(Department $department): JsonResponse
    {
        $this->departmentService->destroy(
            $department
        );
        $this->forget(self::KEY);

        return Response::success(__('department.success_message.destroy_successfully'));
    }

    /**
     * The function syncDepartments handles the synchronization of department data based on the
     * provided request.
     *
     * @param Request request The `` parameter is an instance of the `Illuminate\Http\Request`
     * class. It represents the HTTP request made to the server and contains information such as the
     * request method, headers, and input data.
     *
     * @return a response with a success message and an array of responses for each department action
     * that was processed.
     */
    //TODO : split this method into add, update, delete methods
    public function syncDepartments(Request $request)
    {
        $response = [];
        foreach ($request->departments as $item) {
            try {
                switch ($item['action']) {
                    case 'add':
                        $response[] = $this->departmentService->handleAddAction($item['data']);
                        break;
                    case 'updated':
                        $response[] = $this->departmentService->handleUpdateAction($item['data']);
                        break;
                    case 'delete':
                        $response[] = $this->departmentService->handleDeleteAction($item['data']);
                        break;
                    default:
                        $response[] = Response::error(__('validation.custom.invalid_action'))->original;
                        break;
                }
            } catch (\Exception $e) {
                $response[] = Response::error($e->getMessage())->original;
            }
        }
        $this->forget(self::KEY);

        return Response::success(__('department.success_message.sync_successfully'), $response);
    }
}
