<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Traits\RedisCache;
use App\Models\Company;
use Response;

class SiteCompanyDepartmentController extends Controller
{
    use RedisCache;

    const COMPANIES = 'Companies';

    /**
     * The function retrieves a list of companies and returns a success response with the companies
     * data.
     *
     * @return a success response with a message and an array of companies.
     */
    public function getCompanies()
    {
        if ($this->existsKey(self::COMPANIES)) {
            $companies  = $this->getData(self::COMPANIES);
        } else {
            $companies = Company::all(['id', 'name']);
            $this->load(self::COMPANIES, $companies);
        }

        return Response::success(
            __('company.success_message.getCompanies'),
            [
                'companies' => $companies,
            ]
        );
    }

    /**
     * The function getSitesByCompany retrieves the sites associated with a company and returns a
     * success response with the company and sites data.
     *
     * @param companyId The companyId parameter is the unique identifier of a company. It is used to
     * retrieve the sites associated with the specified company.
     *
     * @return a success response with a message and an array containing the company and its associated
     * sites.
     */
    public function getSitesByCompany($companyId)
    {
        $company = Company::findOrFail($companyId);
        $sites = $company->sites()->get(['id', 'name']);

        return Response::success(
            __('company.success_message.getSitesByCompany'),
            [
                'company' => $company,
                'sites' => $sites,
            ]
        );
    }
}
