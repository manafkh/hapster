<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Traits\RedisCache;
use Response;
use Spatie\Permission\Models\Role;

class RoleController extends ApiController
{
    use RedisCache;
    const KEY = "ROLES";
    /**
     * The index function retrieves roles from the database and returns a success response with the
     * roles data.
     *
     * @return a success response with a message and an array of roles.
     */
    public function index()
    {
        if ($this->existsKey(self::KEY)) {
            $roles  = $this->getData(self::KEY);
        } else {
            $roles = Role::all();;
            $this->load(self::KEY, $roles);
        }
        return Response::success(
            __('user.success_message.index_role'),
            [
                'roles' => $roles
            ]
        );
    }
}
