<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Services\SkillService;
use Illuminate\Http\Request;
use Response;

class LRSController extends Controller
{
    public function __construct(private SkillService $skillservice)
    {
    }

    public function store(Request $request)
    {

        $response = $this->skillservice->exportSkillToLRS(
            $request->validated('actor'),
            'launched',
            $request->validated('object')
        );

        return Response::success('', $response);
    }
}
