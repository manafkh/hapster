<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginUserRequest;
use App\Http\Requests\Auth\RefreshTokenRequest;
use App\Http\Requests\Auth\RegisterUserRequest;
use App\Http\Requests\Auth\SendVerifyEmailRequest;
use App\Http\Requests\Auth\VerifyEmailRequest;
use App\Models\License;
use App\Models\User;
use App\Services\AuthService;
use App\Services\Keycloak\KeycloakService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;

class AuthController extends Controller
{
    public function __construct(
        private AuthService     $authService,
        private KeycloakService $keycloakService
    )
    {
    }

    /**
     * The function registers a user with validated user data and sites, and returns a success message.
     *
     * @param RegisterUserRequest request The  parameter is an instance of the
     * RegisterUserRequest class. It is used to retrieve the data submitted by the user during the
     * registration process.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function register(RegisterUserRequest $request): JsonResponse
    {
        $this->authService->register($request->validatedUserData(), $request->validated('sites'));

        return Response::success(__('auth.success_message.register_successfully'));
    }

    public function login(LoginUserRequest $request, License $license): JsonResponse
    {
        $data = $this->authService
            ->login($request->validated('email'), $request->validated('password'));

        if ($data['user']->isVerified() === false) {
            $this->authService->sendOTPCode($data['user']);

            return Response::error(__('auth.check_verify_email'), 403);
        }

        $license = $license->getBySiteId($data["user"]->sites()->first()->id);
        if (empty($license) || Carbon::now()->isBetween($license->start_date, $license->end_date, true) === false) {
            return Response::error(__('auth.invalid_license'), 408);
        }

        return Response::success(
            __('auth.success_message.login_successfully'),
            [
                'token' => $data['access_token'],
                'refresh_token' => $data['refresh_token'],
                'user' => $data['user'],
            ]
        );
    }

    /**
     * The function logs out the user and returns a success message.
     *
     * @return JsonResponse a JsonResponse object.
     */
    public function logout(Request $request): JsonResponse
    {
       
        $this->keycloakService->logouUser($request->get('refresh_token'));
        return Response::success(
            __('auth.success_message.logout_successfully'),

        );
    }

    /**
     * The `refreshToken` function takes a `RefreshTokenRequest` object, refreshes the token using the
     * Keycloak service, and returns a JSON response with the new access token and refresh token.
     *
     * @param RefreshTokenRequest request The parameter `` is an instance of the
     * `RefreshTokenRequest` class. It is likely a custom class that represents the request made to
     * refresh a token. It may contain properties such as `refresh_token`, which is the token that
     * needs to be refreshed.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function refreshToken(RefreshTokenRequest $request): JsonResponse
    {
        $refreshToken = $this->keycloakService->refreshToken(
            $request->validated('refresh_token')
        );

        return Response::success(
            __('auth.success_message.refresh_token_successfully'),
            [
                'token' => $refreshToken['access_token'],
                'refresh_token' => $refreshToken['refresh_token'],
            ]
        );
    }

    /**
     * The function sends a verification email to a user if they have not already been verified.
     *
     * @param SendVerifyEmailRequest request The  parameter is an instance of the
     * SendVerifyEmailRequest class. It is used to retrieve the email address from the request data.
     *
     * @return a success response with the message "auth.success_message.send_verify_email" if the user
     * is not already verified. If the user is already verified, it returns an error response with the
     * message "auth.already_verified".
     */
    public function sendVerificationEmail(SendVerifyEmailRequest $request)
    {
        $user = User::getUser($request->get('email'))->first();

        if ($user->isVerified()) {
            return Response::error(__('auth.already_verified'), 400);
        }

        $this->authService->sendOTPCode($user);

        return Response::success(__('auth.success_message.send_verify_email'));
    }

    /**
     * The function verifies the email address of a user, validates the OTP (one-time password), and
     * returns a JSON response with an access token, refresh token, and user data.
     *
     * @param VerifyEmailRequest request The  parameter is an instance of the
     * VerifyEmailRequest class. It is used to retrieve the validated data from the request, such as
     * the email and OTP (one-time password) values.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function verifyEmail(VerifyEmailRequest $request): JsonResponse
    {
        $user = User::getUser($request->validated('email'))->firstOrFail();

        $data = $this->authService->login($request->validated('email'), $request->validated('password'));

        $this->authService->verifyOTP($user, $request->validated('otp'));


        return Response::success(__('auth.success_message.email_verified'), [
            'token' => $data['access_token'],
            'refresh_token' => $data['refresh_token'],
            'user' => $data['user'],
        ]);
    }
}
