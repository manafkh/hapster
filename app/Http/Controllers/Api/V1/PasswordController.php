<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ChangePasswordByAdminRequest;
use App\Http\Requests\Auth\ChangePasswordRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use App\Http\Requests\Auth\SendResetLinkEmailRequest;
use App\Http\Requests\Auth\ShowResetFormRequest;
use App\Http\Traits\RedisCache;
use App\Models\User;
use App\Services\AuthService;
use Response;

class PasswordController extends Controller
{
    use RedisCache;

    private const KEY = "users";
    /**
     * The function is a constructor that takes an instance of the AuthService class as a parameter.
     */
    public function __construct(
        private AuthService $authService,
    ) {
    }

    /**
     * The function sends a reset link email to the user's provided email address.
     *
     * @param SendResetLinkEmailRequest request The  parameter is an instance of the
     * SendResetLinkEmailRequest class. It is used to retrieve the input data from the user, such as
     * the email address to which the reset link email will be sent. The request object is typically
     * validated before being used to ensure that the input data is valid
     *
     * @return a success response with the message "auth.success_message.send_reset_email".
     */
    public function sendResetLinkEmail(SendResetLinkEmailRequest $request)
    {
        $this->authService->sendResetLinkEmail($request->validated('email'));

        return Response::success(__('auth.success_message.send_reset_email'));
    }

    /**
     * The function "showResetForm" displays the password reset form with the provided token and email.
     *
     * @param ShowResetFormRequest request The `` parameter is an instance of the
     * `ShowResetFormRequest` class, which is a custom request class that handles the validation and
     * authorization for the password reset form.
     * @param token The token parameter is used to pass the password reset token to the view. This
     * token is generated when a user requests a password reset and is used to verify the authenticity
     * of the reset request.
     *
     * @return a view called 'auth.passwords.reset' with the token and email as data.
     */
    public function showResetForm(ShowResetFormRequest $request, $token = null)
    {
        return view('auth.passwords.reset',
            [
                'token' => $token,
                'email' => $request->validated('email'),
            ]);
    }

    /**
     * The function resets a user's password and returns a view indicating the success of the reset.
     *
     * @param ResetPasswordRequest request The `` parameter is an instance of the
     * `ResetPasswordRequest` class. It is used to retrieve the input data from the user, such as the
     * email and password for resetting the password.
     *
     * @return The view 'auth.passwords.reset-success' is being returned.
     */
    public function reset(ResetPasswordRequest $request)
    {
        $this->authService->reset(
            $request->validated('email'),
            $request->validated('password')
        );

        return view('auth.passwords.reset-success');
    }

    /**
     * The function `changePassword` in PHP validates and changes the user's password and returns a
     * success message.
     *
     * @param ChangePasswordRequest request The `` parameter is an instance of the
     * `ChangePasswordRequest` class. It is used to retrieve the input data from the user for changing
     * the password. The `validated()` method is called on the `` object to retrieve the
     * validated data for the `new_password` field.
     *
     * @return a success response with the message "auth.success_message.password_change_successfully".
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $this->authService->changePassword($request->validated('new_password'));
        $this->forget(self::KEY);
        return Response::success(__('auth.success_message.password_change_successfully'));
    }

    /**
     * The function changes a user's password by an admin and returns a success message.
     *
     * @param User user The "user" parameter is an instance of the User model. It represents the user
     * whose password needs to be changed.
     * @param ChangePasswordByAdminRequest request The  parameter is an instance of the
     * ChangePasswordByAdminRequest class. It is used to validate and retrieve the new password value
     * from the request.
     *
     * @return a success response with the message "auth.success_message.password_change_successfully".
     */
    public function changePasswordByAdmin(User $user, ChangePasswordByAdminRequest $request)
    {

        $this->authService->changePasswordByAdmin($user,
                                                $request->validated('new_password'),
                                                $request->validated('is_reset'));

         $this->forget(self::KEY);
        return Response::success(__('auth.success_message.password_change_successfully'));
    }
}
