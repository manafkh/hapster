<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use App\Services\TrainerService;
use Illuminate\Http\Request;
use Response;

class TrainerController extends ApiController
{
    public function __construct(private TrainerService $trainerService)
    {
    }

    /**
     * The index function retrieves trainers based on the request and returns a success response with
     * the trainers.
     *
     * @param Request request The  parameter is an instance of the Request class, which
     * represents an HTTP request made to the server. It contains information about the request, such
     * as the HTTP method, URL, headers, and any data sent with the request.
     *
     * @return a success response with a message and an array of trainers.
     */
    public function index(Request $request)
    {
        $trainers = $this->trainerService->getTrainers($request);

        return Response::success(
            __('trainer.success_message.index'),
            [
                'trainers' => $trainers,
            ]
        );
    }
}
