<?php

namespace App\Http\Controllers\Api\V1;

use App\Enums\TrainingStatusEnum;
use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Training\CreateTrainingRequest;
use App\Http\Requests\Training\UpdateStatusTrainingRequest;
use App\Http\Requests\Training\UpdateTrainingRequest;
use App\Http\Resources\Training\TrainingsMetaDataResource;
use App\Models\Stage;
use App\Models\Training;
use App\Services\TrainingService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Response;

class TrainingController extends ApiController
{
    public function __construct(private TrainingService $trainingService)
    {
    }

    public function create(CreateTrainingRequest $request)
    {
        $training = $this->trainingService->create($request);

        return Response::success(
            __('training.success_message.create_successfully'),
            [
                'training' => $training,
            ]);
    }

    public function updateStatus(UpdateStatusTrainingRequest $request)
    {
        $training = $this->trainingService->updateStatus(
            $request->validated('status'),
            $request->validated('training_id'),
            $request->validatedSignatureFiles(),
            $request->validated('responsible_signature_date'),
            $request->validated('trainer_signature_date')
        );

        return Response::success(
            __('training.success_message.update_status_successfully'),
            [
                'training' => $training,
            ]);
    }

    public function getMyTrainings(Request $request)
    {
        $myTrainings = auth('api')->user()->trainings()->with(['skill', 'stages', 'user', 'trainees']);
        if ($request->has('search')) {
            $myTrainings->whereHas('skill', function ($query) use ($request) {
                $query->whereTranslationLike('name', '%' . $request->get('search') . '%');

            });
        }

        return Response::success(
            __('training.success_message.get_my_trainings'),
            [
                'my_trainings' => TrainingsMetaDataResource::collection($myTrainings->get()),
            ]);
    }

    /**
     * The function retrieves the scheduled training times for a specific user and returns them in a
     * success response.
     *
     * @param Request request The  parameter is an instance of the Request class, which is used
     * to retrieve data from the HTTP request. In this case, it is used to get the value of the
     * 'user_id' parameter from the request.
     *
     * @return a success response with a message and an array of training times.
     */
    public function getTimesToAllTrainingsScheduled(Request $request)
    {
        $trainingTimes = Training::query()
            ->where('user_id', $request->get('user_id'))
            ->where('status', TrainingStatusEnum::SCHEDULED)
            ->get(['id', 'date', 'from', 'to']);

        return Response::success(
            __('training.success_message.get_times_to_all_trainings_scheduled'),
            [
                'training_times' => $trainingTimes,
            ],

        );
    }

    /**
     * The function retrieves metadata for all trainings, including related skill, stages, user, and
     * trainees, and allows for searching by skill name.
     *
     * @param Request request The  parameter is an instance of the Request class, which
     * represents an HTTP request. It contains information about the request such as the request
     * method, headers, query parameters, and request body.
     *
     * @return a success response with a message and an array of trainings metadata. The trainings
     * metadata is obtained by querying the authenticated user's trainings and including relationships
     * with skill, stages, user, and trainees. If a search parameter is provided in the request, the
     * trainings are filtered based on the translation of the skill name. The trainings metadata is
     * then transformed into a collection
     */
    public function getAllTrainingsMetaData(Request $request)
    {
        $trainings = auth('api')->user()->trainings()->with(['skill', 'stages', 'user', 'trainees']);
        if ($request->has('search')) {
            $trainings->whereHas('skill', function (Builder $query) use ($request) {
                $query->whereTranslationLike('name', '%' . $request->get('search') . '%');
            });
        }

        return Response::success(
            __('training.success_message.get_all_trainings_metadata'),
            [
                'trainings' => TrainingsMetaDataResource::collection($trainings->get()),
            ],
        );
    }

    /**
     * The function `getInfoTraining` loads information about a training and returns a success response
     * with the loaded data.
     *
     * @param Training training The parameter "training" is an instance of the Training class. It is
     * used to load related data such as stages, user, skill, trainees, and signingResponsible. The
     * loaded data is then returned as a success response with the message
     * "training.success_message.get_info_training" and the loaded
     *
     * @return a success response with a message and an array containing the training information.
     */
    public function getInfoTraining(Training $training)
    {
        $training->load(['stages', 'user', 'skill', 'trainees', 'signingResponsible']);

        return Response::success(
            __('training.success_message.get_info_training'),
            [
                'trainings' => $training,
            ]);
    }

    /**
     * The function updates a training record and returns a success response with the updated training
     * data.
     *
     * @param UpdateTrainingRequest request The  parameter is an instance of the
     * UpdateTrainingRequest class. It is used to validate and retrieve the data sent in the request to
     * update a training.
     * @param Training training The "training" parameter is an instance of the Training model. It
     * represents the training that needs to be updated.
     *
     * @return a success response with a message and an array containing the updated training object.
     */
    public function update(UpdateTrainingRequest $request, Training $training)
    {
        $training = $this->trainingService->update($training, $request->validated());

        $training->load(['user', 'skill', 'stages', 'trainees']);

        return Response::success(
            __('training.success_message.update_successfully'),
            [
                'training' => $training,
            ],
        );
    }

    /**
     * The function "getStages" retrieves the stages from the database and returns a success response
     * with the stages.
     *
     * @return a success response with a message and an array of stages.
     */
    public function getStages()
    {
        $stages = Stage::get(['id', 'name']);

        return Response::success(
            __('training.success_message.get_stages'),
            [
                'trainings' => $stages,
            ]);
    }
}
