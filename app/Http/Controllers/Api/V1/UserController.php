<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\User\CreateCapturerORAdminRequest;
use App\Http\Requests\User\CreateTrainerRequest;
use App\Http\Requests\User\UpdateCapturerOrAdminRequest;
use App\Http\Requests\User\UpdateTrainerRequest;
use App\Http\Resources\User\SyncUserReadResource;
use App\Http\Traits\RedisCache;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Response;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class UserController extends ApiController
{
    use RedisCache;

    private const KEY = "users";

    public function __construct(private UserService $userService)
    {
    }

    /**
     * The function `indexByRole` retrieves users by their roles and returns a JSON response with the
     * users grouped by roles.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function indexByRole(): JsonResponse
    {
        if ($this->existsKey(self::KEY)) {
            $roles = $this->getData(self::KEY);
        } else {
            $roles = QueryBuilder::for($this->userService->getUsersByRolesInSite())
                ->allowedFilters([
                    AllowedFilter::exact('role_id', 'id'),
                ])->get();
            $this->load(self::KEY, $roles);
        }

        return Response::success(
            __('user.success_message.index_by_role'),
            [
                'user_by_roles' => $roles,
            ]
        );
    }

    /**
     * The function creates a capturer or admin user and returns a success response with the created
     * user.
     *
     * @param CreateCapturerORAdminRequest request The parameter `` is an instance of the
     * `CreateCapturerORAdminRequest` class. It is used to retrieve the data sent in the HTTP request.
     *
     * @return JsonResponse A JsonResponse is being returned.
     */
    public function createCapturerOrAdmin(CreateCapturerORAdminRequest $request): JsonResponse
    {
        $user = $this->userService->createCapturerOrAdmin(
            $request->validatedUserData(),
            $request->validated('roles')
        );
        $this->forget(self::KEY);

        return Response::success(
            __('user.success_message.create_successfully'),
            [
                'user' => $user
            ]
        );
    }

    /**
     * The function creates a trainer, retrieves their information, and returns a success response.
     *
     * @param CreateTrainerRequest request The  parameter is an instance of the
     * CreateTrainerRequest class. It is used to retrieve the validated data from the request, such as
     * user data, roles, departments, and skills.
     *
     * @return a success response with a message and a user object.
     */
    public function createTrainer(CreateTrainerRequest $request)
    {

        $user = $this->userService->createTrainer(
            $request->validatedUserData(),
            $request->validated('roles'),
            $request->validated('departments'),
            $request->validated('skills'),
        );

        $user = $this->userService->getUserInfo($user);
        $this->forget(self::KEY);

        return Response::success(
            __('user.success_message.create_successfully'),
            [
                'user' => $user
            ]
        );
    }

    /**
     * The function updates a trainer's information and returns a success response with the updated
     * user data.
     *
     * @param UpdateTrainerRequest request The  parameter is an instance of the
     * UpdateTrainerRequest class. It is used to validate and retrieve the data sent in the request.
     * @param User user The "user" parameter is an instance of the User model. It represents the user
     * that needs to be updated.
     *
     * @return a success response with a message and the updated user information.
     */
    public function updateTrainer(UpdateTrainerRequest $request, User $user)
    {
        $user = $this->userService->updateTrainer(
            $user,
            $request->validatedUserData(),
            $request->validated('roles'),
            $request->validated('departments'),
            $request->validated('skills')
        );

        $user = $this->userService->getUserInfo($user);
        $this->forget(self::KEY);
        return Response::success(
            __('user.success_message.update_successfully'),
            [
                'user' => $user
            ]
        );
    }

    /**
     * The function updates the user's capturer or admin roles and returns a success response with the
     * updated user data.
     *
     * @param UpdateCapturerOrAdminRequest request The  parameter is an instance of the
     * UpdateCapturerOrAdminRequest class. It is used to validate and retrieve the data sent in the
     * request.
     * @param User user The "user" parameter is an instance of the User model. It represents the user
     * that needs to be updated with new data.
     *
     * @return a success response with a message and the updated user data.
     */
    public function updateCapturerOrAdmin(UpdateCapturerOrAdminRequest $request, User $user)
    {
        $user = $this->userService->updateCapturerOrAdmin(
            $user,
            $request->validatedUserData(),
            $request->validated('roles')
        );

        return Response::success(
            __('user.success_message.update_successfully'),
            [
                'user' => $user
            ]
        );
    }

    /**
     * The function deletes a user from a keycloak and returns a success message.
     *
     * @param User user The "user" parameter is an instance of the User model. It represents the user
     * that needs to be deleted from the system.
     *
     * @return a success response message.
     */
    public function delete(User $user)
    {
        //TODO : delete user from keycloak
        $this->delete($user);
        $this->forget(self::KEY);
        return Response::success(__('user.success_message.destroy_successfully'));
    }

    /**
     * The function retrieves user information and returns a success response with the user data.
     *
     * @param User user The parameter "user" is an instance of the User class. It is passed to the
     * getUserInfo method of the userService object to retrieve information about the user.
     *
     * @return a success response with a message and the user information.
     */
    public function getUserInfo(User $user)
    {
        $user = $this->userService->getUserInfo($user);

        return Response::success(
            __('user.success_message.get_user_info'),
            [
                'user' => $user
            ]
        );
    }

    /**
     * The function syncUser handles synchronization of user data based on different actions (add,
     * update, delete) and returns a response indicating the success or failure of each action.
     *
     * @param Request request The  parameter is an instance of the Request class, which is used
     * to retrieve data from the HTTP request. In this case, it is used to retrieve the 'users' data
     * from the request.
     *
     * @return a response with a success message and an array of responses.
     */
    //TODO : split this method into add, update, delete methods
    public function syncUser(Request $request)
    {
        $response = [];

        foreach ($request->users as $key => $item) {
            try {
                switch ($item['action']) {
                    case 'add':
                        $response[] = $this->userService->handleAddAction($item['data']);
                        break;
                    case 'updated':
                        $response[] = $this->userService->handleUpdateAction($item['data']);
                        break;
                    case 'delete':
                        $response[] = $this->userService->handleDeleteAction($item['data']);
                        break;
                    default:
                        $response[] = Response::error(__('validation.custom.invalid_action'))->original;

                        break;
                }
            } catch (\Exception $e) {
                $response[] = Response::error($e->getMessage())->original;
            }
        }
        $this->forget(self::KEY);
        return Response::success(__('user.success_message.sync_successfully'), $response);
    }

    // Todo : remove to separate controller

    /**
     * The function syncUserSites retrieves the sites associated with the authenticated user and
     * returns a success response with the user's sites.
     *
     * @return a success response with a message and an array of user sites.
     */
    public function syncUserSites()
    {
        $userSites = auth('api')->user()->sites;

        return Response::success(
            __('user.success_message.sync_user_sites'),
            [
                'user_sites' => $userSites,
            ]
        );
    }

    /**
     * The function syncUserDepartments retrieves the departments that belong to the authenticated
     * user's sites and returns a success response with the user departments.
     *
     * @return a success response with a message and an array containing the user's departments.
     */
    public function syncUserDepartments()
    {
        $userDepartments = auth('api')->user()
            ->departmentsBelongsToUserSites
            ->makeHidden(['children', 'laravel_through_key']);

        return Response::success(
            __('user.success_message.sync_user_departments'),
            [
                'user_departments' => $userDepartments,
            ]
        );
    }

    /**
     * The function syncUserSkills retrieves the user's admin or capture skills and returns a success
     * response with the user skills.
     *
     * @return a success response with a message and an array of user skills.
     */
    public function syncUserSkills()
    {

        $userSkills = auth('api')->user()->adminOrCaptureSkills;

        return Response::success(
            __('user.success_message.sync_user_skills'),
            [
                'user_skills' => $userSkills,
            ]
        );
    }

    /**
     * The function syncUsersRead retrieves users associated with the authenticated user's sites and
     * returns a success response with the users.
     *
     * @return a success response with a message and an array of users.
     */
    public function syncUsersRead()
    {
        if ($this->existsKey(self::KEY)) {
            $users = $this->getData(self::KEY);
        } else {

            $siteIDs = auth('api')->user()->sites->pluck('id')->toArray();
            $users = User::query()->whereHas('sites', function (Builder $q) use ($siteIDs) {
                return $q->whereIn('site_id', $siteIDs);
            })->get();
            $this->load(self::KEY, $users);
        }
        return Response::success(
            __('user.success_message.sync_users_read'),
            [
                'users' => SyncUserReadResource::collection($users),
            ]
        );
    }
}
