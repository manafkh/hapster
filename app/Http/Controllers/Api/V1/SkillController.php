<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Skill\CreateSkillRequest;
use App\Http\Requests\Skill\UpdateSkillRequest;
use App\Http\Traits\RedisCache;
use App\Models\Department;
use App\Models\Skill;
use App\Services\SkillService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use App\Http\Requests\Skill\ImportTFSRequest;
use App\Jobs\ImportSkillJob;
use App\Models\TFSSkills;
use Response;

class SkillController extends ApiController
{
    use RedisCache;

    const ACCESS_SKILL = 'access_skills';

    const INDEX = 'Index_skills';

    const ADMIN_SKILLS_SHARED = 'admin_skills_shared';

    const ADMIN_SKILLS_NOT_SHARED = 'admin_skills_not_shared';

    public function __construct(private SkillService $skillService)
    {
    }

    /**
     * The index function retrieves skills data from cache if available, otherwise it fetches it from
     * the database and stores it in cache, and then returns a success response with the skills data.
     *
     * @return JsonResponse A JsonResponse object is being returned.
     */
    public function index(): JsonResponse
    {

        if ($this->existsKey(self::INDEX)) {
            $skills  = $this->getData(self::INDEX);
        } else {
            $skills =  Skill::all();
            $this->load(self::INDEX, $skills);
        }

        return Response::success(
            __('skill.success_message.index'),
            [
                'skills' => $skills,
            ]
        );
    }

    /**
     * The function creates a skill with steps and keypoints, loads the skill data, forgets certain
     * cache keys, and returns a success response with the created skill.
     *
     * @param CreateSkillRequest request The  parameter is an instance of the
     * CreateSkillRequest class. It is used to retrieve the validated data from the request.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function create(CreateSkillRequest $request): JsonResponse
    {

        $skill = $this->skillService->createSkillWithStepsAndKeypoints(
            $request->validatedSkillData(),
            $request->validated('steps'),
            $request->validated('image'),
            $request->validated('methods_and_documentation'),
        );

        $skill->load([
            'steps',
            'steps.keypoints',
        ]);
        $this->forgetListKeys([
            self::ACCESS_SKILL,
            self::ADMIN_SKILLS_NOT_SHARED,
            self::ADMIN_SKILLS_SHARED,
            self::INDEX
        ]);
        return Response::success(
            __('skill.success_message.create_successfully'),
            [
                'skill' => $skill,
            ]
        );
    }

    /**
     * The function updates a skill with steps and keypoints, refreshes the skill data, and returns a
     * success response.
     *
     * @param UpdateSkillRequest request The `` parameter is an instance of the
     * `UpdateSkillRequest` class, which is a custom request class that handles the validation and data
     * retrieval for updating a skill.
     * @param Skill skill The "skill" parameter is an instance of the Skill model. It represents the
     * skill that needs to be updated with the provided data and steps.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function update(UpdateSkillRequest $request, Skill $skill): JsonResponse
    {
        $skill = $this->skillService->updateSkillWithStepsAndKeypoints(
            $skill,
            $request->validatedSkillData(),
            $request->validated('steps'),
            $request->validated('delete_step_ids'),
            $request->validated('delete_keypoint_ids'),
            $request->validated('image'),
            $request->validated('methods_and_documentation'),
        );

        $skill->refresh()
            ->load([
                'steps',
                'steps.keypoints',
            ]);

        $this->forgetListKeys([
            self::ACCESS_SKILL,
            self::ADMIN_SKILLS_NOT_SHARED,
            self::ADMIN_SKILLS_SHARED,
            self::INDEX
        ]);
        return Response::success(
            __('skill.success_message.update_successfully'),
            [
                'skill' => $skill,
            ]
        );
    }

    /**
     * The above function deletes a skill and returns a success message.
     *
     * @param Skill skill The parameter `` is an instance of the `Skill` model. It represents the
     * skill that needs to be deleted from the database.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function destroy(Skill $skill): JsonResponse
    {
        $skill->delete();
        $this->forgetListKeys([
            self::ACCESS_SKILL,
            self::ADMIN_SKILLS_NOT_SHARED,
            self::ADMIN_SKILLS_SHARED,
            self::INDEX
        ]);
        return Response::success(__('skill.success_message.destroy_successfully'));
    }

    /**
     * The function returns a JSON response with a success message and the skill object.
     *
     * @param Skill skill The parameter "skill" is an instance of the Skill class.
     *
     * @return JsonResponse A JsonResponse object is being returned.
     */
    public function getSkill(Skill $skill): JsonResponse
    {
        return Response::success(
            __('skill.success_message.get_skill'),
            [
                'skill' => $skill,
            ]
        );
    }

    /**
     * The function retrieves the accessible skills for the authenticated user's departments and
     * returns a success response with the accessible skills.
     *
     * @return a success response with a message and an array of accessible skills. The accessible
     * skills are obtained by querying the departments associated with the authenticated user and
     * retrieving the skills that have requests with the "is_access" field set to true.
     */
    public function getAccessibleSkills()
    {
        $authUser = auth('api')->user();

        $departments = $authUser->departments()
            ->with([
                'skills' => function ($query) {
                    $query->whereHas('requests', function ($query) {
                        return $query->where('is_access', '=', true);
                    });
                },
            ])->get();

        return Response::success(
            __('skill.success_message.get_accessible_skills'),
            [
                'accessible_skills' => $departments,
            ]
        );
    }

   /**
    * The function retrieves other skills that are not accessible to the authenticated user.
    *
    * @return JsonResponse a JsonResponse.
    */
    public function getOtherSkill(): JsonResponse
    {
        $authUser = auth('api')->user();

        $accessibleSkillIds = $authUser
            ->accessSkills()
            ->allRelatedIds()
            ->toArray();

        $otherSkills = Department::whereIn('site_id', $authUser->sites->pluck('id')->toArray())
            ->with([
                'children',
                'children.skills',
                'skills' => function ($query) use ($accessibleSkillIds) {
                    return $query->whereNotIn('skills.id', $accessibleSkillIds);
                },
            ])->get();

        return Response::success(
            __('skill.success_message.get_other_skill'),
            [
                'other_skills' => $otherSkills,
            ],
        );
    }

    /**
     * The function `getAdminSkills` retrieves departments with skills and skills without departments
     * for an admin user.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function getAdminSkills(): JsonResponse
    {


        if ($this->existsKey(self::ADMIN_SKILLS_SHARED)) {
            $departmentsWithSkills  = $this->getData(self::ADMIN_SKILLS_SHARED);
        } else {
            $user = auth('api')->user();

            $departmentsWithSkills = $user
                ->departmentsBelongsToUserSites()
                ->with([
                    'children',
                    'children.skills',
                    'skills',
                ])->get();
            $this->load(self::ADMIN_SKILLS_SHARED, $departmentsWithSkills);
        }

        if ($this->existsKey(self::ADMIN_SKILLS_NOT_SHARED)) {
            $skillsWithoutDepartments  = $this->getData(self::ADMIN_SKILLS_NOT_SHARED);
        } else {
            $skillsWithoutDepartments = $user->sites()->first()
                ->skills()->doesntHave('departments')->get();
            $this->load(self::ADMIN_SKILLS_NOT_SHARED, $skillsWithoutDepartments);
        }
        return Response::success(
            __('skill.success_message.get_admin_skills'),
            [
                'departments_with_skills' => $departmentsWithSkills,
                'skills_without_departments' => $skillsWithoutDepartments,
            ]
        );
    }

    /**
     * The function "shareSkills" shares skills from one department to another department in PHP.
     *
     * @param Department fromDepartment The "fromDepartment" parameter represents the department from
     * which the skills will be shared. It is an instance of the "Department" class.
     * @param Department toDepartment The `` parameter is an instance of the `Department`
     * class. It represents the department to which the skills will be shared.
     *
     * @return JsonResponse a JsonResponse.
     */
    public function shareSkills(Department $fromDepartment, Department $toDepartment): JsonResponse
    {
        $skillsIdForShare = $fromDepartment->skills()->allRelatedIds();

        $toDepartment->skills()->syncWithoutDetaching($skillsIdForShare);

        return Response::success(
            __('skill.success_message.share_skills_successfully')
        );
    }

    /**
     * The function returns a success response with the admin skills of the authenticated user without
     * departments.
     *
     * @return a success response with a message and an array of admin skills.
     */
    public function getSkillsAdminWithoutDepartments()
    {
        return Response::success(
            __('skill.success_message.get_skills_admin_without_departments'),
            [
                'admin_skills' => auth('api')->user()->adminOrCaptureSkills,
            ]
        );
    }

   /**
    * The function `getTrainerSkills` retrieves the access skills of a trainer and returns a success
    * response with the trainer skills.
    *
    * @return a success response with a message and an array of trainer skills.
    */
    public function getTrainerSkills()
    {

        if ($this->existsKey(self::ACCESS_SKILL)) {
            $accessSkills  = $this->getData(self::ACCESS_SKILL);
        } else {
            $accessSkills = auth('api')->user()->accessSkills;
            $this->load(self::ACCESS_SKILL, $accessSkills);
        }

        return Response::success(
            __('skill.success_message.get_trainer_skills'),
            [
                'trainer_skills' => $accessSkills
            ]
        );
    }
    /**
     * The function syncSkills handles the synchronization of skills based on the provided request
     * data.
     *
     * @param Request request The `` parameter is an instance of the `Request` class, which
     * represents an HTTP request made to the server. It contains information about the request, such
     * as the request method, headers, and request data.
     *
     * @return a response with a success message and an array of responses.
     */
    public function syncSkills(Request $request)
    {
        $response = [];
        foreach ($request->skills as $item) {
            try {
                switch ($item['action']) {
                    case 'add':
                        $response[] = $this->skillService->handleAddAction($item['data']);
                        break;
                    case 'updated':
                        $response[] = $this->skillService->handleUpdateAction($item['data']);
                        break;
                    case 'delete':
                        $response[] = $this->skillService->handleDeleteAction($item['data']);
                        break;
                    default:
                        $response[] = Response::error(__('validation.custom.invalid_action'))->original;
                        break;
                }
            } catch (\Exception $e) {

                $response[] = Response::error($e->getMessage())->original;
            }
        }
        $this->forgetListKeys([
            self::ACCESS_SKILL,
            self::ADMIN_SKILLS_NOT_SHARED,
            self::ADMIN_SKILLS_SHARED,
            self::INDEX
        ]);
        return Response::success(__('skill.success_message.sync_successfully'), $response);
    }

   /**
    * The function translates a skill based on the provided language and skill ID, and returns a
    * success message along with the translated skill.
    *
    * @param Request request The `` parameter is an instance of the `Request` class, which is
    * used to handle HTTP requests in Laravel. It contains information about the current request, such
    * as the request method, URL, headers, and any data sent with the request.
    *
    * @return The code is returning a success response with a message and the translated skill.
    */
    public function translateSkill(Request $request)
    {
        $skill  = $this->skillService->translateSkill($request->get('lang'), $request->get('skill_id'));
        return Response::success(__('skill.success_message.sync_successfully'), $skill);
    }

    public function importSkills(ImportTFSRequest $request)
    {
        $userId = auth('api')->user()->id;
        foreach ($request->validated('files') as $file) {
            $TFSpath = \Storage::disk('public')->put('TFS', $file);
            $TFS = TFSSkills::create(['path' => $TFSpath]);
            dispatch(new ImportSkillJob($TFS->path,
                $request->validated('location'),
                $userId));

        }
        $this->forgetListKeys([
            self::ACCESS_SKILL,
            self::ADMIN_SKILLS_NOT_SHARED,
            self::ADMIN_SKILLS_SHARED,
            self::INDEX
        ]);
        return \Response::success('TFS proccessing');

    }
}
