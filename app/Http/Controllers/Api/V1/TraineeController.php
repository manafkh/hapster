<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Trainee\CreateTraineeRequest;
use App\Http\Requests\Trainee\UpdateTraineeRequest;
use App\Http\Resources\TraineeResource;
use App\Models\Trainee;
use App\Services\TraineeService;
use Illuminate\Http\Request;
use Response;

class TraineeController extends ApiController
{
    public function __construct(private TraineeService $traineeService)
    {
    }

    /**
     * The index function retrieves trainees based on the request and returns a success response with
     * the trainees as a collection.
     *
     * @param Request request The `` parameter is an instance of the `Illuminate\Http\Request`
     * class. It represents an incoming HTTP request and contains information such as the request
     * method, headers, query parameters, and request body.
     *
     * @return a success response with a message and an array of trainees. The trainees are being
     * transformed into a collection of TraineeResource objects.
     */
    public function index(Request $request)
    {
        $trainees = $this->traineeService->getTrainees($request);

        return Response::success(
            __('trainee.success_message.index'),
            [
                'trainees' => TraineeResource::collection($trainees),
            ]);
    }

    /**
     * The function creates a trainee using the validated trainee data, departments, and avatar
     * provided in the request, and returns a success response with the created trainee.
     *
     * @param CreateTraineeRequest request The  parameter is an instance of the
     * CreateTraineeRequest class. It is used to retrieve and validate the data sent in the request.
     *
     * @return a success response with a message and an array containing the created trainee.
     */
    public function create(CreateTraineeRequest $request)
    {
        $trainee = $this->traineeService
            ->create(
                $request->validatedTraineeData(),
                $request->validated('departments'),
                $request->validated('avatar')
            );

        return Response::success(
            __('trainee.success_message.create_successfully'),
            [
                'trainee' => $trainee,
            ],
        );
    }

    /**
     * The function updates a trainee's information and returns a success response with the updated
     * trainee data.
     *
     * @param Trainee trainee The "trainee" parameter is an instance of the Trainee class. It
     * represents the trainee that needs to be updated.
     * @param UpdateTraineeRequest request The `` parameter is an instance of the
     * `UpdateTraineeRequest` class. It is used to retrieve and validate the data sent in the request.
     * It contains methods like `validatedTraineeData()`, `validated('departments')`, and
     * `validated('avatar')` to retrieve the validated
     *
     * @return a success response with a message and an array containing the updated trainee data.
     */
    public function update(Trainee $trainee, UpdateTraineeRequest $request)
    {
        $trainee = $this->traineeService
            ->update(
                $trainee,
                $request->validatedTraineeData(),
                $request->validated('departments'),
                $request->validated('avatar')
            );

        return Response::success(
            __('trainee.success_message.update_successfully'),
            [
                'trainee' => $trainee,
            ],
        );
    }

    /**
     * The destroy function deletes a trainee and returns a success message.
     *
     * @param Trainee trainee The parameter `` is an instance of the `Trainee` model. It
     * represents the trainee that needs to be deleted from the database.
     *
     * @return a success response with the message "trainee.success_message.destroy_successfully".
     */
    public function destroy(Trainee $trainee)
    {
        $trainee->delete();

        return Response::success(
            __('trainee.success_message.destroy_successfully'),
        );
    }
    //TODO : split this method into add, update, delete methods
    //TODO : Add documentation for the request
    /**
     * The function syncTrainee handles multiple actions (add, update, delete) for trainees and returns
     * a response indicating the success or failure of each action.
     *
     * @param Request request The  parameter is an instance of the Request class, which is used
     * to retrieve data from the HTTP request. In this case, it is used to retrieve the trainees data
     * from the request.
     *
     * @return a response with a success message and an array of responses.
     */
    public function syncTrainee(Request $request)
    {
        $response = [];
        foreach ($request->trainees as $key => $item) {
            try {
                switch ($item['action']) {
                    case 'add':
                        $response[] = $this->traineeService->handleAddAction($item['data']);
                        break;
                    case 'updated':
                        $response[] = $this->traineeService->handleUpdateAction($item['data']);
                        break;
                    case 'delete':
                        $response[] = $this->traineeService->handleDeleteAction($item['data']);
                        break;
                    default:
                        $response[] = Response::error(__('validation.custom.invalid_action'))->original;
                        break;
                }
            } catch (\Exception $e) {
                $response[] = Response::error($e->getMessage())->original;
            }
        }

        return Response::success(__('trainee.success_message.sync_successfully'), $response);
    }
}
