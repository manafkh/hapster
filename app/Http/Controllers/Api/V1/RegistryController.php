<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Registry\GenerateRegistryTokenRequest;
use App\Http\Requests\Registry\RefreshRegistryTokenRequest;
use App\Services\RegistryService;
use Illuminate\Support\Facades\Response;

class RegistryController
{
    public function __construct(protected RegistryService $registryService)
    {
    }

    /**
     * The function generates a token and refresh token for device registration and returns a success
     * response with the generated tokens.
     *
     * @param GenerateRegistryTokenRequest request The  parameter is an instance of the
     * GenerateRegistryTokenRequest class. It is used to retrieve the 'identifier' value from the
     * request data.
     *
     * @return a response with a success message and an array containing the generated token and
     * refresh token.
     */
    public function generateToken(GenerateRegistryTokenRequest $request)
    {
        $getDeviceTokenResult = $this->registryService->getDeviceToken($request->get('identifier'));

        if ($getDeviceTokenResult->isFail()) {
            return Response::error($getDeviceTokenResult->getMessage(), $getDeviceTokenResult->getStatus());
        }

        $data = $getDeviceTokenResult->getData();

        return Response::success(
            __('auth.success_message.register_successfully'),
            [
                'token' => $data['token'],
                'refresh_token' => $data['refresh_token'],
            ]
        );
    }

    /**
     * The `refreshToken` function refreshes a device token and returns a success response with the new
     * token and refresh token.
     *
     * @param RefreshRegistryTokenRequest request The parameter `` is an instance of the
     * `RefreshRegistryTokenRequest` class. It is used to retrieve the refresh token from the request.
     *
     * @return a response. If the refreshDeviceTokenResult is a failure, it returns an error response
     * with the message and status from the result. If the refreshDeviceTokenResult is successful, it
     * returns a success response with the message "auth.success_message.register_successfully" and an
     * array containing the token and refresh_token from the data.
     */
    public function refreshToken(RefreshRegistryTokenRequest $request)
    {
        $refreshDeviceTokenResult = $this->registryService->refreshDeviceToken($request->get('refresh_token'));

        if ($refreshDeviceTokenResult->isFail()) {
            return Response::error($refreshDeviceTokenResult->getMessage(), $refreshDeviceTokenResult->getStatus());
        }

        $data = $refreshDeviceTokenResult->getData();

        return Response::success(
            __('auth.success_message.register_successfully'),
            [
                'token' => $data['token'],
                'refresh_token' => $data['refresh_token'],
            ]
        );
    }
}
