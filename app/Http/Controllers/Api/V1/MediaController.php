<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\MediaUploadRequest;

class MediaController extends Controller
{
    public function mediaUpload(MediaUploadRequest $request)
    {
        $mediaUrls = [];
        foreach ($request->validated('medias') as $media) {
            $path = \Storage::disk('temporary_folder')->putFile(
                $request->validated('uuid'),
                $media
            );

            $mediaUrls[] = \Storage::disk('temporary_folder')->url($path);
        }

        return \Response::success(
            'you media is upload',
            [
                'mediaUrls' => $mediaUrls,
            ]
        );
    }

}
