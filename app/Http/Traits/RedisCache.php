<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Cache;

trait RedisCache
{
    public function load($key, $data)
    {
        return Cache::forever($key, $data);
    }

    public function forget($key)
    {
        Cache::forget($key);
    }

    public function forgetListKeys($keys)
    {
        foreach ($keys as $key) {
            Cache::forget($key);
        }
    }

    public function existsKey($key)
    {
        if (Cache::has($key)) {
            return true;
        }

        return false;
    }

    public function getData($key)
    {
        return Cache::get($key);
    }
}
