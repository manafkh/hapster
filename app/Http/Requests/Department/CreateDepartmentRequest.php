<?php

namespace App\Http\Requests\Department;

use App\Models\Department;
use App\Rules\SkillsBelongsToParentDepartmentRule;
use Illuminate\Database\Query\Builder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @mixin Department
 */
class CreateDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'sort_order' => (Department::where('parent_id', $this->parent_id)->max('sort_order') ?? 0) + 1,
            'site_id' => auth('api')->user()->sites()->first()->id,
        ]);
    }

    public function rules(): array
    {

        return [
            'parent_id' => [
                'nullable',
                Rule::exists('departments', 'id')
                    ->where(fn (Builder $query) => $query->where('id', '!=', $this->id)->where('site_id', $this->site_id)),
            ],
            'name' => ['required', 'string', 'unique:departments,name'],
            'description' => ['nullable'],
            'sort_order' => ['nullable', 'integer'],
            'skills' => [
                'nullable',
                'array',
                Rule::exists('skills', 'id'),
            ],
            'skills.*' => ['integer'],
        ];
    }

    public function validatedDepartmentData()
    {
        return [
            'parent_id' => $this->parent_id,
            'name' => $this->name,
            'description' => $this->description,
            'sort_order' => $this->sort_order,
            'site_id' => $this->site_id,
        ];
    }
}
