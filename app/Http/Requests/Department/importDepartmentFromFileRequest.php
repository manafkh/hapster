<?php

namespace App\Http\Requests\Department;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\File;

class ImportDepartmentFromFileRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'file' => ['required', File::types(['xlsx', 'csv'])
                ->max(12 * 1024), ],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
