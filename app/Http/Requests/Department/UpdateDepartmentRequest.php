<?php

namespace App\Http\Requests\Department;

use App\Models\Department;
use App\Rules\SkillsBelongsToParentDepartmentRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @mixin Department
 */
class UpdateDepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'site_id' => auth('api')->user()->sites()->first()->id,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id' => ['nullable', 'exists:departments,id'],
            'parent_id' => [
                'nullable',
                Rule::exists('departments', 'id')
                    ->where(fn ($q) => $q->whereNot('id', $this->department['id'])->where('site_id', $this->site_id)),
            ],

            'name' => ['required', 'string', 'unique:departments,name,'.$this->department['id']],
            'description' => ['nullable'],
            'skills' => [
                'nullable',
                'array',
                Rule::exists('skills', 'id'),
            ],
            'skills.*' => ['integer'],
        ];
    }

    public function validatedDepartmentData()
    {
        return \Arr::whereNotNull([
            'parent_id' => $this->parent_id,
            'name' => $this->name,
            'description' => $this->description,
            'sort_order' => $this->sort_order,
            'site_id' => $this->site_id,
        ]);
    }
}
