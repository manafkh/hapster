<?php

namespace App\Http\Requests\Department;

use App\Rules\CheckDepartmentSameLevelRule;
use Illuminate\Foundation\Http\FormRequest;

class ReorderDepartmentRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        $this->merge([
            'department_sorted' => array_values(array_unique($this->department_sorted)),
        ]);
    }

    public function rules(): array
    {
        return [
            'department_sorted' => [
                'array', 'min:1', 'exists:departments,id,deleted_at,NULL',
                new CheckDepartmentSameLevelRule(),
            ],
            'department_sorted.*' => ['required', 'integer'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
