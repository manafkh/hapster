<?php

namespace App\Http\Requests\Training;

use App\Rules\AccessibleSkillToTrainingRule;
use App\Rules\BelongsToSkillTrainingRule;
use App\Rules\SigningNotBelongsToStageRule;
use App\Rules\TraineeBelongsToTrainingRule;
use App\Rules\UniqueDateTimeTrainingRule;
use App\Rules\UserBelongsToRoleAdminOrTrainer;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CreateTrainingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer', 'exists:users,id,deleted_at,NULL', new BelongsToSkillTrainingRule],
            'skill_id' => ['required', 'integer', new AccessibleSkillToTrainingRule],
            'trainees' => ['array', 'min:1'],
            'trainees.*' => ['exists:trainees,id,deleted_at,NULL', new TraineeBelongsToTrainingRule],
            'trainings' => ['array', 'min:1'],
            'trainings.*' => [new UniqueDateTimeTrainingRule],
            'trainings.*.stages' => ['array', 'min:1'],
            'trainings.*.stages.*' => [new SigningNotBelongsToStageRule],
            'trainings.*.stages.*.stage_id' => ['exists:stages,id,deleted_at,NULL'],
            'trainings.*.stages.*.signing_responsible' => [
                'required_if:trainings.*.stages.*.stage_id,==,4', 'nullable', new UserBelongsToRoleAdminOrTrainer,
            ],
            'trainings.*.date' => ['required', 'date', 'after_or_equal:'.Carbon::now()->format('y-m-d')],
            'trainings.*.from' => ['required', 'date_format:H:i'],
            'trainings.*.to' => ['required', 'date_format:H:i', 'after:trainings.*.from'],

        ];
    }

    public function validatedTrainingData(): array
    {
        return [
            'skill_id' => $this->validated('skill_id'),
            'user_id' => $this->validated('user_id'),
        ];
    }

    public function messages()
    {
        return [
            'trainings.*.stages.*.signing_responsible' => [
                'required_if' => __('validation.stage.signing_responsible_required'),
            ],
        ];
    }
}

/*
 * function (string $attribute, mixed $trainings, Closure $fail) {
    foreach ($trainings as $i => $train) {
        foreach ($train['stages'] as $j => $stage) {
            if ($stage['stage_id'] == 4 && !isset($stage['signing_responsible'])) {
                $fail("The trainings.' . $i . 'stages." . $j . ".signing_responsible is required");
            } elseif ($stage['stage_id'] != 4 ){
                return $this->except('trainings.' . $i . '.stages.' . $j . '.signing_responsible');
            }
        }
    }

}*/
