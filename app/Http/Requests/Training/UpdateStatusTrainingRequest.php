<?php

namespace App\Http\Requests\Training;

use App\Enums\TrainingStatusEnum;
use App\Rules\CheckIfRequiredFileSignatureRule;
use App\Rules\CheckMediaExistRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateStatusTrainingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'training_id' => ['required', 'exists:trainings,id,deleted_at,NULL'],
            'status' => ['required', new Enum(TrainingStatusEnum::class)],


            'responsible_signature' => [new CheckIfRequiredFileSignatureRule(), 'nullable'],
            'responsible_signature.url' => ['required_with:responsible_signature', 'string', 'url', new
            CheckMediaExistRule()],

            'responsible_signature_date'=>['nullable'],

            'trainee_signature' => [new CheckIfRequiredFileSignatureRule(), 'nullable'],
            'trainee_signature.url' => ['required_with:trainee_signature', 'nullable', 'string', 'url', new
            CheckMediaExistRule()],
            'trainer_signature_date'=>['required_with:trainee_signature', 'nullable'],
        ];
    }


    public function validatedSignatureFiles()
    {
        return \Arr::whereNotNull([
            'responsible_signature' => $this->responsible_signature,
            'trainee_signature' => $this->trainee_signature,
        ]);
    }
}
