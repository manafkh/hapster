<?php

namespace App\Http\Requests\Training;

use App\Rules\AccessibleSkillToTrainingRule;
use App\Rules\BelongsToSkillTrainingRule;
use App\Rules\SigningNotBelongsToStageRule;
use App\Rules\TraineeBelongsToTrainingRule;
use App\Rules\UpdateUniqueDateTimeTrainingRule;
use App\Rules\UserBelongsToRoleAdminOrTrainer;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class UpdateTrainingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer', 'exists:users,id,deleted_at,NULL', new BelongsToSkillTrainingRule],
            'skill_id' => ['required', 'integer', new AccessibleSkillToTrainingRule],
            'trainees' => ['array', 'min:1'],
            'trainees.*' => ['exists:trainees,id,deleted_at,NULL', new TraineeBelongsToTrainingRule()],
            'stages' => ['array', 'min:1'],
            'stages.*' => [new SigningNotBelongsToStageRule],
            'stages.*.stage_id' => ['exists:stages,id,deleted_at,NULL'],
            'stages.*.signing_responsible' => [
                'required_if:stages.*.stage_id,==,4', 'nullable', new UserBelongsToRoleAdminOrTrainer,
            ],

            'date' => ['required', 'date', new UpdateUniqueDateTimeTrainingRule, 'after_or_equal:'.Carbon::now()->format('y-m-d')],
            'from' => ['required', 'date_format:H:i'],
            'to' => ['required', 'date_format:H:i', 'after:trainings.*.from'],
        ];
    }

    public function messages()
    {
        return [
            'trainings.*.stages.*.signing_responsible' => [
                'required_if' => __('validation.stage.signing_responsible_required'),
            ],
        ];
    }
}
