<?php

namespace App\Http\Requests\Trainee;

use App\Models\Trainee;
use App\Rules\CheckMediaExistRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @mixin Trainee
 */
class UpdateTraineeRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['nullable', 'exists:trainees,id,deleted_at,NULL'],
            'first_name' => ['nullable', 'max:50'],
            'last_name' => ['nullable', 'max:50'],
            'phone_number' => ['nullable', 'max:20'],
            'trainee_identify' => [
                'nullable',
                'max:50',
                Rule::unique('trainees', 'trainee_identify')
                    ->ignore($this->trainee['id']),
            ],
            'email' => ['nullable', 'email', 'max:50', 'unique:users,email',
                Rule::unique('trainees', 'email')
                    ->ignore($this->trainee['id']),
            ],

            'departments' => ['array', 'min:1', 'exists:departments,id,deleted_at,NULL'],
            'departments.*' => ['integer'],

            'avatar' => ['nullable'],
            'avatar.url' => ['nullable', 'string', 'url', new CheckMediaExistRule()],
        ];
    }

    public function validatedTraineeData(): array
    {
        return \Arr::whereNotNull([
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone_number' => $this->phone_number,
            'trainee_identify' => $this->trainee_identify,
            'email' => $this->email,
        ]);
    }

    public function authorize(): bool
    {
        return true;
    }
}
