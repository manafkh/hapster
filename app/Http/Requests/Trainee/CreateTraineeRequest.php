<?php

namespace App\Http\Requests\Trainee;

use App\Models\Trainee;
use App\Rules\CheckMediaExistRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @mixin Trainee
 */
class CreateTraineeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'first_name' => ['required', 'max:50'],
            'last_name' => ['required', 'max:50'],
            'phone_number' => ['required', 'max:20'],
            'trainee_identify' => ['required', 'max:50', 'unique:trainees,trainee_identify'],
            'email' => ['required', 'unique:trainees,email', 'email', 'max:50', 'unique:users,email'],
            'departments' => ['array', 'min:1', 'exists:departments,id,deleted_at,NULL'],
            'departments.*' => ['integer'],

            'avatar' => ['nullable'],
            'avatar.url' => ['nullable', 'string', 'url', new CheckMediaExistRule()],
        ];
    }

    public function validatedTraineeData()
    {
        return [
            'user_id' => auth('api')->user()->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone_number' => $this->phone_number,
            'trainee_identify' => $this->trainee_identify,
            'email' => $this->email,
            'avatar' => $this->avatar,
        ];
    }
}
