<?php

namespace App\Http\Requests\Skill;

use App\Rules\TFSFileExtensionValidator;
use Illuminate\Foundation\Http\FormRequest;

class ImportTFSRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'files' => ['array', 'min:1'],
            'files.*' => ['file', new TFSFileExtensionValidator],
            'location' => ['required', 'string', 'max:250'],
        ];
    }
}
