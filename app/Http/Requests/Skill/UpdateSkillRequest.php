<?php

namespace App\Http\Requests\Skill;

use App\Enums\KeypointTypeEnum;
use App\Enums\SkillTypeEnum;
use App\Models\Keypoint;
use App\Models\Skill;
use App\Rules\CheckMediaExistRule;
use App\Rules\IsKeypointBelongsToSkillsRule;
use App\Rules\IsStepBelongsToSkillsRule;
use App\Rules\KeypointRequiredForFirstStepRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

/**
 * @mixin Skill
 */
class UpdateSkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id' => ['nullable', 'exists:skills,id'],
            'name' => ['required', 'string', 'max: 255'],

            'image' => ['nullable'],
            'image.id' => ['nullable', 'integer'],
            'image.url' => ['nullable', 'string', 'url', new CheckMediaExistRule()],

            'confidentiality_level' => ['required', 'numeric', 'min:1', 'max:5'],
            'type' => ['required', new Enum(SkillTypeEnum::class)],
            'writing_date' => ['required', 'date'],
            'author' => ['required', 'string', 'max: 255'],
            'location' => ['required', 'string'],
            'importance' => ['required', 'string'],
            'importance' => ['nullable', 'string'],
            'required_skill' => ['nullable', 'string'],
            'required_safety_equipments' => ['nullable', 'string'],
            'required_materials_parts' => ['nullable', 'string'],
            'tools_supplies_machinery' => ['nullable', 'string'],

            'methods_and_documentation.add' => ['array'],
            'methods_and_documentation.add.*.id' => ['nullable', 'integer'],
            'methods_and_documentation.add.*.url' => ['nullable', 'string', 'url', new CheckMediaExistRule()],
            'methods_and_documentation.delete' => ['nullable', 'array'],
            'methods_and_documentation.delete.*' => ['nullable'],

            //###### Step #######
            'steps' => ['required', 'array', 'min:1'],
            'steps.*.id' => [
                'nullable',
                'integer',
                'exists:steps,id,deleted_at,NULL',
                new IsStepBelongsToSkillsRule($this->skill['id']),
            ],
            'steps.*.name' => ['required', 'string', 'max:255'],
            'steps.*.sort_order' => ['required', 'numeric'],

            //###### Keypoint #######
            'steps.*.keypoints' => ['nullable', 'array'],
            'steps.*.keypoints.*.id' => [
                'nullable',
                'integer',
                'exists:keypoints,id,deleted_at,NULL',
                new IsKeypointBelongsToSkillsRule($this->skill['id']),
            ],
            'steps.*.keypoints.*.step_id' => ['nullable', 'integer', 'exists:steps,id,deleted_at,NULL'],
            'steps.*.keypoints.*.name' => ['required', 'string', 'max:255'],
            'steps.*.keypoints.*.sort_order' => ['required', 'integer'],
            'steps.*.keypoints.*.reason' => ['required', 'string'],
            'steps.*.keypoints.*.type' => ['required', new Enum(KeypointTypeEnum::class)],

            'steps.*.keypoints.*.media.add' => [
                'nullable', 'array','max:'.config('app.max_keypoint_attachments_count')
            ],
            'steps.*.keypoints.*.media.add.*.id' => ['nullable', 'integer'],
            'steps.*.keypoints.*.media.add.*.url' => ['nullable', 'string', 'url', new CheckMediaExistRule()],
            'steps.*.keypoints.*.media.delete' => ['nullable', 'array'],
            'steps.*.keypoints.*.media.delete.*' => ['nullable',],

            'delete_step_ids' => ['array', new IsStepBelongsToSkillsRule($this->skill['id'])],
            'delete_step_ids.*' => ['integer'],
            'delete_keypoint_ids' => ['array', new IsKeypointBelongsToSkillsRule($this->skill['id'])],
            'delete_keypoint_ids.*' => ['integer'],
        ];
    }

    public function validatedSkillData(): array
    {
        return \Arr::whereNotNull([
            'id' => ['nullable', 'exists:skills,id'],
            'name' => $this->name,
            'confidentiality_level' => $this->confidentiality_level,
            'type' => $this->type,
            'writing_date' => $this->writing_date,
            'author' => $this->author,
            'location' => $this->location,
            'importance' => $this->importance,
            'required_skill' => $this->required_skill,
            'required_safety_equipments' => $this->required_safety_equipments,
            'required_materials_parts' => $this->required_materials_parts,
            'tools_supplies_machinery' => $this->tools_supplies_machinery,

            'user_id' => auth('api')->user()->id,
        ]);
    }
}
