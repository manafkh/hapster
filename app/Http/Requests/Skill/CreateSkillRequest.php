<?php

namespace App\Http\Requests\Skill;

use App\Enums\KeypointTypeEnum;
use App\Enums\SkillTypeEnum;
use App\Models\Skill;
use App\Rules\CheckMediaExistRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

/**
 * @mixin Skill
 */
class CreateSkillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max: 255'],
            'user_id' => ['required', 'exists:users,id'],

            'image' => ['nullable'],
            'image.url' => ['nullable', 'string', 'url', new CheckMediaExistRule()],

            'confidentiality_level' => ['required', 'numeric', 'min:1', 'max:5'],
            'type' => ['required', new Enum(SkillTypeEnum::class)],
            'writing_date' => ['required', 'date'],
            'author' => ['required', 'string', 'max: 255'],
            'location' => ['required', 'string'],
            'importance' => ['nullable', 'string'],
            'required_skill' => ['nullable', 'string'],
            'required_safety_equipments' => ['nullable', 'string'],
            'required_materials_parts' => ['nullable', 'string'],
            'tools_supplies_machinery' => ['nullable', 'string'],

            'methods_and_documentation.add' => ['array'],
            'methods_and_documentation.add.*.id' => ['nullable', 'integer'],
            'methods_and_documentation.add.*.url' => ['nullable', 'string', 'url', new CheckMediaExistRule()],

            'steps' => ['required', 'array', 'min:1'],
            'steps.*.name' => ['required', 'string', 'max:255'],
            'steps.*.sort_order' => ['required', 'numeric'],
            'steps.*.keypoints' => ['nullable', 'array'],

            'steps.*.keypoints.*.name' => ['required', 'string', 'max:255'],
            'steps.*.keypoints.*.sort_order' => ['required', 'integer'],
            'steps.*.keypoints.*.reason' => ['required', 'string'],
            'steps.*.keypoints.*.type' => ['required', new Enum(KeypointTypeEnum::class)],

            'steps.*.keypoints.*.media.add' => ['nullable', 'array', 'min:1',
            'max:'.config('app.max_keypoint_attachments_count')],
            'steps.*.keypoints.*.media.add.*.id' => ['nullable', 'integer'],
            'steps.*.keypoints.*.media.add.*.url' => ['nullable', 'string', 'url', new CheckMediaExistRule()],
        ];
    }

    public function validatedSkillData(): array
    {

        return [
            'name' => $this->name,
            'confidentiality_level' => $this->confidentiality_level,
            'type' => $this->type,
            'writing_date' => $this->writing_date,
            'author' => $this->author,
            'location' => $this->location,
            'importance' => $this->importance,
            'required_skill' => $this->required_skill,
            'required_safety_equipments' => $this->required_safety_equipments,
            'required_materials_parts' => $this->required_materials_parts,
            'tools_supplies_machinery' => $this->tools_supplies_machinery,

            'user_id' => $this->user_id,
        ];
    }
}
