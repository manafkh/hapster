<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MediaUploadRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'uuid' => ['required', 'string'],
            'medias' => ['required', 'array', 'min:1'],
            'medias.*' => ['required', 'file'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
