<?php

namespace App\Http\Requests\Auth;

use App\Rules\LoginFieldExistsRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class LoginUserRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email' => ['required', 'lowercase', 'max:254', new LoginFieldExistsRule()],
            'password' => ['required'],
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function validatedData()
    {
        return [
            'email' => Str::lower($this->email),
            'password' => $this->password,
        ];
    }
}
