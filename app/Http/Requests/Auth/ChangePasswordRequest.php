<?php

namespace App\Http\Requests\Auth;

use App\Rules\CheckOldPasswordKeycloakRule;
use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'current_password' => [...config('auth.password_regex'), new CheckOldPasswordKeycloakRule],
            'new_password' => [...config('auth.password_regex'), 'confirmed'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
