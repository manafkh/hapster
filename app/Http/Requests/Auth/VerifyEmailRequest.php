<?php

namespace App\Http\Requests\Auth;

use App\Rules\CheckOldPasswordKeycloakRule;
use Illuminate\Foundation\Http\FormRequest;

class VerifyEmailRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'otp' => ['required', 'string'],
            'email' => ['required'],
            'password' => ['required'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
