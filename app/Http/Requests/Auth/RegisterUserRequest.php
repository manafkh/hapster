<?php

namespace App\Http\Requests\Auth;

use App\Rules\CheckMediaExistRule;
use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    public function rules()
    {
        return [
            'first_name' => ['required', 'string', 'max:254'],
            'last_name' => ['required', 'string', 'max:254'],
            'email' => ['required', 'lowercase', 'email', 'max:254', 'unique:users,email'],
            'username' => ['required', 'lowercase', 'unique:users,username', 'max:254'],
            'phone_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:8', 'max:20'],
            'avatar' => ['required'],
            'avatar.url' => ['required', 'string', 'url', new CheckMediaExistRule()],
            'password' => [...config('auth.password_regex'), 'confirmed'],

            'sites' => ['required', 'array', 'max:1'],
            'sites.*' => ['required', 'integer', 'exists:sites,id,deleted_at,NULL'],
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function validatedUserData()
    {
        return [
            'first_name' => $this->validated('first_name'),
            'last_name' => $this->validated('last_name'),
            'email' => $this->validated('email'),
            'username' => $this->validated('username'),
            'phone_number' => $this->validated('phone_number'),
            'password' => $this->validated('password'),
            'avatar' => $this->validated('avatar'),
        ];
    }
}
