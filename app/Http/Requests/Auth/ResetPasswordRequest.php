<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ResetPasswordRequest extends FormRequest
{
    public function rules(Request $request)
    {
        return [
            'token' => 'required',
            'email' => 'required|email|exists:users,email,deleted_at,NULL',
            'password' => [...config('auth.password_regex'), 'confirmed'],
        ];
    }

    public function authorize()
    {
        return true;
    }

    public function validatedData()
    {
        return [
            'email' => $this->validated('email'),
            'password' => $this->validated('password'),
            'password_confirmation' => $this->validated('password_confirmation'),
            'token' => $this->validated('token'),
        ];
    }
}
