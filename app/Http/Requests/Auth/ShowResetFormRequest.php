<?php

namespace App\Http\Requests\Auth;

use App\Rules\LoginFieldExistsRule;
use Illuminate\Foundation\Http\FormRequest;

class ShowResetFormRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => ['required', 'lowercase', 'max:254', new LoginFieldExistsRule()],

        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
