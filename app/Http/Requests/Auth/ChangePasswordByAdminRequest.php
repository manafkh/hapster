<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class ChangePasswordByAdminRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'new_password' => [...config('auth.password_regex'), 'confirmed'],
            'is_reset'=>['boolean']
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
