<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SendResetLinkEmailRequest extends FormRequest
{
    public function rules(Request $request): array
    {
        return [
            'email' => [
                'required',
                'lowercase',
                'max:254',
                'email',
                'exists:users,email,deleted_at,NULL'],
        ];
    }

    public function authorize(): bool
    {
        return true;
    }
}
