<?php

namespace App\Http\Requests\User;

use App\Enums\UserRoleEnum;
use App\Models\User;
use App\Rules\CheckMediaExistRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @mixin User
 */
class UpdateTrainerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id' => ['nullable', 'exists:users,id,deleted_at,NULL'],
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:8', 'max:20'],
            'avatar' => ['nullable'],
            'avatar.id' => ['nullable', "integer"],
            'avatar.url' => ['nullable', 'string', 'url', new CheckMediaExistRule()],
            'is_reset' => ['nullable', 'boolean'],

            'skills' => ['nullable', 'array','exists:skills,id,deleted_at,NULL'],
            'skills.*' => ['integer'],

            'departments' => ['required', 'array', 'min:1', 'exists:departments,id,deleted_at,NULL'],
            'departments.*' => ['integer'],

            'roles.*' => [
                'required',
                'integer',
            ],

            'roles' => [
                'required',
                'array',
                'min:1',
                Rule::exists('roles', 'id'),
                function (string $attribute, mixed $value, \Closure $fail) {
                    //TODO : improve to be Rule class
                    if (!in_array(UserRoleEnum::Trainer->value, $value)) {
                        $fail('You must use another API for the non-trainer role');
                    }
                },
            ],
        ];
    }

    public function validatedUserData()
    {
        return \Arr::whereNotNull([
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone_number' => $this->phone_number,
            'avatar' => $this->avatar,
        ]);
    }
}
