<?php

namespace App\Http\Requests\User;

use App\Enums\UserRoleEnum;
use App\Models\User;
use App\Rules\CheckMediaExistRule;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @mixin User
 */
class CreateTrainerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string', 'max:254'],
            'last_name' => ['required', 'string', 'max:254'],
            'email' => ['required', 'lowercase', 'email', 'max:254', 'unique:users,email'],
            'username' => ['required', 'lowercase', 'unique:users,username', 'max:254'],
            'phone_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:8', 'max:20'],
            'avatar' => ['required'],
            'avatar.url' => ['required', 'string', 'url', new CheckMediaExistRule()],
            'password' => [...config('auth.password_regex'), 'confirmed'],
            'is_reset' => ['required', 'boolean'],

            'skills' => ['nullable', 'array', 'exists:skills,id,deleted_at,NULL'],
            'skills.*' => ['integer'],

            'departments' => ['required', 'array', 'min:1', 'exists:departments,id,deleted_at,NULL'],
            'departments.*' => ['integer'],

            'roles.*' => [
                'required',
                'integer',
            ],

            'roles' => [
                'required',
                'array',
                'min:1',
                Rule::exists('roles', 'id'),
                function (string $attribute, mixed $value, Closure $fail) {
                    //TODO : improve to be Rule class
                    if (!in_array(UserRoleEnum::Trainer->value, $value)) {
                        $fail('You must use another API for the non-trainer role');
                    }
                },
            ],
        ];
    }

    public function validatedUserData()
    {
        return [
            'username' => $this->username,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'phone_number' => $this->phone_number,
            'is_reset' => $this->is_reset,
            'password' => $this->password,
            'avatar' => $this->avatar,
        ];
    }
}
