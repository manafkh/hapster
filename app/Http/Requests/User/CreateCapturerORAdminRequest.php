<?php

namespace App\Http\Requests\User;

use App\Enums\UserRoleEnum;
use App\Rules\AllowUserRoleRule;
use App\Rules\CheckMediaExistRule;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class CreateCapturerORAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'first_name' => ['required', 'string', 'max:254'],
            'last_name' => ['required', 'string', 'max:254'],
            'email' => ['required', 'lowercase', 'email', 'max:254', 'unique:users,email'],
            'username' => ['required', 'lowercase', 'unique:users,username', 'max:254'],
            'phone_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:8', 'max:20'],
            'avatar' => ['required'],
            'avatar.url' => ['required', 'string', 'url', new CheckMediaExistRule()],
            'password' => [...config('auth.password_regex'), 'confirmed'],

            'is_reset' => ['required', 'boolean'],
            'roles' => [
                'array', 'min:1',
                new AllowUserRoleRule(
                    UserRoleEnum::Admin->value,
                    UserRoleEnum::Capturer->value,
                ),
            ],
            'roles.*' => [
                'required',
                'integer',
            ],
        ];
    }

    public function validatedUserData()
    {
        return [
            'username' => $this->username,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone_number' => $this->phone_number,
            'email' => $this->email,
            'password' => $this->password,
            'is_reset' => $this->is_reset,
            'avatar' => $this->avatar,
        ];
    }
}
