<?php

namespace App\Http\Requests\User;

use App\Enums\UserRoleEnum;
use App\Models\User;
use App\Rules\AllowUserRoleRule;
use App\Rules\CheckMediaExistRule;
use Arr;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @mixin User
 */
class UpdateCapturerOrAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id' => ['nullable', 'exists:users,id,deleted_at,NULL'],
            'first_name' => ['required'],
            'last_name' => ['required'],
            'phone_number' => ['required', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'min:8', 'max:20'],
            'is_reset' => ['nullable', 'boolean'],
            'avatar' => ['nullable'],
            'avatar.id' => ['nullable', "integer"],
            'avatar.url' => ['nullable', 'string', 'url', new CheckMediaExistRule()],

            'roles' => [
                'array', 'min:1',
                new AllowUserRoleRule(
                    UserRoleEnum::Admin->value,
                    UserRoleEnum::Capturer->value,
                ),
            ],
            'roles.*' => [
                'required',
                'integer',
            ],
        ];
    }

    public function validatedUserData(): array
    {
        return Arr::whereNotNull([
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'phone_number' => $this->phone_number,
            'is_reset' => $this->is_reset,
            'avatar' => $this->avatar,
        ]);
    }
}
