<?php

namespace App\Providers;

use App\Exceptions\AuthInvalidCredentialsException;
use App\Exceptions\KeycloakException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\ServiceProvider;

class KeycloakServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Http::macro('keycloak', function () {
            return Http::withOptions([
                'verify' => false,
            ])
                ->baseUrl(config('keycloak.url'))
                ->timeout(2)
                ->throw(
                    function (Response $res) {
                        if ($res->clientError()) {
                            if ($res->status() == 409) {
                                throw KeycloakException::clientError(
                                    $res->json('errorMessage'),
                                    \Illuminate\Http\Response::HTTP_CONFLICT
                                );
                            } elseif ($res->status() == 400) {
                                //   "Token is not active"
                                //   "Session not active"
                                throw KeycloakException::clientError(
                                    $res->json('error_description'),
                                    \Illuminate\Http\Response::HTTP_BAD_REQUEST
                                );
                            } elseif ($res->status() == 401) {
                                //   "Invalid user credentials"
                                throw new AuthInvalidCredentialsException(
                                    __('validation.current_password'),
                                    \Illuminate\Http\Response::HTTP_UNAUTHORIZED
                                );
                            } elseif ($res->status() == 404) {
                                throw KeycloakException::clientError(
                                    $res->json('error'),
                                    \Illuminate\Http\Response::HTTP_NOT_FOUND
                                );
                            } else {
                                dd(
                                    $res->status(),
                                    $res->body()
                                );
                            }
                        }
                        if ($res->serverError()) {
                            dd(
                                'serverError',
                                $res->status(),
                                $res->body()
                            );
                            throw KeycloakException::serverError();
                        }
                        if ($res->failed()) {
                            dd(
                                'serverError',
                                $res->status(),
                                $res->body()
                            );
                            throw new KeycloakException('something went wrong in keycloak');
                        }
                    }
                );
        });
    }
}
