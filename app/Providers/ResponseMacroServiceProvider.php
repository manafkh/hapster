<?php

namespace App\Providers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\ServiceProvider;
use Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Response::macro(
            'success',
            function ($message = null, $content = [], $status = 200): JsonResponse {
                return Response::json([
                    'message' => $message,
                    'content' => $content,
                    'status' => $status,
                ], $status);
            }
        );

        Response::macro(
            'error',
            function ($message, int $status = 500): JsonResponse {
                return Response::json([
                    'message' => $message,
                    'status' => $status,
                ], $status);
            }
        );

        Response::macro(
            'errorValidation',
            function ($validator): JsonResponse {
                return Response::json([
                    'message' => $validator->errors()->first(),
                    'errors' => $validator->errors(),
                    'status' => 422,
                ], 422);
            }
        );
    }
}
