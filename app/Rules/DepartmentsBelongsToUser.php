<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class DepartmentsBelongsToUser implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        $user = auth('api')->user();
        if ($user->hasRole('ADMIN')) {

            $departments = $user->departmentsBelongsToUserSites()->get()->pluck('id')->toArray();

        } else {
            $departments = $user->departments->pluck('id')->toArray();
        }
        if (! in_array($value, $departments)) {
            $fail(__('validation.trainee.not_access_department'));
        }
    }
}
