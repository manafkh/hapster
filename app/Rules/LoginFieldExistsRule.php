<?php

namespace App\Rules;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class LoginFieldExistsRule implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (! User::where('username', $value)
            ->orWhere('email', $value)->exists()) {

            $fail(__('auth.email_or_username_not_found'));
        }
    }
}
