<?php

namespace App\Rules;

use App\Models\Stage;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class SigningNotBelongsToStageRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $signatureStageID = Stage::where('name', 'Signature')->first()?->id;

        if (isset($value['signing_responsible']) && $signatureStageID != $value['stage_id']) {
            $fail(__('validation.stage.this_stage_cannot_signing'));
        }
    }
}
