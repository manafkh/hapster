<?php

namespace App\Rules;

use App\Models\Department;
use App\Models\Skill;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class SkillsBelongsToParentDepartmentRule implements ValidationRule
{
    public function __construct(protected ?int $parentDepartmentId)
    {
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        if (! empty($this->parentDepartmentId) && $parentDepartment = Department::find($this->parentDepartmentId)) {

            $parentDepartmentSkills = $parentDepartment->skills()->allRelatedIds()->toArray();

            $skillsIdNotFoundInParentDepartment = array_diff($value, $parentDepartmentSkills);

            if (
                Skill::whereIn('id', $value)->count() !== count($value) ||
                count($skillsIdNotFoundInParentDepartment) > 0
            ) {
                $fail(__(
                    'validation.skill.must_belong_to_parent_department',
                    ['skill_ids' => implode(', ', $skillsIdNotFoundInParentDepartment)]
                ));
            }
        }
    }
}
