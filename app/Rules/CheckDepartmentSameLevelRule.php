<?php

namespace App\Rules;

use App\Models\Department;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CheckDepartmentSameLevelRule implements ValidationRule
{
    public function validate(string $attribute, mixed $departmentsIds, Closure $fail): void
    {
        $site_id = auth('api')->user()->sites()->first()->id;
        $departmentsParentIds = Department::whereIn('id', $departmentsIds)
        ->where('site_id',$site_id)->pluck('parent_id')->toArray();
        $departmentsParentId = $departmentsParentIds[0];

        // replace null value in array in 0 value for array_count_values not support null
        array_walk(
            $departmentsParentIds,
            fn (&$value) => $value = $value === null ? 0 : $value
        );

        if (array_count_values($departmentsParentIds)[$departmentsParentIds[0]] != count($departmentsParentIds)) {
            $fail(__('validation.custom.department_ids_not_same_level'));
        }

        $countDepartmentsInSameLevel = Department::where('parent_id', $departmentsParentId)
        ->where('site_id',$site_id)->count();

        if (count($departmentsParentIds) != $countDepartmentsInSameLevel) {
            $fail(__('validation.custom.should_send_all_department_ids_for_this_level'));
        }
    }
}
