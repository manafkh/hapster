<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class KeypointRequiredForFirstStepRule implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        //TODO : test
        if (is_array($value) && ! empty($value)) {
            $keyPointForFirstStep = $value[0]['keypoints'];
            if (! isset($keyPointForFirstStep) || empty($keyPointForFirstStep)) {
                $fail(__('validation.custom.first_step_must_has_at_least_one_keypoint'));
            }
        }
    }
}
