<?php

namespace App\Rules;

use App\Models\Training;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class UpdateUniqueDateTimeTrainingRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        $uniqueTimeTraining = Training::where('user_id', '=', Request()->get('user_id'))
            ->where('date', '=', $value)
            ->where('from', '<=', Request()->get('from'))
            ->where('to', '>=', Request()->get('from'))
            ->where('id', '!=', Request()->route()->training->id)
            ->exists();

        if ($uniqueTimeTraining) {
            $fail(__('validation.training.unique_time_training'));

        }
    }
}
