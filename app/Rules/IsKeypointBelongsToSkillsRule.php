<?php

namespace App\Rules;

use App\Models\Skill;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class IsKeypointBelongsToSkillsRule implements ValidationRule
{
    public function __construct(private int $skillId)
    {

    }

    public function validate(string $attribute, mixed $keypointIds, Closure $fail): void
    {
        if (empty($keypointIds)) {
            return;
        }

        !is_array($keypointIds) && $keypointIds = [$keypointIds];

        $count = count($keypointIds);

        $belongs = Skill::query()
            ->whereHas('steps.keypoints',
                function ($query) use ($keypointIds) {
                    $query->whereIn('id', $keypointIds);
                }, '>=', $count)
            ->where('id', $this->skillId)->exists();

        if (!$belongs) {
            $fail(__('validation.custom.keypoint_not_belong_to_skill'));
        }
    }
}
