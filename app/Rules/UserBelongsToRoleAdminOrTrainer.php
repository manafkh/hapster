<?php

namespace App\Rules;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class UserBelongsToRoleAdminOrTrainer implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $isAdminOrTrainer = User::Role(['Admin', 'Trainer'])->where('id', $value)->exists();

        if (! $isAdminOrTrainer) {
            $fail(__('validation.training.this_user_not_belongsTo_admin_or_trainer'));
        }
    }
}
