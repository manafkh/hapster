<?php

namespace App\Rules;

use App\Exceptions\MediaUploadIsNotExistException;
use App\Services\MediaService;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CheckMediaExistRule implements ValidationRule
{

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        try {
            if (isset($value) && (!str_contains($value, "media"))) {

                $filePath = MediaService::getFilePathByDisk($value);

                MediaService::checkMediaExist($filePath);

            }


        } catch (MediaUploadIsNotExistException|\Exception $e) {

            $fail(__('exception.check_media_uploaded'));
        }
    }
}
