<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class AllowUserRoleRule implements ValidationRule
{
    private $roles;

    public function __construct(...$roles)
    {
        $this->roles = $roles;
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (is_array($value) && is_array($this->roles)) {
            $difference = array_diff($value, $this->roles);

            if (! empty($difference)) {
                $fail(__('validation.custom.user_role_error',
                    ['role_ids' => implode(' or ', $this->roles)]));
            }
        }

    }
}
