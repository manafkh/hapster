<?php

namespace App\Rules;

use Auth;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class BelongsToUserSiteRule implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $user = Auth::guard('api')->user(); // Get the currently authenticated user

        if (! $user->sites()->where('sites.id', $value)->exists()) {
            $fail(__('validation.site.must_belong_to_user'));
        }
    }
}
