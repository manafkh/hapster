<?php

namespace App\Rules;

use App\Enums\TrainingStatusEnum;
use App\Models\Stage;
use App\Models\Training;
use Closure;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\ValidationRule;

class CheckIfRequiredFileSignatureRule implements DataAwareRule, ValidationRule
{

    public $implicit = true;

    protected $data = [];

    public function setData(array $data): static
    {
        $this->data = $data;

        return $this;
    }

    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        if ($this->data['status'] != TrainingStatusEnum::DONE->value && isset($value)) {
            $fail(__('validation.accepted_if', ['attribute' => $attribute, 'other' => 'training has stages', 'value' => 'Signature']));
        }

        $training = Training::find($this->data['training_id']);
        $trainingHasSignature = $training->stages()->where('name', 'Signature')->exists();

        if ($trainingHasSignature && !isset($value)) {
            $fail(__('validation.required', ['attribute' => $attribute]));
        }

    }
}
