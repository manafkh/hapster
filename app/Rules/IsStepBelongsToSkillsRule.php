<?php

namespace App\Rules;

use App\Models\Skill;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class IsStepBelongsToSkillsRule implements ValidationRule
{
    public function __construct(private int $skillId)
    {

    }

    public function validate(string $attribute, mixed $stepIds, Closure $fail): void
    {

        if (empty($stepIds)) {
            return;
        }

        !is_array($stepIds) && $stepIds = [$stepIds];

        $count = count($stepIds);

        $belongs = Skill::query()
            ->whereHas('steps',
                function ($query) use ($stepIds) {
                    $query->whereIn('id', $stepIds);
                }, '>=', $count)
            ->where('id', $this->skillId)->exists();

        if (!$belongs) {
            $fail(__('validation.custom.step_not_belong_to_skill'));
        }
    }
}
