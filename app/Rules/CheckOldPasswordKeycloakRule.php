<?php

namespace App\Rules;

use App\Exceptions\AuthInvalidCredentialsException;
use App\Services\Keycloak\KeycloakService;
use Auth;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CheckOldPasswordKeycloakRule implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        try {
            if (Auth::guard('api')->check()) {
                $user = Auth::guard('api')->user();
                app(KeycloakService::class)->login($user->email, $value);
            }
        } catch (AuthInvalidCredentialsException $e) {
            $fail(__('validation.current_password'));
        }
    }
}
