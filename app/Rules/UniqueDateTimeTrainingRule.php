<?php

namespace App\Rules;

use App\Enums\TrainingStatusEnum;
use App\Models\Training;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class UniqueDateTimeTrainingRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        $uniqueTimeTraining = Training::where('user_id', '=', Request()->get('user_id'))
            ->where('status', TrainingStatusEnum::SCHEDULED)
            ->where('date', '=', $value['date'])
            ->where('from', '<=', $value['from'])
            ->where('to', '>=', $value['from'])->exists();
        if ($uniqueTimeTraining) {
            $fail(__('validation.training.unique_time_training'));

        }
    }
}
