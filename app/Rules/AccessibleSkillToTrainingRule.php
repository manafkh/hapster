<?php

namespace App\Rules;

use App\Models\Requests;
use App\Models\User;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class AccessibleSkillToTrainingRule implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {

        $user = User::find(Request()->get('user_id'));
        if ($user == null) {
            $fail(__('validation.training.user_not_exists'));
        } else {
            if ($user->hasRole('Admin')) {
                $accessibleSkillsIDs = $user->adminOrCaptureSkills()->pluck('id')->toArray();
            } else {
                $accessibleSkillsIDs = Requests::where('user_id', $user->id)->access()->pluck('skill_id')->toArray();
            }

            if (! in_array($value, $accessibleSkillsIDs)) {
                $fail(__('validation.training.not_access_skill'));
            }
        }

    }
}
