<?php

namespace App\Rules;

use App\Models\User;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class TraineeBelongsToTrainingRule implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $user = User::find(Request()->get('user_id'));
        if (!$user) {
            $fail(__('validation.training.user_not_exists'));
        } else {
            if ($user->hasRole('Admin')) {
                $traineeIDs = $user->TraineesBelongsToAdmin?->pluck('id')->toArray();
            } else {
                $traineeIDs = $user->TraineesBelongsToTrainer?->pluck('id')->toArray();
            }

            if (is_array($traineeIDs) && !in_array($value, $traineeIDs)) {
                $fail(__('validation.training.the_trainee_not_access'));
            }
        }
    }
}
