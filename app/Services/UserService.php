<?php

namespace App\Services;

use App\Enums\AccessTypeToSkillEnum;
use App\Enums\UserRoleEnum;
use App\Http\Requests\User\CreateCapturerORAdminRequest;
use App\Http\Requests\User\CreateTrainerRequest;
use App\Http\Requests\User\UpdateCapturerOrAdminRequest;
use App\Http\Requests\User\UpdateTrainerRequest;
use App\Mail\SendCredentialsToUserMail;
use App\Models\User;
use App\Services\Keycloak\KeycloakService;
use Arr;
use Auth;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Throwable;
use Validator;
use Spatie\Permission\Models\Role;

class UserService
{
    public function __construct(private KeycloakService $keycloakService)
    {
    }

    /**
     * The function creates a user with the given data and assigns the specified roles to the user.
     *
     * @param array userData An array containing the user data such as username, first name, last name,
     * email, phone number, password, avatar, and is_reset flag.
     * @param array roles An array of roles that the user should be assigned to.
     *
     * @return User a User object.
     */
    public function createCapturerOrAdmin(
        array $userData,
        array $roles
    ): User
    {

        $user = $this->createUser(
            username: $userData['username'],
            firstName: $userData['first_name'],
            lastName: $userData['last_name'],
            email: $userData['email'],
            phoneNumber: $userData['phone_number'],
            password: $userData['password'],
            avatar: $userData['avatar'],
            isReset: $userData['is_reset']
        );

        $this->syncSiteUserFromAuthUser($user);
        $depratment_ids = $user->departmentsBelongsToUserSites->pluck('id')->toArray();
        $user->departments()->sync($depratment_ids);
        $user->syncRoles($roles);


        $this->sendCredentialsToNewUser($user, $userData['password']);


        return $user;
    }

    /**
     * The function creates a trainer by creating a user, assigning roles, syncing departments, and
     * assigning skills.
     *
     * @param array userData An array containing the user data such as username, first name, last name,
     * email, phone number, password, avatar, and is_reset flag.
     * @param array roles An array of roles that the trainer will have. Each role is represented by a
     * string value.
     * @param array departments An array of department IDs that the trainer belongs to.
     * @param array skills An array of skills that the trainer possesses.
     *
     * @return the created user.
     */
    public function createTrainer(
        array $userData,
        array $roles,
        array $departments,
        array $skills
    )
    {

        $user = $this->createUser(
            username: $userData['username'],
            firstName: $userData['first_name'],
            lastName: $userData['last_name'],
            email: $userData['email'],
            phoneNumber: $userData['phone_number'],
            password: $userData['password'],
            avatar: $userData['avatar'],
            isReset: $userData['is_reset']
        );
        $user->syncRoles($roles);

        $this->syncSiteUserFromAuthUser($user);

        $user->departments()->sync($departments);

        $this->assignSkillsToUser($skills, $user);

        $this->sendCredentialsToNewUser($user, $userData['password']);

        return $user;
    }

    /**
     * The function updates a trainer's information, including user data, roles, departments, and
     * skills.
     *
     * @param User user The `` parameter is an instance of the `User` class. It represents the
     * user that needs to be updated as a trainer.
     * @param array userData An array containing the updated user data, such as name, email, and
     * password.
     * @param array roles An array of role IDs that will be assigned to the user.
     * @param array departments An array of department IDs that the trainer belongs to.
     * @param array skills An array of skill IDs that need to be assigned to the trainer.
     *
     * @return User a User object.
     */
    public function updateTrainer(
        User  $user,
        array $userData,
        array $roles,
        array $departments,
        array $skills): User
    {

        $this->updateUser(
            $user,
            $userData
        );

        $user->syncRoles($roles);

        $user->departments()->sync($departments);

        $this->assignSkillsToUser($skills, $user);

        return $user;
    }

    /**
     * The function updates a user's information, syncs their roles, and detaches their skills and
     * departments.
     *
     * @param User user The `` parameter is an instance of the `User` class. It represents the
     * user that needs to be updated.
     * @param array userData An array containing the updated user data, such as name, email, password,
     * etc.
     * @param array roles An array of role names that the user should be assigned to.
     *
     * @return User a User object.
     */
    public function updateCapturerOrAdmin(
        User  $user,
        array $userData,
        array $roles
    ): User
    {
        $this->updateUser(
            $user,
            $userData
        );

        $user->syncRoles($roles);

        $user->skills()->detach();
        $user->departments()->detach();

        return $user;
    }

    /**
     * The delete function deletes a user from the Keycloak service and then deletes the user from the
     * local database.
     *
     * @param User user The user object that needs to be deleted.
     */
    public function delete(User $user)
    {
        $this->keycloakService->deleteUserByEmail($user->email);

        $user->delete();
    }

    /**
     * The function "getUserInfo" loads various related data for a given user object and returns the
     * user object with an additional property "accessible_skills" containing an array of skill IDs
     * from the user's requests.
     *
     * @param User user The "user" parameter is an instance of the User class. It represents a user in
     * the system and contains information about the user, such as their departments, skills, roles,
     * and requests.
     *
     * @return the User object with additional information loaded. The User object will have the
     * departments, skills, roles, and requests loaded. Additionally, the User object will have an
     * accessible_skills property which is an array of skill IDs obtained from the user's requests.
     */
    public function getUserInfo(User $user)
    {

        $user->load([
            'departments' => [
                'children.skills',
                'skills',
            ],
            'roles',
            'requests',
        ]);

        $user->accessible_skills = $user->requests()->pluck('skill_id')->toArray();

        return $user;
    }

    /**
     * The function syncs the sites associated with the authenticated user to the given user.
     *
     * @param User user The parameter `` is an instance of the `User` model. It represents the
     * user for whom we want to sync the sites.
     */
    public function syncSiteUserFromAuthUser(User $user): void
    {
        $authUser = Auth::guard('api')->user();

        $sites = $authUser->sites()->allRelatedIds()->toArray();

        $user->sites()->sync($sites);
    }

    /**
     * The function assigns skills to a user with a specific access type and sets the access status to
     * true.
     *
     * @param array skills An array of skill IDs that need to be assigned to the user.
     * @param User user The `` parameter is an instance of the `User` class. It represents a user
     * to whom the skills will be assigned.
     */
    private function assignSkillsToUser(array $skills, User $user): void
    {
        $user->skills()->syncWithPivotValues(
            $skills,
            [
                'access_type' => AccessTypeToSkillEnum::PERMANENT,
                'is_access' => true,
            ]
        );

    }

    /**
     * The function creates a user by adding them to Keycloak, creating a user record in the database,
     * and optionally adding an avatar image.
     *
     * @param username The username is a unique identifier for the user. It is used to distinguish one
     * user from another in the system.
     * @param firstName The parameter "firstName" represents the first name of the user being created.
     * @param lastName The parameter "lastName" is used to specify the last name of the user being
     * created.
     * @param email The email parameter is the email address of the user being created.
     * @param phoneNumber The parameter `` is used to store the phone number of the user
     * being created. It is a string that represents the phone number of the user.
     * @param password The "password" parameter is used to specify the password for the user being
     * created.
     * @param avatar The "avatar" parameter is used to specify the image file that represents the
     * user's avatar. It is an optional parameter, meaning that it is not required to create a user. If
     * an avatar is provided, it will be added to the user's media collection.
     * @param isReset The parameter "isReset" is a boolean flag that indicates whether the user is
     * being created as part of a password reset process. If it is set to true, it means that the user
     * is being created after a password reset request, and additional steps may be taken to handle the
     * reset process. If
     *
     * @return User a User object.
     */
    public function createUser(
        $username,
        $firstName,
        $lastName,
        $email,
        $phoneNumber,
        $password,
        $avatar,
        $isReset = false,
    ): User
    {

        try {
            $this->keycloakService->addUserToKeycloak(
                username: $username,
                email: $email,
                password: $password,
            );

            $user = User::create([
                'username' => $username,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email,
                'phone_number' => $phoneNumber,
                'is_reset' => $isReset,
            ]);

            if (isset($avatar)) {
                // TempMediaService::createTempUrl('avatar.jpg', $user, 'avatar');
                MediaService::make($user, 'avatar', $avatar);
            }

        } catch (Throwable $e) {
            if ($e->getCode() > 499) {
                $this->keycloakService->deleteUserByEmail($email);
            }

            throw $e;
        }

        return $user;
    }

    /**
     * The function updates a user's data and avatar if provided.
     *
     * @param user The `` parameter is an instance of the `User` model. It represents the user
     * that needs to be updated.
     * @param userData An array containing the updated user data, including the avatar if it has been
     * changed.
     *
     * @return User an instance of the User class.
     */
    public function updateUser(
        $user,
        $userData,
    ): User
    {
        $user->update(Arr::except($userData, 'avatar'));

        if (isset($userData['avatar']['url'])
            && !str_contains($userData['avatar']['url'], "media")) {
            MediaService::make($user, 'avatar', $userData['avatar']);
        }

        return $user;
    }


    /**
     * The function `handleAddAction` in PHP handles the creation of a new user based on their roles
     * and returns a success response with the created user data.
     *
     * @param item The "item" parameter is an array that contains the data for creating a new user. It
     * includes information such as the user's name, email, password, roles, departments, and skills.
     *
     * @return array an array.
     */
    public function handleAddAction($item): array
    {

        if (in_array(UserRoleEnum::Trainer->value, $item["roles"])) {
            $requestData = new CreateTrainerRequest($item);
        } else {
            $requestData = new CreateCapturerORAdminRequest($item);
        }

        $validator = Validator::make($item, $requestData->rules());

        if ($validator->fails()) {
            return \Response::errorValidation($validator)->original;
        }

        $newUser = in_array(UserRoleEnum::Trainer->value, $item["roles"]) ?
            $this->createTrainer(
                $requestData->validatedUserData(),
                $requestData->roles,
                $requestData->departments,
                $requestData->skills
            ) :
            $this->createCapturerOrAdmin(
                $requestData->validatedUserData(),
                $requestData->roles,
            );

        return \Response::success(
            __('user.success_message.create_successfully'),
            [
                'user' => $newUser
            ])->original;
    }


    /**
     * The function `handleUpdateAction` in PHP handles the update action for a user, determining
     * whether the user is a trainer or not and updating their information accordingly.
     *
     * @param item The "item" parameter is an array that contains the data for updating a user. It
     * typically includes information such as the user's ID, roles, departments, skills, and other user
     * data.
     *
     * @return array an array.
     */
    public function handleUpdateAction($item): array
    {
        if (in_array(UserRoleEnum::Trainer->value, $item["roles"])) {
            $requestData = new UpdateTrainerRequest($item);
        } else {
            $requestData = new UpdateCapturerOrAdminRequest($item);
        }

        $validator = Validator::make($item, $requestData->rules());

        if ($validator->fails()) {
            return \Response::errorValidation($validator)->original;
        }
        $user = User::find($item['id']);

        $newUser = in_array(UserRoleEnum::Trainer->value, $item["roles"]) ?
            $this->updateTrainer(
                $user,
                $requestData->validatedUserData(),
                $requestData->roles,
                $requestData->departments,
                $requestData->skills
            ) :
            $this->updateCapturerOrAdmin(
                $user,
                $requestData->validatedUserData(),
                $requestData->roles
            );

        return \Response::success(
            __('user.success_message.update_successfully'),
            [
                'user' => $newUser
            ])->original;


    }

    /**
     * The function handles the deletion of a user and returns a success message if the user is found
     * and deleted, or an error message if the user does not exist.
     *
     * @param item The "item" parameter is the identifier of the user that needs to be deleted. It is
     * used to find the user in the database using the User model's "find" method.
     *
     * @return array an array.
     */
    public function handleDeleteAction($item): array
    {
        $user = User::find($item);
        if ($user) {
            $this->delete($user);

            return \Response::success(__('user.success_message.destroy_successfully'))->original;
        } else {
            return \Response::error(__('validation.exists', ['attribute' => 'user']), 404)->original;
        }
    }

    public function getUsersByRolesInSite()
    {
        return Role::query()->with(["users" => function ($query) {
            return $query->search(request()->get('search'))->whereHas("sites", function ($query) {
                return $query->whereIN('site_id', auth('api')->user()->sites->pluck("id")->toArray());
            });
        }]);
    }

    public function sendCredentialsToNewUser($user, $password): void
    {
        \Mail::to($user->email)->send((new SendCredentialsToUserMail(true, $user, $password))
            ->subject("Welcome to Hapster App!")
            ->build());
    }

}
