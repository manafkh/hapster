<?php

namespace App\Services;

use App\Http\Requests\Trainee\CreateTraineeRequest;
use App\Http\Requests\Trainee\UpdateTraineeRequest;
use App\Models\Trainee;
use App\Models\User;
use Illuminate\Http\Request;
use Response;
use Validator;

class TraineeService
{
    /**
     * The function retrieves trainees based on a user ID and an optional search parameter.
     *
     * @param Request request The  parameter is an instance of the Request class, which is used
     * to retrieve data from the HTTP request. It contains information such as the user ID and search
     * query that are passed to the function.
     * @return a collection of trainees that match the given search criteria.
     */
    public function getTrainees(Request $request)
    {
        $trainees = Trainee::query()
            ->when($request->has('user_id'),
                fn($q) => $q->wherehas(
                    'usersByDepartment',
                    fn($q) => $q->where('users.id', $request->get('user_id'))
                ))->when($request->has('search'),
                fn($q) => $q->where('email', 'ILIKE', '%' . $request->get('search') . '%')
            );

        return $trainees->distinct('id')->get();
    }

    /**
     * The function creates a trainee record with the given data, associates it with the specified
     * departments, and optionally adds an avatar image.
     *
     * @param array traineeData An array containing the data for creating a new trainee. This data
     * typically includes attributes such as name, email, age, etc.
     * @param array departments An array of department IDs that the trainee belongs to.
     * @param avatar The "avatar" parameter is an optional parameter that represents the avatar image
     * of the trainee. It can be a file or a URL pointing to the avatar image. If provided, the
     * function will add the avatar image to the trainee's media collection named "avatar".
     * @return The trainee object is being returned.
     */
    public function create(array $traineeData, array $departments, $avatar = null): Trainee
    {
        $trainee = Trainee::create($traineeData);
        $trainee->departments()->sync($departments);

        if (isset($avatar['url'])) {
            MediaService::make($trainee, "avatar", $avatar);
        }

        return $trainee;
    }

    /**
     * The function updates a trainee's information, assigns them to departments, and adds an avatar if
     * provided.
     *
     * @param Trainee trainee The  parameter is an instance of the Trainee model. It represents
     * the trainee that needs to be updated.
     * @param array traineeData An array containing the updated data for the trainee. This data will be
     * used to update the trainee's attributes in the database.
     * @param array departments An array of department IDs that the trainee belongs to.
     * @param avatar The "avatar" parameter is an optional parameter that represents the image file of
     * the trainee's avatar. If an avatar is provided, it will be added to the trainee's media
     * collection named "avatar".
     * @return the updated Trainee object.
     */
    public function update(Trainee $trainee, array $traineeData, array $departments, $avatar = null): Trainee
    {
        $trainee->update($traineeData);
        $trainee->departments()->sync($departments);

        if (isset($avatar['url'])) {
            MediaService::make($trainee, "avatar", $avatar);
        }

        return $trainee;
    }

    /**
     * The function handles the addition of a trainee by validating the input data, creating a new
     * trainee, and returning a success response.
     *
     * @param item The "item" parameter is the data that is being passed to the "handleAddAction"
     * function. It is used to create a new trainee by calling the "create" method with the validated
     * trainee data, departments, and avatar.
     * @return the original response from the `Response::success` method.
     */
    public function handleAddAction($item)
    {
        $requestData = new CreateTraineeRequest($item);
        $validator = Validator::make($item, $requestData->rules());

        if ($validator->fails()) {
            return Response::errorValidation($validator)->original;
        }

        $newTrainee = $this->create(
            $requestData->validatedTraineeData(),
            $requestData->departments,
            $requestData->avatar
        );

        return Response::success(
            __('trainee.success_message.create_successfully'),
            [
                'trainee' => $newTrainee,
            ]
        )->original;
    }

    /**
     * The function `handleUpdateAction` handles the update action for a trainee, validating the input
     * data, finding the trainee by ID, and updating the trainee's information.
     *
     * @param item The "item" parameter is an array that contains the data of a trainee. It is used to
     * update the trainee's information.
     * @return the original response from the `Response::success` method.
     */
    public function handleUpdateAction($item)
    {

        $requestData = new UpdateTraineeRequest($item);

        $requestData->merge(['trainee' => $item]);

        $validator = Validator::make($item, $requestData->rules());

        if ($validator->fails()) {
            return Response::errorValidation($validator)->original;
        }

        $trainee = Trainee::find($item['id']);

        $newTrainee = $this->update(
            $trainee,
            $requestData->validatedTraineeData(),
            $requestData->departments,
            $requestData->avatar
        );

        return Response::success(
            __('trainee.success_message.update_successfully'),
            [
                'trainee' => $newTrainee,
            ]
        )->original;
    }

    /**
     * The function handles the deletion of a trainee record and returns a success message if the
     * deletion is successful, or an error message if the trainee does not exist.
     *
     * @param item The "item" parameter is the ID of the trainee that needs to be deleted.
     * @return a success message if the trainee is found and successfully deleted, and an error message
     * if the trainee does not exist.
     */
    public function handleDeleteAction($item)
    {
        $trainee = Trainee::find($item);

        if ($trainee) {
            $trainee->delete();

            return Response::success(__('trainee.success_message.destroy_successfully'))->original;
        } else {
            return Response::error(__('validation.exists', ['attribute' => 'trainee']), 404)->original;
        }
    }
}
