<?php
namespace App\Services\TranslateEngine;

use App\Models\Keypoint;
use App\Models\Step;

class TranslateEngine{


    public function setRequest($source,$mode,$target,$data)
    {
        $processData['skill']= [
            'id'=>$data->id,
            'name'=>$data->name,
        ];
        foreach($data->steps as $step){
            $keypoints= [];
            foreach($step->keypoints as $keypoint){
                $keypoints[] = [
                    'id'=>$keypoint->id,
                    'name'=>$keypoint->name,
                    'reason'=>$keypoint->reason
                ];
            }
            $processData['skill']['steps'][] =[
                'id'=>$step->id,
                'name'=>$step->name,
                'keypoints'=>$keypoints
            ];

        }

     return   $request = [
                "source"=>$source,
                "mode"=>$mode,
                "target"=>$target,
                "data"=>$processData,
        ];
    }

    public function getData($request){
     $response = \Http::asJson()->timeout(900)->retry(3)->post(config('translate-engine.engine.url'),$request);
        return $response->json();
    }

    public function proccessData($response,$data){
        foreach($response['data'] as $item){
          $data->setDefaultLocale($item['lang']);
          $data->update($item['skill']);
          foreach($item['skill']['steps'] as $step){
           $updateStep  =Step::find($step['id']);
           $updateStep->setDefaultLocale($item['lang']);
           $updateStep->update($step);
            foreach($step['keypoints'] as $keypoint){
                $updateLeypoint  = Keypoint::find($keypoint['id']);
                $updateLeypoint->setDefaultLocale($item['lang']);
                $updateLeypoint->update($keypoint);
            }
          }
        }
    }

}
