<?php

namespace App\Services\TranslateEngine;

interface TranslateEngineInterface
{
    public function setTarget(array $target): static;

    public function setSource(string $source): static;

    public function setOptions(array $options): static;

    public function translate(array $textsToTranslate): ?string;

    public function getResponse(array $textsToTranslate);
}
