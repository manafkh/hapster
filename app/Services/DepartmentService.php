<?php

namespace App\Services;

use App\Http\Requests\Department\CreateDepartmentRequest;
use App\Http\Requests\Department\UpdateDepartmentRequest;
use App\Models\Department;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Validator;

class DepartmentService
{
    /**
     * The function `stopAccessUserToDepartmentSkills` stops user access to department skills when a
     * department is deleted.
     *
     * @param Department deletedDepartment The parameter `` is an instance of the
     * `Department` class.
     */
    public function stopAccessUserToDepartmentSkills(Department $deletedDepartment): void
    {
        $deletedDepartmentSkills = $deletedDepartment->skills()->allRelatedIds();

        $deletedDepartmentUsers = $deletedDepartment->users()->get();

        foreach ($deletedDepartmentUsers as $user) {
            $userDepartmentIds = $user->departments()->allRelatedIds()->toArray();

            $userSkillsNotBelongToDeletedDepartment = User::query()
                ->selectRaw('DISTINCT skills.id')
                ->join('requests', 'users.id', '=', 'requests.user_id')
                ->join('skills', 'requests.skill_id', '=', 'skills.id')
                ->join('department_skill', 'skills.id', '=', 'department_skill.skill_id')
                ->where('requests.is_access', true)
                ->whereIn('department_skill.department_id', $userDepartmentIds)
                ->where('department_skill.department_id', '!=', $deletedDepartment->id)
                ->where('requests.user_id', $user->id)
                ->pluck('id');

            $skillsIdsForBlockAccess = $deletedDepartmentSkills
                ->diff($userSkillsNotBelongToDeletedDepartment)
                ->toArray();

            $user->requests()
                ->whereIn('skill_id', $skillsIdsForBlockAccess)
                ->update(['is_access' => false]);
        }
    }

    /**
     * The function creates a new department with the given data and associates it with the specified
     * skill IDs.
     *
     * @param array departmentData An array containing the data for creating a new department. This
     * could include attributes such as the department name, description, and any other relevant
     * information.
     * @param array skillIds An array of skill IDs that are associated with the department.
     * @return Department an instance of the Department class.
     */
    public function create(array $departmentData, array $skillIds): Department
    {
        $department = Department::create($departmentData);
        $usersIds = User::query()->whereHas('sites', function (Builder $q) use ($department) {
            return $q->where('site_id', $department->site_id);
        })->whereHas('roles', function (Builder $q) {
            return $q->whereIN('name', ['Admin', 'Capturer']);
        })->pluck('id')->toArray();

        $department->skills()->sync($skillIds);
        $department->users()->sync($usersIds);

        return $department;
    }

    /**
     * The function updates a department's data and syncs its skills with the provided skill IDs.
     *
     * @param Department department The  parameter is an instance of the Department class.
     * It represents the department that needs to be updated.
     * @param array departmentData An array containing the updated data for the department. This data
     * will be used to update the department's attributes.
     * @param array skillIds The skillIds parameter is an array that contains the IDs of the skills
     * that are associated with the department.
     * @return Department an instance of the Department class.
     */
    public function update(Department $department, array $departmentData, array $skillIds): Department
    {
        $department->update($departmentData);

        $department->skills()->sync($skillIds);

        return $department;

    }

    /**
     * The function destroys a department by stopping user access to department skills and then
     * deleting the department.
     *
     * @param Department department The parameter `` is an instance of the `Department`
     * class.
     * @return bool a boolean value of true.
     */
    public function destroy(Department $department): bool
    {
        $this->stopAccessUserToDepartmentSkills($department);

        $department->delete();

        return true;
    }

    /**
     * The function takes an array of department IDs and updates the sort_order column in the
     * departments table based on the order of the IDs in the array.
     *
     * @param array departmentSortedIds An array of department IDs in the desired order.
     * @return bool a boolean value, specifically `true`.
     */
    public function reorder(array $departmentSortedIds): bool
    {
        $caseStatements = '';
        foreach ($departmentSortedIds as $index => $id) {
            $sort_order = $index + 1; // +1 to start sort_order from 1
            $caseStatements .= "WHEN {$id} THEN {$sort_order} ";
        }

        $caseExpression = "CASE id {$caseStatements}END";

        \DB::table('departments')
            ->whereIn('id', $departmentSortedIds)
            ->update([
                'sort_order' => \DB::raw($caseExpression),

            ]);

        return true;

    }

    /**
     * The function `handleAddAction` in PHP handles the creation of a new department, validates the
     * input data, and returns a success response with the newly created department.
     *
     * @param item The "item" parameter is the data that is being passed to the "handleAddAction"
     * function. It is used to create a new department.
     * @return array an array.
     */
    public function handleAddAction($item): array
    {

        $requestData = new CreateDepartmentRequest($item);

        $requestData->merge([
            'sort_order' => (Department::where('parent_id', $requestData->parent_id)->max('sort_order') ?? 0) + 1,
        ]);

        $validator = Validator::make($item, $requestData->rules());

        if ($validator->fails()) {
            return \Response::errorValidation($validator)->original;
        }

        $newDepartment = $this->create(
            $requestData->validatedDepartmentData(),
            $requestData->skills
        );

        return \Response::success(
            __('department.success_message.create_successfully'),
            [
                'department' => $newDepartment,
            ])->original;
    }

    /**
     * The function `handleUpdateAction` in PHP handles the update action for a department, validating
     * the input data, finding the department by ID, and updating it with the new data.
     *
     * @param item The "item" parameter is an array that contains the data for updating a department.
     * It typically includes the department ID, as well as any other fields that need to be updated,
     * such as the department name or description.
     * @return array an array.
     */
    public function handleUpdateAction($item): array
    {

        $requestData = new UpdateDepartmentRequest($item);

        $requestData->merge([
            'department' => $item,
        ]);

        $validator = Validator::make($item, $requestData->rules());
        if ($validator->fails()) {
            return \Response::errorValidation($validator)->original;
        }

        $department = Department::find($item['id']);

        $newDepartment = $this->update(
            $department,
            $requestData->validatedDepartmentData(),
            $requestData->skills
        );

        return \Response::success(
            __('department.success_message.update_successfully'),
            [
                'department' => $newDepartment,
            ])->original;

    }

    /**
     * The function handles the deletion of a department and returns a success message if the
     * department is found and deleted, or an error message if the department does not exist.
     *
     * @param item The "item" parameter is the ID of the department that needs to be deleted.
     * @return array an array.
     */
    public function handleDeleteAction($item): array
    {
        $department = Department::find($item);
        if ($department) {
            $this->destroy($department);

            return \Response::success(__('department.success_message.destroy_successfully'))->original;
        } else {
            return \Response::error(__('validation.exists', ['attribute' => 'department']), 404)->original;
        }
    }
}
