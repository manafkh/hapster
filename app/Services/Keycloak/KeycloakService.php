<?php

namespace App\Services\Keycloak;

use Arr;
use Illuminate\Support\Facades\Http;
use KeycloakGuard\Exceptions\KeycloakGuardException;

class KeycloakService
{
    public function __construct(
        private KeycloakEndPoints $keycloakEndPoints
    ) {
    }

    private function clientParameter(): array
    {
        return [
            'client_id' => config('keycloak.client_id'),
            'client_secret' => config('keycloak.client_secret'),
        ];
    }

    public function login($email, $password)
    {
        $response = Http::keycloak()
            ->asForm()
            ->post(
                $this->keycloakEndPoints->token(),
                [
                    ...$this->clientParameter(),
                    'grant_type' => 'password',
                    'username' => $email,
                    'password' => $password,
                    'scope' => 'profile email openid',
                ]
            );

        return [
            'status' => true,
            'access_token' => $response->json('access_token'),
            'refresh_token' => $response->json('refresh_token'),
        ];
    }

    public function addUserToKeycloak(
        $username,
        $email,
        $password,
    ): bool {
        $accessToken = $this->getAdminAccessToken();

        $response = Http::keycloak()
            ->asJson()
            ->withToken($accessToken)
            ->post(
                $this->keycloakEndPoints->getUsers(),
                [
                    'username' => $username,
                    'enabled' => true,
                    'email' => $email,
                    'credentials' => [
                        [
                            'type' => 'password',
                            'value' => $password,
                            'temporary' => false,
                        ],
                    ],
                ]
            );

        return true;
    }

    public function refreshToken($refreshToken): array
    {
        $response = Http::keycloak()
            ->asForm()
            ->throw(fn () => throw new KeycloakGuardException('Refresh token expired'))
            ->post(
                $this->keycloakEndPoints->token(),
                [
                    ...$this->clientParameter(),
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $refreshToken,
                ]
            );

        return [
            'access_token' => $response->json('access_token'),
            'refresh_token' => $response->json('refresh_token'),
        ];
    }

    public function getAdminAccessToken()
    {
        $response = Http::keycloak()
            ->asForm()
            ->post(
                $this->keycloakEndPoints->token(),
                [
                    'username' => config('keycloak.keycloak_admin_username'),
                    'password' => config('keycloak.keycloak_admin_password'),
                    'client_id' => 'admin-cli',
                    'grant_type' => 'password',
                ]
            );

        return $response->json('access_token');
    }

    public function resetKeycloakPassword($userEmail, $password): bool
    {
        $keycloakUser = $this->getKeycloakUser($userEmail);

        $response = Http::keycloak()
            ->asJson()
            ->withToken($this->getAdminAccessToken())
            ->put(
                $this->keycloakEndPoints->resetPassword($keycloakUser['id']),
                [
                    'type' => 'password',
                    'value' => $password,
                    'credentialData' => $password,
                    'temporary' => false,
                ]
            );

        return true;
    }

    public function deleteUserByEmail($userEmail): bool
    {
        $keycloakUser = $this->getKeycloakUser($userEmail);

        if ($keycloakUser) {
            Http::keycloak()
                ->asForm()
                ->withToken($this->getAdminAccessToken())
                ->delete(
                    $this->keycloakEndPoints->deleteUser($keycloakUser['id'])
                );
        }

        return true;
    }

    private function getAllUsersWithoutAdmin(): array
    {
        // TODO : improve to get all user by client

        $response = Http::keycloak()
            ->asJson()
            ->withToken($this->getAdminAccessToken())
            ->get(
                $this->keycloakEndPoints->getUsers()
            );

        return Arr::where(
            $response->json(),
            fn ($el) => $el['username'] != 'admin'
        );
    }

    public function deleteAllUsersWithoutAdmin()
    {
        $keycloakUserEmails = Arr::pluck($this->getAllUsersWithoutAdmin(), 'email');

        foreach ($keycloakUserEmails as $keycloakUserEmail) {
            $this->deleteUserByEmail($keycloakUserEmail);
        }

        return true;
    }

    public function getKeycloakUser($email)
    {
        $response = Http::keycloak()
            ->asJson()
            ->withToken($this->getAdminAccessToken())
            ->get(
                $this->keycloakEndPoints->getUserByEmail($email)
            );

        return $response->json(0);
    }

    public function logouUser($refresh_token)
    {
        // $credentials =  $this->refreshToken($refresh_token);
        $response = Http::keycloak()
            ->asForm()
            ->post(
                $this->keycloakEndPoints->logout(),
                [
                    'client_id' => config('keycloak.client_id'),
                    'client_secret' => config('keycloak.client_secret'),
                    'refresh_token' =>$refresh_token,
                ]
            );
        return $response->successful();
    }
}
