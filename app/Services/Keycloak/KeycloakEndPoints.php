<?php

namespace App\Services\Keycloak;

class KeycloakEndPoints
{
    private string $realmsPrefix;

    private string $realm;

    public function __construct()
    {
        $this->realm = config('keycloak.realm');

        $this->realmsPrefix = '/admin/realms/'.$this->realm;
    }

    private function sanitizePath($path)
    {
        return preg_replace('/\/+/', '/', trim($path, '/'));
    }

    private function resolvePath(array $pathParts = [], array $queryParams = []): string
    {
        // Join path parts
        $path = $this->sanitizePath(implode('/', $pathParts));

        // Convert query parameters to a query string
        $queryString = http_build_query($queryParams);

        // If query string is not empty, append it to the path
        if ($queryString) {
            $path .= '?'.$queryString;
        }

        return $path;
    }

    public function token(): string
    {
        return $this->resolvePath(['realms', $this->realm, 'protocol/openid-connect/token']);
    }

    public function getUsers(): string
    {
        return $this->resolvePath([$this->realmsPrefix, 'users']);
    }

    public function resetPassword($keycloakUserId): string
    {
        return $this->resolvePath([$this->realmsPrefix, 'users', $keycloakUserId, 'reset-password']);
    }

    public function getUserByEmail($email): string
    {
        return $this->resolvePath([$this->realmsPrefix, 'users'], ['email' => $email]);
    }

    public function deleteUser($keycloakUserId): string
    {
        return $this->resolvePath([$this->realmsPrefix, 'users', $keycloakUserId]);
    }

    public function logout(): string
    {
        return $this->resolvePath(['realms/', $this->realm, '/protocol/openid-connect/logout']);
    }
}
