<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Http\Request;

class TrainerService
{
    /**
     * The function retrieves trainers with admin privileges and filters them based on a search term
     * provided in the request.
     *
     * @param Request request The  parameter is an instance of the Request class, which
     * represents an HTTP request. It contains information about the current request, such as the
     * request method, headers, and query parameters.
     * @return a collection of trainers that match the search query provided in the request.
     */
    public function getTrainers(Request $request)
    {
        return User::trainersWithAdmin()->search('%'.$request->get('search').'%')->get();
    }
}
