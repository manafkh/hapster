<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Http\Testing\MimeType;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Support\Str;
use Spatie\MediaLibrary\HasMedia;

class TempMediaService
{
    // public function __construct(protected Media $media)
    // {
    // }

    public static function createTempUrl(string $name, HasMedia $model, string $collectionName)
    {
        Media::create([
            'uuid' => Str::uuid(),
            'model_type' => get_class($model),
            'model_id' => $model->id,
            'collection_name' => $collectionName,
            'name' => pathinfo($name, PATHINFO_FILENAME),
            'file_name' => $name,
            'mime_type' => MimeType::from($name),
            'disk' => config('media-library.disk_name'),
            'conversions_disk' => config('media-library.disk_name'),
            'size' => 12,
            'order_column' => 1,
            'manipulations' => [],
            'custom_properties' => [],
            'generated_conversions' => [],
            'responsive_images' => [],
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    public static function replaceFileByUrl(string $url, string $disk, UploadedFile $file)
    {
        Storage::disk($disk)
            ->putFile(explode('storage/', $url)[1], $file);
    }
}
