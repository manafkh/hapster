<?php

namespace App\Services;

use App\Models\Skill;
use App\Models\Step;

class StepService
{
    public function __construct(private KeypointService $keypointService)
    {
    }

    /**
     * The function creates steps for a given skill and associates keypoints with each step.
     *
     * @param Skill skill The "skill" parameter is an instance of the Skill class. It represents a
     * specific skill for which we want to create steps.
     * @param array stepsData An array containing data for each step. Each element in the array should
     * be an associative array with the following keys:
     * @return bool a boolean value of true.
     */
    public function createStepsForSkills(Skill $skill, array $stepsData): bool
    {
        foreach ($stepsData as $step_request) {
            $step = $skill->steps()->create([
                'name' => $step_request['name'],
                'sort_order' => $step_request['sort_order'],
            ]);
            if (isset($step_request['keypoints'])) {
                $this->keypointService->createKeypointsForStep($step, $step_request['keypoints']);
            }

        }

        return true;
    }

    /**
     * The function updates the steps for a given skill, including creating new steps if necessary, and
     * updates the keypoints for each step.
     *
     * @param Skill skill The "skill" parameter is an instance of the Skill class. It represents a
     * specific skill that you want to update the steps for.
     * @param stepsData The  parameter is an array that contains the data for each step. Each
     * element in the array represents a step and contains the following information:
     * @return bool a boolean value of true.
     */
    public function updateStepsForSkills(Skill $skill, $stepsData): bool
    {
        foreach ($stepsData as $stepData) {
            $step = $skill->steps()->updateOrCreate(
                [
                    'id' => isset($stepData['id']) ? $stepData['id'] : null,
                ],
                [
                    'name' => $stepData['name'],
                    'sort_order' => $stepData['sort_order'],
                ]
            );
            $this->keypointService->updateKeypointsForStep($step, $stepData['keypoints']);
        }

        return true;
    }

    /**
     * The function deletes steps from the database when updating a skill, based on the provided step
     * IDs.
     *
     * @param deleteStepsIds An optional array of step IDs that need to be deleted.
     * @return bool a boolean value.
     */
    public function deleteStepsWhenUpdateSkill(?array $deleteStepsIds): bool
    {
        return Step::whereIn('id', $deleteStepsIds)->delete();
    }
}
