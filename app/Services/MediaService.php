<?php

namespace App\Services;

use App\Exceptions\MediaUploadIsNotExistException;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaService
{
//    public ?array $addMediaPaths;
//
//    public ?array $removeMediaIds;
//
//    public ?string $tempDisk;

    //    public ?string $mediaDisk;

    public function __construct(protected HasMedia $model, protected string $collection)
    {
//        $this->tempDisk = 'temporary_folder';
        //        $this->mediaDisk = config('media-library.disk_name');
    }

    public static function make(HasMedia $model, string $collection, $mediaObject): bool
    {
        return (bool)(new self($model, $collection))->addMedia($mediaObject);
    }

    public static function makeUpdateMultiMedia(HasMedia $model, string $collection, array $mediaObjects): bool
    {
        return (new self($model, $collection))->updateMultiMedia($mediaObjects);
    }

    public static function makeAddMultiMedia(HasMedia $model, string $collection, $mediaObjects): bool
    {
        return (new self($model, $collection))->addMultiMedia($mediaObjects);
    }

    /**
     * @return Media
     *
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function addMedia($mediaObject): ?Media
    {

        [$oldFileObjects, $newMediaObject] = $this->separateFilesByFormat([$mediaObject]);

        if (empty($newMediaObject)) {
            return null;
        }

        $mediaPath = $this->resolveFilePath($newMediaObject[0]);

        $media = $this->model->addMedia($mediaPath)->preservingOriginal()->toMediaCollection($this->collection);

        $media->save();

        return $media;
    }

    public function removeMedia(...$mediaIds): bool
    {
        if (is_array($mediaIds[0])) {
            $mediaIds = $mediaIds[0];
        }

        Media::whereIn('id', $mediaIds)->delete();

        return true;
    }

    public function updateMultiMedia(array $mediaObjects): bool
    {
        [$addMediaPaths, $removeMediaIds] = $this->resolveMultiMediaFiles($mediaObjects);

        foreach ($addMediaPaths as $mediaPath) {
            $this->model
                ->addMedia($mediaPath)
                ->preservingOriginal()
                ->toMediaCollection($this->collection);
        }

        //Todo check if file is removed from storage
        $this->removeMedia($removeMediaIds);

        return true;
    }

    public function addMultiMedia($mediaObjects): bool
    {

        [$addMediaPaths, $removeMediaIds] = $this->resolveMultiMediaFiles($mediaObjects);

        foreach ($addMediaPaths as $mediaPath) {
            $this->model
                ->addMedia($mediaPath)
                ->preservingOriginal()
                ->toMediaCollection($this->collection);
        }

        return true;
    }

    /**
     * @throws MediaUploadIsNotExistException
     */
    public function resolveFilePath($mediaObject): string
    {
        $url = $mediaObject['url'];

        $filePath = static::getFilePathByDisk($url);

        static::checkMediaExist($filePath);

        return $filePath;
    }


    public function getIdFromFileUrl($mediaUrl): int
    {
        $diskUrl = config('filesystems.disks.media.url');
        $parts = explode('/', str_replace($diskUrl, '', $mediaUrl));

        $mediaId = trim($parts[1], '/');

        return (int)$mediaId;
    }

    public static function getFilePathByDisk($url): string
    {
        //        $relativePath = parse_url($url, PHP_URL_PATH),

        $tempDiskUrl = config("filesystems.disks.temporary_folder.url");
        $tempDiskRoot = config("filesystems.disks.temporary_folder.root");

        if (str_contains($url, $tempDiskUrl)) {
            $filePath = $tempDiskRoot . str_replace($tempDiskUrl, '', $url);
        } else {
            throw new Exception('the temporary url is not valid');
        }

        return $filePath;
    }

    /**
     * @throws MediaUploadIsNotExistException
     */
    public function resolveMultiMediaFiles($mediaObjects): array
    {
        $addMediaObjects = isset($mediaObjects['add']) ? $mediaObjects['add'] : [];
        $removeMediaUrls = isset($mediaObjects['delete']) ? $mediaObjects['delete'] : [];

        $addMediaPaths = [];
        $removeMediaIds = [];
        [$oldFileObjects, $newFileObjects] = $this->separateFilesByFormat($addMediaObjects);

        foreach ($newFileObjects as $mediaObject) {
            $addMediaPaths[] = $this->resolveFilePath($mediaObject);

        }

        foreach ($removeMediaUrls as $removeMediaUrl) {
            $removeMediaIds[] = $this->getIdFromFileUrl($removeMediaUrl);
        }

        return [$addMediaPaths, $removeMediaIds];
    }

    /**
     * @throws MediaUploadIsNotExistException
     */
    public static function checkMediaExist($filePath): bool
    {
        if (!file_exists($filePath)) {
            throw new MediaUploadIsNotExistException(__('exception.check_media_uploaded'));
        }

        return true;
    }

    public function separateFilesByFormat($fileObjects): array
    {
        $oldFileObjects = [];
        $newFileObjects = [];

        foreach ($fileObjects as $fileObject) {
            if (isset($fileObject['url'])) {

                if (isset($fileObject['id'])) {
                    $oldFileObjects[] = $fileObject;
                } else {
                    $newFileObjects[] = $fileObject;
                }
            } else {
                throw new Exception('the media object format is not correct');
            }
        }

        return [
            $oldFileObjects,
            $newFileObjects,
        ];
    }
}
