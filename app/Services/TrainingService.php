<?php

namespace App\Services;

use App\Enums\TrainingStatusEnum;
use App\Http\Requests\Training\CreateTrainingRequest;
use App\Models\Training;
use App\Notifications\CalendarRequestTrainingToTraineeNotification;
use App\Notifications\CalendarRequestTrainingToTrainerNotification;
use Arr;
use Illuminate\Support\Facades\Notification;

class TrainingService
{
    /**
     * The function creates multiple training instances based on the provided request data and returns
     * an array of the created trainings.
     *
     * @param CreateTrainingRequest request The `` parameter is an instance of the
     * `CreateTrainingRequest` class. It is used to validate and retrieve the data sent in the request.
     * @return array an array of Training objects.
     */
    public function create(CreateTrainingRequest $request): array
    {
        $trainings = [];
        foreach ($request->get('trainings') as $training_request) {
            $training = Training::create($request->validatedTrainingData()
                + $training_request
                + ['status' => TrainingStatusEnum::SCHEDULED]);

            $training->stages()->sync($training_request['stages']);
            $training->trainees()->sync($request->validated('trainees'));
            $this->inviteUsersToTraining($training);
            $trainings[] = $training->load(['stages', 'trainees', 'signingResponsible']);
        }

        return $trainings;
    }

    /**
     * The function updates the status of a training record in the database and returns the updated
     * training record.
     *
     * @param status The status parameter is used to update the status of a training. It can be a
     * string or an integer representing the new status value.
     * @param trainingID The trainingID parameter is the unique identifier of the training that needs
     * to be updated.
     * @return the updated training object.
     */
    public function updateStatus($status, $trainingID, $signatureFiles,$responsibleDate,$trainerDate)
    {
        $training = Training::find($trainingID);
        $training->status = $status;
        $training->trainer_signature_date = $trainerDate;
        $training->responsible_signature_date = $responsibleDate;


        if (isset($signatureFiles)) {
            if (isset($signatureFiles['responsible_signature'])) {
                MediaService::make($training, "responsible_signature", $signatureFiles['responsible_signature']);
            }
            if (isset($signatureFiles['trainee_signature'])) {
                MediaService::make($training, "trainee_signature", $signatureFiles['trainee_signature']);
            }
        }


        $training->save();

        return $training;
    }

    /**
     * The function updates a training record with new data, including the status, stages, and
     * trainees.
     *
     * @param training The "training" parameter is an instance of a Training model. It represents a
     * specific training session or program.
     * @param trainingData The `` parameter is an array that contains the updated data for
     * the training. It typically includes information such as the training's name, description, start
     * date, end date, and any other relevant details.
     * @return the updated training object.
     */
    public function update(Training $training, $trainingData)
    {
        $training->update(Arr::except($trainingData, ['stages'])
            + ['status' => TrainingStatusEnum::SCHEDULED]);


        $stagesArray = Arr::mapWithKeys($trainingData['stages'], function ($el) {
            return [
                $el["stage_id"] => [
                    "signing_responsible" => isset($el["signing_responsible"]) ? $el["signing_responsible"] : null
                ]
            ];
        });


        $training->stages()->sync($stagesArray);

        $training->trainees()->sync($trainingData['trainees']);

        return $training;
    }

    /**
     * The function invites users to a training session by sending them a calendar request
     * notification.
     *
     * @param training The parameter `` is an instance of the Training model. It represents a
     * specific training session or event.
     */
    public function inviteUsersToTraining($training): void
    {
        Notification::send(
            $training->user,
            new CalendarRequestTrainingToTrainerNotification($training->user, $training,
                [
                    ...$training->trainees,
                    $training->user
                ])
        );
        foreach ($training->trainees as $trainee) {
            Notification::send(
                $trainee,
                new CalendarRequestTrainingToTraineeNotification($trainee, $training, [
                    ...$training->trainees,
                    $training->user
                ])
            );
        }
    }
}
