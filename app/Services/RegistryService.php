<?php

namespace App\Services;

use App\Exceptions\AuthException;
use App\Helpers\OperationResult;
use App\Models\Registry;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class RegistryService
{
    private const TOKEN_EXPIRATION_PER_DAYS = 30;

    private const REFRESH_TOKEN_EXPIRATION_PER_DAYS = 90;

    public function __construct(protected Registry $registry, protected OperationResult $operationResult)
    {
    }

    /**
     * The function generates a unique token by combining an ordered UUID and the current microtime,
     * and checks if the token already exists in the registry, recursively generating a new token if it
     * does.
     *
     * @param int step The "step" parameter is an optional parameter that indicates the number of times
     * the function has recursively called itself. It is used to keep track of the number of attempts
     * made to generate a unique token.
     * @return string a string, which is the generated unique token.
     */
    private function generateUniqueToken(int $step = 0): string
    {
        if ($step === 5) {
            Log::warning("error on generation unique registry token for the {$step} in a row");
            throw AuthException::generateUniqueToken();
        }

        $token = md5(Str::orderedUuid()->toString() . microtime());
        if ($this->registry->where('token', $token)->exists()) {
            $this->generateUniqueToken($step + 1);
        }

        return $token;
    }

    /**
     * The function generates a unique refresh token by combining an ordered UUID and the current
     * microtime, and checks if the token already exists in the registry, recursively generating a new
     * token if it does.
     *
     * @param int step The "step" parameter is an optional parameter that indicates the number of times
     * the function has recursively attempted to generate a unique refresh token. It is used to track
     * the number of attempts made to generate a unique token and prevent an infinite loop in case of
     * failure.
     * @return string a string, which is the generated unique refresh token.
     */
    private function generateUniqueRefreshToken(int $step = 0): string
    {
        if ($step === 5) {
            Log::warning("error on generation unique registry refresh token for the {$step} in a row");
            throw AuthException::generateUniqueToken();
        }

        $refreshToken = md5(Str::orderedUuid()->toString() . microtime());
        if ($this->registry->where('refresh_token', $refreshToken)->exists()) {
            $this->generateUniqueToken($step + 1);
        }

        return $refreshToken;
    }

    /**
     * The function findByMacAddress searches for a registry entry based on a given MAC address and
     * returns the first matching entry.
     *
     * @param string macAddress A string representing the MAC address to search for in the registry.
     * @return ?Registry an instance of the `Registry` class or `null` if no matching record is found.
     */
    private function findByMacAddress(string $macAddress): ?Registry
    {
        return $this->registry->query()
            ->where('mac_address', $macAddress)
            ->first();
    }

    /**
     * The function findByIpAddress searches for a registry entry based on a given IP address and
     * returns the first matching entry.
     *
     * @param string macAddress A string representing the IP address to search for in the registry.
     * @return ?Registry an instance of the `Registry` class or `null` if no matching record is found.
     */
    private function findByIpAddress(string $ipAddress): ?Registry
    {
        return $this->registry->query()
            ->where('ip_address', $ipAddress)
            ->first();
    }

    /**
     * The function creates unique tokens and refresh tokens for a given registry object and saves them
     * along with their expiration dates.
     *
     * @param Registry registry The `registry` parameter is an instance of the `Registry` class. It
     * represents a registry object that contains information about a specific entity or resource.
     * @return Registry the updated `` object.
     */
    private function createTokensForRegistry(Registry $registry): Registry
    {
        $token = $this->generateUniqueToken();
        $refreshToken = $this->generateUniqueRefreshToken();

        $registry->token = $token;
        $registry->token_expired_at = Carbon::today()->addDays(self::TOKEN_EXPIRATION_PER_DAYS);
        $registry->refresh_token = $refreshToken;
        $registry->refresh_token_expired_at = Carbon::today()->addDays(self::REFRESH_TOKEN_EXPIRATION_PER_DAYS);
        $registry->save();

        return $registry;
    }

    /**
     * The function `getDeviceToken` retrieves or generates a token and refresh token for a device
     * based on its MAC address.
     *
     * @param string macAddress The `macAddress` parameter is a string that represents the MAC address
     * of a device.
     * @return OperationResult an instance of the OperationResult class.
     */
    public function getDeviceToken(string $identifier): OperationResult
    {
        $registry = filter_var($identifier, FILTER_VALIDATE_IP) !== false
            ? $this->findByIpAddress($identifier)
            : $this->findByMacAddress($identifier);

        if (empty($registry)) {
            return $this->operationResult->markAsClientError(__('auth.device_not_found'));
        }

        if (empty($registry->token) === false) {
            if ($registry->token_expired_at->isPast() === false) {
                return $this->operationResult->setData([
                    'token' => $registry->token,
                    'refresh_token' => $registry->refresh_token,
                ]);
            }

            if ($registry->refresh_token_expired_at->isPast() === false) {
                $token = $this->generateUniqueToken();
                $registry->update([
                    'token' => $token,
                    'token_expired_at' => Carbon::today()->addDays(self::TOKEN_EXPIRATION_PER_DAYS),
                ]);

                return $this->operationResult->setData([
                    'token' => $registry->token,
                    'refresh_token' => $registry->refresh_token,
                ]);
            }
        }

        $registry = $this->createTokensForRegistry($registry);

        return $this->operationResult->setData([
            'token' => $registry->token,
            'refresh_token' => $registry->refresh_token,
        ]);
    }

    /**
     * The function checks if a given token exists in the registry and if it has not expired.
     *
     * @param string token A string representing a token that needs to be checked.
     * @return bool a boolean value.
     */
    public function checkToken(string $token): bool
    {
        $registry = $this->registry->where('token', $token)
            ->first();

        return empty($registry) === false && $registry->token_expired_at->isPast() === false;
    }

    /**
     * The function finds a record in the registry based on a given refresh token.
     *
     * @param refreshToken The `refreshToken` parameter is a string that represents a unique token used
     * for refreshing an access token. It is typically used in authentication systems to obtain a new
     * access token when the current one expires.
     * @return the first result from the query that matches the given refresh token.
     */
    private function findBYRefreshToken($refreshToken)
    {
        return $this->registry->query()
            ->where('refresh_token', $refreshToken)
            ->first();
    }

    /**
     * The function refreshes a device token by finding the corresponding registry using a refresh
     * token, generating a new unique token, and updating the registry with the new token and
     * expiration date.
     *
     * @param string refreshToken The `refreshToken` parameter is a string that represents a unique
     * identifier for a device token. It is used to find a device token registry in the system.
     * @return OperationResult an instance of the OperationResult class.
     */
    public function refreshDeviceToken(string $refreshToken): OperationResult
    {
        $registry = $this->findByRefreshToken($refreshToken);

        if (empty($registry) || $registry->refresh_token_expired_at->isPast()) {
            return $this->operationResult->markAsClientError(__('auth.refresh_token_not_found'));
        }

        $token = $this->generateUniqueToken();
        $registry->update([
            'token' => $token,
            'token_expired_at' => Carbon::today()->addDays(self::TOKEN_EXPIRATION_PER_DAYS),
        ]);

        return $this->operationResult->setData([
            'token' => $registry->token,
            'refresh_token' => $registry->refresh_token,
        ]);
    }
}
