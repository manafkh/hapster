<?php

namespace App\Services;

use App\Http\Requests\Skill\CreateSkillRequest;
use App\Http\Requests\Skill\UpdateSkillRequest;
use App\Jobs\TranslateSkillJob;
use App\Models\Skill;
use App\Models\User;
use Validator;

class SkillService
{
    /**
     * The function is a constructor that initializes the StepService and KeypointService objects.
     */
    public function __construct(
        private StepService     $stepService,
        private KeypointService $keypointService,
    )
    {
    }

    /**
     * The function creates a skill with steps and keypoints, adds media files, associates the skill
     * with user sites, and creates steps for the skill.
     *
     * @param array skillData An array containing the data for creating a new skill. This data
     * typically includes attributes such as the skill's name, description, and any other relevant
     * information.
     * @param array stepsData An array containing the data for the steps of the skill. Each element of
     * the array represents a step and should have the following structure:
     * @return Skill a Skill object.
     */
    public function createSkillWithStepsAndKeypoints(
        array $skillData,
        array $stepsData,
              $image,
              $methodsAndDocumentation
    ): Skill
    {
        $skill = Skill::create($skillData);
        $siteIds = User::query()->where('id', '=', $skillData['user_id'])->first()->sites->pluck('id')->toArray();
        $skill->sites()->sync($siteIds);

        if (isset($image)) {
            MediaService::make($skill, 'image', $image);
        }

        MediaService::makeAddMultiMedia($skill, 'methods_and_documentation', $methodsAndDocumentation);


        $this->stepService->createStepsForSkills($skill, $stepsData);
        dispatch(new TranslateSkillJob(app()->getLocale(), $skill, ['en', 'fr', 'es'], 'T2ST'));
        return $skill;
    }

    /**
     * The function updates a skill with steps and keypoints based on the provided request data.
     *
     * @param Skill skill The `` parameter is an instance of the `Skill` model. It represents the
     * skill that needs to be updated.
     * @param UpdateSkillRequest request The `` parameter is an instance of the
     * `UpdateSkillRequest` class, which is a custom request class that handles the validation and data
     * retrieval for updating a skill. It contains the data submitted by the user for updating the
     * skill.
     * @return Skill an instance of the Skill class.
     */
    public function updateSkillWithStepsAndKeypoints(
        Skill $skill,
        array $skillData,
        array $stepsData,
        array $deleteStepIds,
        array $deleteKeypointIds,
              $image,
              $methodsAndDocumentation
    ): Skill
    {
        $skill->update($skillData);

        if (isset($image)) {
            MediaService::make($skill, 'image', $image);
        }

        MediaService::makeUpdateMultiMedia($skill, 'methods_and_documentation', $methodsAndDocumentation);

        $this->stepService->updateStepsForSkills($skill, $stepsData);

        $this->stepService->deleteStepsWhenUpdateSkill($deleteStepIds);

        $this->keypointService->deleteKeypointsWhenUpdateSkill($deleteKeypointIds);
        dispatch(new TranslateSkillJob(app()->getLocale(), $skill, ['en', 'fr', 'es'], 'T2ST'));
        return $skill;
    }

    public function handleAddAction($item): array
    {

        $requestData = new CreateSkillRequest($item);

        $validator = Validator::make($item, $requestData->rules());
        if ($validator->fails()) {
            return \Response::errorValidation($validator)->original;
        }

        $newSkill = $this->createSkillWithStepsAndKeypoints(
            $requestData->validatedSkillData(),
            $requestData->steps,
            $requestData->image,
            $requestData->methods_and_documentation,
        );

        $newSkill->load([
            'steps',
            'steps.keypoints',
        ]);
        return \Response::success(
            __('skill.success_message.create_successfully'),
            [
                'skill' => $newSkill,
            ]
        )->original;
    }

    public function handleUpdateAction($item): array
    {

        $requestData = new UpdateSkillRequest($item);

        $requestData->merge([
            'skill' => $item,
        ]);

        $validator = Validator::make($item, $requestData->rules());

        if ($validator->fails()) {
            return \Response::errorValidation($validator)->original;
        }

        $skill = Skill::find($item['id']);

        $newSkill = $this->updateSkillWithStepsAndKeypoints(
            $skill,
            $requestData->validatedSkillData(),
            $requestData->steps,
            $requestData->delete_step_ids,
            $requestData->delete_keypoint_ids,
            $requestData->image,
            $requestData->methods_and_documentation,
        );

        $newSkill->load([
            'steps',
            'steps.keypoints',
        ]);

        return \Response::success(
            __('skill.success_message.update_successfully'),
            [
                'skill' => $newSkill,
            ]
        )->original;
    }

    public function handleDeleteAction($item): array
    {
        $skill = Skill::find($item);
        if ($skill) {
            $skill->delete();

            return \Response::success(__('skill.success_message.destroy_successfully'))->original;
        } else {
            return \Response::error(__('validation.exists', ['attribute' => 'skill']), 404)->original;
        }
    }

    public function exportSkillToLRS($actor, $verb, $object)
    {

        $authorization = 'Basic ' . base64_encode(config('LRS.USER') . ':' . config('LRS.PASSWORD'));

        $response = \Http::withHeaders([
            'X-Experience-API-Version' => '2.0',
            'Authorization' => $authorization,
            'Content-Type' => 'application/json',
        ])
            ->post(
                config('LRS.URL') . '/xapi/statements',
                [
                    'actor' => ['mbox' => 'mailto:' . $actor],
                    'verb' => [
                        'id' => 'http://adlnet.gov/expapi/verbs/' . $verb,
                        'display' => ['en-US' => $verb]
                    ],
                    'object' => [
                        'id' => $object
                    ],
                    'version' => '2.0'
                ]
            );

        return $response->json(0);
    }

    public function translateSkill($lang, $skillId)
    {

        $skill = Skill::findOrFail($skillId);
        dispatch_sync(new TranslateSkillJob(app()->getLocale(), $skill, [$lang], 'T2ST'));
        return $skill->translateOrDefault($lang);
    }
}
