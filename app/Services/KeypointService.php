<?php

namespace App\Services;

use App\Models\Keypoint;
use App\Models\Step;

class KeypointService
{
    /**
     * The function creates keypoints for a given step using the provided data and adds media files to
     * each keypoint.
     *
     * @param Step step The `` parameter is an instance of the `Step` class. It represents the
     * step for which the keypoints are being created.
     * @param indexStep The parameter `` represents the index of the current step in the
     * loop. It is used to generate a unique key for the media file being added to each keypoint.
     * @param keypointsData An array containing data for creating keypoints. Each element in the array
     * represents a keypoint and contains the following properties:
     * @return bool a boolean value of `true`.
     */
    public function createKeypointsForStep(Step $step, $keypointsData): bool
    {
        foreach ($keypointsData as $KeypointData) {
            $newKeypoint = $step->keypoints()->create([
                'name' => $KeypointData['name'],
                'reason' => $KeypointData['reason'],
                'sort_order' => $KeypointData['sort_order'],
                'type' => $KeypointData['type'],
            ]);

            if (isset($KeypointData['media'])) {
                MediaService::makeAddMultiMedia($newKeypoint, 'keypoint_media', $KeypointData['media']);
            }
        }

        return true;
    }

    /**
     * The function updates or creates keypoints for a given step, including their name, reason, type,
     * and associated media files.
     *
     * @param Step step The `` parameter is an instance of the `Step` class. It represents a step
     * in a process or a task.
     * @param indexStep The parameter `` represents the index of the current step in the
     * loop. It is used to construct the file input name for media files in the request.
     * @param keypointsData An array of data containing information about the keypoints. Each element
     * in the array represents a keypoint and contains the following properties:
     * @return bool a boolean value of true.
     */
    public function updateKeypointsForStep(Step $step, $keypointsData): bool
    {
        foreach ($keypointsData as $keypointData) {

            $newKeypoint = Keypoint::updateOrCreate(
                [
                    'id' => isset($keypointData['id']) ? $keypointData['id'] : null,
                ],
                [
                    'name' => $keypointData['name'],
                    'reason' => $keypointData['reason'],
                    'step_id' => isset($keypointData['step_id']) ? $keypointData['step_id'] : $step->id,
                    'sort_order' => $keypointData['sort_order'],
                    'type' => $keypointData['type'],
                ]
            );

            if (isset($keypointData['media'])) {
                MediaService::makeUpdateMultiMedia($newKeypoint, 'keypoint_media', $keypointData['media']);
            }
        }

        return true;
    }

    /**
     * The function deletes keypoints when updating a skill in PHP.
     *
     * @param deleteKeypointsIds An optional array of IDs representing the keypoints that need to be
     * deleted.
     * @return bool a boolean value.
     */
    public function deleteKeypointsWhenUpdateSkill(?array $deleteKeypointsIds): bool
    {
        return Keypoint::whereIn('id', $deleteKeypointsIds)->delete();
    }
}
