<?php

namespace App\Services;

use \Carbon\Carbon;
class ImportSkillService
{
    public function __construct(private SkillService $skillService)
    {

    }

    public function prepareImportSkill($file,$location,$userId){

       $targetFolder =  $this->convertTfsFileTOFolder($file);
       $jsonFile = $targetFolder.'/'."newData.json";
       $targetName = pathinfo($file, PATHINFO_FILENAME);
       $skill =  $this->compareDataSkillWithJsonFile($this->readJsonData($jsonFile), $targetName,$location,$userId);
      
       return  $this->skillService->handleAddAction($skill);

    }
    public function convertTfsFileTOFolder($fileName)
    {

        $tfsFile = config('filesystems.disks.public.root').'/'.$fileName;
        $targetName = pathinfo($tfsFile, PATHINFO_FILENAME);

        \Storage::disk('temporary_folder')->makeDirectory($targetName);
        $targetFolder = config('filesystems.disks.temporary_folder.root').'/'.$targetName;
        if (file_exists($tfsFile)) {
            $zip = new \ZipArchive();
            if ($zip->open($tfsFile) === TRUE) {
                $zip->extractTo($targetFolder);
                $zip->close();
                \Storage::disk('public')->delete($fileName);
            } else {
                return  'Failed to open the TFS file.';
            }
        } else {
            return 'TFS file not found.';
        }
        return $targetFolder;
    }

    public function readJsonData($jsonFile)
    {
        return json_decode(file_get_contents($jsonFile), true);
    }


    public function compareDataSkillWithJsonFile($jsonData, $folderName,$location,$userId): array
    {
        app()->setLocale($jsonData['language']);
        return [
            'name' => $jsonData['skillName'],
            'image'=>$this->getUrlMedia($this->getMediaSkill($jsonData),$folderName),
            'user_id' => $userId,
            'confidentiality_level' => $jsonData['confidentiality'],
            'type' => $jsonData['skillDataTable']['prerequisites']['generic_specific'],
            'writing_date' =>$this->parseDate($jsonData['skillDataTable']['prerequisites']['WritingDate']
            ,$jsonData['language']),
            'methods_and_documentation' => ['add'=>$this->getUrlMedia(
                $jsonData['skillDataTable']['prerequisites']['methodDocFileNameTbl'],$folderName
            )],
            'author' => $jsonData['skillDataTable']['prerequisites']['JBSAuthor'],
            'location' => $location,
            'importance' => $jsonData['skillDataTable']['prerequisites']['WhyImportant'],
            'required_skill' => $jsonData['skillDataTable']['prerequisites']["WhatSkillsRequired"],
            'required_safety_equipments' => $jsonData['skillDataTable']['prerequisites']["SafetyRequired"],
            'required_materials_parts' => $jsonData['skillDataTable']['prerequisites']["MaterialsPartsRequired"],
            'tools_supplies_machinery' => $jsonData['skillDataTable']['prerequisites']["ToolsSuppliesMachinery"],
            'steps' => $this->compareDataStepsWithJsonFile($jsonData, $folderName)

        ];
    }

    public function getMediaSkill($jsonData)
    {
        if (!empty($jsonData['skillPhoto'])) {
            return $jsonData['skillPhoto'];
        } else {
            return $jsonData['skillVideo'];
        }
    }

    public function compareDataStepsWithJsonFile($jsonData, $folderName): array
    {
        $sortStep = 1;
        $steps=[];
        foreach ($jsonData['skillDataTable']['steps'] as $step) {
            $sortKeypoint = 1;
            $keyPoints = [];
            foreach ($step['keyPoints'] as $keyPoint) {

               $add =  ["add"=>$this->getUrlMedia(
                    isset($keyPoint['imgs']) ?$keyPoint['imgs']:null, $folderName
                    )
               ];
                $keyPoints[] = [
                    'name' => $keyPoint['keyName'],
                    'reason' => $keyPoint['reason'],
                    'sort_order' => $sortKeypoint,
                    'type' => $keyPoint['keyPointIconId'] ?? 1,
                    'media' =>  isset($keyPoint['imgs']) ? $add : [] ,
                ];
                $sortKeypoint++;
            }
            $steps[] = [
                'name' => $step['stepName'],
                'sort_order' => $sortStep,
                'keypoints'=>$keyPoints

            ];

            $sortStep++;
        }
        return $steps;
    }

    public function getUrlMedia($media, $folderName)
    {
       if(isset($media)||!empty($media)) {
        if (is_array($media)) {
            $mediaUrls = [];
            foreach ($media as $file) {
                $mediaUrls[] = [ "url"=>\Storage::disk('temporary_folder')->url($folderName . '/' . $file)];
            }
            return $mediaUrls;
        }

        return  [ "url"=>\Storage::disk('temporary_folder')->url($folderName . '/' . $media)];
    }
     return ['url'=>null];
    }
    public function parseDate($dateString,$locale){
        if($locale == 'fr'){
            setlocale(LC_TIME, 'fr_FR.utf8');
            $dateString = str_replace(
                ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet',
                 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],

                ['January', 'February', 'March', 'April', 'May', 'June', 'July',
                'August', 'September', 'October', 'November', 'December'],
                $dateString );


        }
        $carbonDate = Carbon::parse($dateString);
        setlocale(LC_TIME, null);
        return $carbonDate->format('Y-m-d H:i:s');

    }


}
