<?php

namespace App\Services;

use App\Enums\UserRoleEnum;
use App\Exceptions\AuthException;
use App\Exceptions\AuthInvalidCredentialsException;
use App\Mail\OTPMail;
use App\Mail\SendCredentialsToUserMail;
use App\Models\User;
use App\Services\Keycloak\KeycloakService;
use Hash;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Password;

class AuthService
{
    public function __construct(
        private KeycloakService $keycloakService,
        private UserService     $userService,
    )
    {
    }

    /**
     * @throws AuthException
     */
    /**
     * The login function takes an email and password as parameters, retrieves the user with the given
     * email, attempts to login using the Keycloak service, and returns the access token, refresh
     * token, and user information.
     *
     * @param email The email parameter is the email address of the user trying to log in.
     * @param password The password parameter is the user's password that they entered during the login
     * process.
     * @return an array with the following keys and values:
     */
    public function login($email, $password)
    {

        //TODO: improve try and catch errors

        try {
            $user = User::getUser($email)->firstOrFail();

            $keycloakTokens = $this->keycloakService->login($email, $password);
        } catch (AuthInvalidCredentialsException $e) {
            isset($user) && $user->checkAttemptsCount();
            throw $e;
        }

        return [
            'access_token' => $keycloakTokens['access_token'],
            'refresh_token' => $keycloakTokens['refresh_token'],
            'user' => $user,
        ];
    }

    /**
     * The function registers a user by creating a new user, assigning roles, syncing sites, sending an
     * OTP code, and returning the user.
     *
     * @param array userData An array containing user data such as username, first name, last name,
     * email, phone number, password, and avatar.
     * @param array sites The "sites" parameter is an array that contains the IDs of the sites that the
     * user should be associated with. These IDs are used to sync the user's sites in the "sites"
     * relationship.
     * @return the created user object.
     */
    public function register(array $userData, array $sites)
    {
        $user = $this->userService->createUser(
            username: $userData['username'],
            firstName: $userData['first_name'],
            lastName: $userData['last_name'],
            email: $userData['email'],
            phoneNumber: $userData['phone_number'],
            password: $userData['password'],
            avatar: $userData['avatar'],
        );


        $user->syncRoles(UserRoleEnum::Admin->value);

        $user->sites()->sync($sites);
        $depratment_ids = $user->departmentsBelongsToUserSites->pluck('id')->toArray();
        $user->departments()->sync($depratment_ids);
        $this->sendOTPCode($user);

        $this->sendWelcomeToNewUser($user);

        return $user;
    }

    /**
     * The function `changePassword` resets the password for the currently authenticated user using the
     * Keycloak service.
     *
     * @param newPassword The  parameter is the new password that the user wants to set for
     * their account.
     */
    public function changePassword($newPassword): void
    {
        $user = Auth::guard('api')->user();
        $this->keycloakService->resetKeycloakPassword(
            $user->email,
            $newPassword
        );
        $user->is_reset = false;
        $user->save();
    }

    /**
     * The function "changePasswordByAdmin" resets the password for a user in Keycloak using the
     * provided email and new password.
     *
     * @param User user The user object represents the user whose password needs to be changed. It
     * contains information about the user, such as their email, name, and other details.
     * @param newPassword The  parameter is the new password that will be set for the user.
     */
    public function changePasswordByAdmin(User $user, $newPassword,$isReset): void
    {
        $this->keycloakService->resetKeycloakPassword($user->email, $newPassword);
        $user->update(['is_reset'=>$isReset]);
    }

    /**
     * The function sends a password reset link to the user's email and updates the user's "is_reset"
     * status to true.
     *
     * @param email The email parameter is the email address of the user who wants to reset their
     * password.
     * @return true result of the update operation on the user model, which is either true or false.
     */
    public function sendResetLinkEmail($email)
    {
        Password::sendResetLink(['email' => $email]);

        return true;
    }

    /**
     * The function resets the password for a user in Keycloak and updates the user's "is_reset" flag
     * to false in the database.
     *
     * @param email The email parameter is the email address of the user whose password needs to be
     * reset.
     * @param password The password parameter is the new password that will be set for the user.
     */
    public function reset($email, $password)
    {
        $this->keycloakService->resetKeycloakPassword(
            $email,
            $password
        );

        $user = User::where('email', $email)->first();
        $user->is_reset = false;
        $user->save();
    }

    /**
     * The function sends an OTP code to a user's email address.
     *
     * @param user The "user" parameter is an object that represents a user in your application. It
     * likely contains information such as the user's email address, name, and other relevant details.
     */
    public function sendOTPCode($user): void
    {
        $otp = $this->generateOTP($user);
        \Mail::to($user->email)->send((new OTPMail($user, $otp))->build());
    }

    /**
     * The function generates a random OTP (One-Time Password) for a user, checks if it already exists
     * in the database, and updates the user's OTP and OTP expiry time.
     *
     * @param user The "user" parameter is an instance of the User model. It represents the user for
     * whom the OTP (One-Time Password) is being generated.
     * @return int an integer value, which is the generated OTP (One-Time Password).
     */
    public function generateOTP($user): int
    {
        creation:
        $otp = random_int(100000, 999999);

        if (User::where('otp', $otp)->exists()) {
            goto creation;
        }

        $user->update([
            'otp' => Hash::make($otp),
            'otp_expiry' => Carbon::now()->addMinutes(3),
        ]);

        return $otp;
    }

    /**
     * The function verifies the OTP (One-Time Password) provided by the user and updates the user's
     * email verification status if the OTP is correct and has not expired.
     *
     * @param user The "user" parameter represents the user object or model that contains the
     * information of the user who is trying to verify their OTP (One-Time Password).
     * @param otp The `otp` parameter is the one-time password (OTP) code that the user has entered for
     * verification.
     */
    public function verifyOTP($user, $otp): void
    {
        if (!Hash::check($otp, $user->otp) || !Carbon::now()->lt($user->otp_expiry)) {
            throw AuthException::wrongOTPCode();
        }

        $user->forceFill([
            'email_verified_at' => $user->freshTimestamp(),
            'otp' => null,
            'otp_expiry' => null,
        ])->save();
    }


    public function sendWelcomeToNewUser($user): void
    {
        \Mail::to($user->email)->send((new SendCredentialsToUserMail(false, $user))
            ->subject("Welcome to Hapster App!")
            ->build());
    }

}
