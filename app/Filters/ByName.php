<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
class ByName
{
    public function __construct(protected Request $request){

    }
    public  function handle(Builder $builder, \Closure $next) {
        return $next($builder)->when($this->request->has('first_name'),
            fn($query) => $query->where('first_name','REGEXP',$this->request->first_name)
        );
    }

}
