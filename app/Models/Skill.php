<?php

namespace App\Models;

use App\Enums\SkillTypeEnum;
use App\Http\Resources\Media\MediaResource;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Skill extends Model implements HasMedia, TranslatableContract
{
    use CascadeSoftDeletes, HasFactory, SoftDeletes;
    use InteractsWithMedia;
    use LogsActivity;
    use Translatable;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    public $translatedAttributes = ['name', 'audio'];

    protected $fillable = [
        'user_id',
        'confidentiality_level',
        'type',
        'writing_date',
        'author',
        'location',
        'importance',
        'required_skill',
        'required_safety_equipments',
        'required_materials_parts',
        'tools_supplies_machinery',
    ];

    protected $casts = [
        'methods_and_documentation' => 'array',
        'writing_date' => 'date',
        'type' => SkillTypeEnum::class,
    ];

    protected $cascadeDeletes = [
        'siteSkillPivot',
        'departmentSkillPivot',
        'requests',
        'steps',
        'steps',
    ];

    protected $appends = [
        'departments_shared_number',
        'image',
        'methods_and_documentation',
        'site_id'
    ];

    protected $hidden = [
        'departments_count',
        'media',
        'translations'
    ];

    protected $with = ['steps'];

    protected $withCount = ['departments'];


    protected function siteId(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->sites()->first()->id,
        );
    }
    protected function image(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->hasMedia('image') ? MediaResource::make($this->getFirstMedia('image')) : null,
        );
    }

    protected function methodsAndDocumentation(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->hasMedia('methods_and_documentation') ?
                MediaResource::collection($this->getMedia('methods_and_documentation')) : [],
        );
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(62)
            ->height(62)
            ->nonQueued();
    }

    public function siteSkillPivot(): HasMany
    {
        return $this->hasMany(SiteSkill::class);
    }

    public function departmentSkillPivot(): HasMany
    {
        return $this->hasMany(DepartmentSkill::class);
    }


    protected function departmentsSharedNumber(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->departments_count ?? 0,
        );
    }

    public function departments(): BelongsToMany
    {
        return $this->belongsToMany(Department::class);
    }

    public function requests(): HasMany
    {
        return $this->hasMany(Requests::class);
    }

    public function steps(): HasMany
    {
        return $this->hasMany(Step::class);
    }

    public function trainings()
    {
        return $this->hasMany(Training::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('image')
            ->singleFile();

        $this->addMediaCollection('methods_and_documentation');
    }

    public function sites(): BelongsToMany
    {
        return $this->belongsToMany(Site::class);
    }
}
