<?php

namespace App\Models;

use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Department extends Model
{
    use CascadeSoftDeletes, HasFactory,SoftDeletes;
    use LogsActivity;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    protected $cascadeDeletes = [
        'departmentUserPivot',
        'departmentSkillPivot',
        'departmentTraineePivot',
        'children',
    ];

    protected $fillable = [
        'parent_id',
        'name',
        'sort_order',
        'description',
        'site_id',
    ];

    protected $appends = ['skills_count', 'children_count', 'skill_ids'];

    protected function skillsCount(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->skills()->count(),
        );
    }

    protected function skillIds(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->skills()->pluck('id')->toArray(),
        );
    }

    public function getChildrenCountAttribute()
    {
        $count = 0;
        foreach ($this->children as $child) {
            $count++;
            $count += $child->getChildrenCountAttribute();
        }

        return $count;
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'department_user', 'department_id', 'user_id');
    }

    public function departmentUserPivot(): HasMany
    {
        return $this->hasMany(DepartmentUser::class);
    }

    public function departmentSkillPivot(): HasMany
    {
        return $this->hasMany(DepartmentSkill::class);
    }

    public function departmentTraineePivot(): HasMany
    {
        return $this->hasMany(DepartmentTrainee::class);
    }

    public function site(): BelongsTo
    {
        return $this->belongsTo(Site::class);
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Department::class, 'parent_id');
    }

    public function children(): HasMany
    {
        return $this->hasMany(Department::class, 'parent_id')
            ->with(['children', 'children.skills'])->orderBy('sort_order');
    }

    public function skills(): BelongsToMany
    {
        return $this->belongsToMany(Skill::class);
    }

    public function trainees(): BelongsToMany
    {
        return $this->belongsToMany(Trainee::class, 'department_trainee');
    }

    public function scopeIsParent(Builder $query, $isParent): Builder
    {
        return $isParent
        ? $query->whereNotNull('parent_id')
        : $query->whereNull('parent_id');
    }
}
