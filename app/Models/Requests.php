<?php

namespace App\Models;

use App\Enums\AccessTypeToSkillEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Requests extends Model
{
    use HasFactory, SoftDeletes;
    use LogsActivity;

    const PERMANENT = 'permanent';

    const TEMPORARY = 'temporary';

    protected $fillable = [
        'user_id',
        'skill_id',
        'access_type',
        'from_date',
        'to_date',
        'is_access',
    ];

    protected $casts = [
        'access_type' => AccessTypeToSkillEnum::class,
        'from_date' => 'date',
        'to_date' => 'date',
        'is_access' => 'boolean',
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    public function skill(): BelongsTo
    {
        return $this->belongsTo(Skill::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function scopeAccess(Builder $query): Builder
    {
        return $query->where('is_access', true);
    }
}
