<?php

namespace App\Models;

use App\Enums\TrainingStatusEnum;
use App\Http\Resources\Media\MediaResource;
use App\Mail\LoginAttemptWarning;
use DB;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Staudenmeir\EloquentHasManyDeep\HasManyDeep;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

class User extends Authenticatable implements HasMedia
{
    use CascadeSoftDeletes, HasFactory, HasRoles, Notifiable, SoftDeletes;
    use HasRelationships;
    use InteractsWithMedia;
    use LogsActivity;

    protected $cascadeDeletes = [
        'userSitePivot',
        'trainingStagePivot',
        'departmentUserPivot',
        'trainees',
        'skills',
        'requests',
    ];

    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'phone_number',
        'verify_code',
        'email',
        'email_verified_at',
        'attempts_count',
        'retry_attempt_time',
        'is_reset',
        'remember_token',
        'otp',
        'otp_expiry',
    ];

    protected $hidden = [
        'password',
        'media',
        'remember_token',
        'otp',
        'otp_expiry',
        'verify_code',
        'attempts_count',
        'retry_attempt_time',

    ];

    protected $with = [
        'roles',
    ];
    protected $appends = [
        'avatar',
        'full_name',
        'captured_skills_count',
        'trainings_done_count',
        'department_ids',
        'site_ids',
        'skill_ids',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'otp_expiry' => 'datetime',
        'password' => 'hashed',
        'is_reset' => 'boolean',
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }
    protected function fullName(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->first_name .' '. $this->last_name,
        );
    }
    protected function avatar(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->hasMedia('avatar') ? MediaResource::make($this->getFirstMedia('avatar')) : null,
        );
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(62)
            ->height(62)
            ->nonQueued();
    }

    public function userSitePivot(): HasMany
    {
        return $this->hasMany(UserSite::class);
    }

    public function trainingStagePivot(): HasMany
    {
        return $this->hasMany(TrainingStage::class, 'signing_responsible');
    }

    public function departmentUserPivot(): HasMany
    {
        return $this->hasMany(DepartmentUser::class);
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function sites(): BelongsToMany
    {
        return $this->belongsToMany(Site::class, 'user_site');
    }

    public function captureSkills(): HasMany
    {
        return $this->hasMany(Skill::class);
    }

    protected function capturedSkillsCount(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->captureSkills()->count(),
        );
    }

    protected function trainingsDoneCount(): Attribute
    {

        return Attribute::make(
            get: fn () => $this->trainings()->where('status', TrainingStatusEnum::DONE)->count(),
        );
    }

    public function departmentIds(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->departments->pluck('id')->toArray(),
        );
    }

    public function skillIds(): Attribute
    {
        if ($this->accessSkills()->count() > 0 || $this->hasRole('Trainer')) {
            return Attribute::make(
                get: fn () => $this->accessSkills()->pluck('id')->toArray(),
            );
        } else {
            return Attribute::make(
                get: fn () => $this->adminOrCaptureSkills()->pluck('id')->toArray(),
            );
        }
    }

    public function siteIds(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->sites->pluck('id')->toArray(),
        );
    }

    public function skills(): BelongsToMany
    {
        return $this->belongsToMany(Skill::class, 'requests')
            ->withTimestamps()
            ->withPivot([
                'access_type',
                'from_date',
                'to_date',
                'is_access',
            ]);
    }

    public function accessSkills(): BelongsToMany
    {
        return $this->belongsToMany(Skill::class, 'requests')
            ->wherePivot('is_access', true);
    }

    public function departmentsBelongsToUserSites(): HasManyDeep
    {
        return $this->hasManyDeep(Department::class, ['user_site', Site::class]);
    }

    public function adminOrCaptureSkills()
    {
        return $this->hasManyDeep(Skill::class, ['user_site', Site::class, 'site_skill']);
    }

    public function getAllTrainingsByAdminSites()
    {
        return $this->hasManyDeepFromRelations(
            $this->adminOrCaptureSkills(),
            (new Skill())->trainings()
        )->distinct('id');
    }

    public function traineesByDepartment()
    {
        return $this->hasManyDeep(Trainee::class, ['department_user', Department::class, 'department_trainee']);
    }


    public function getTraineesByRole()
    {
        if ($this->hasRole('Admin')) {
            return $this->TraineesBelongsToAdmin();
        } elseif ($this->hasRole('Trainer')) {
            return $this->TraineesBelongsToTrainer();
        }

        return null;
    }

    public function getDepartmentByRole()
    {
        if ($this->hasRole('Admin')) {
            return $this->departmentsBelongsToUserSites();
        } elseif ($this->hasRole('Trainer')) {
            return $this->departments();
        }

        return null;
    }

    public function departments(): BelongsToMany
    {
        return $this->belongsToMany(Department::class);
    }

    public function requests(): HasMany
    {
        return $this->hasMany(Requests::class);
    }

    public function isVerified(): bool
    {
        return empty($this->email_verified_at) === false;
    }

    public static function getUser($email): Builder
    {
        return self::query()
            ->where('email', $email)
            ->orWhere('username', $email);
    }

    public function scopeTrainers(Builder $query): void
    {
        $query->whereHas('roles', function (Builder $query) {
            $query->where('name', 'Trainer');
        });
    }

    public function ScopeTrainersWithAdmin(Builder $query): void
    {
        $query->trainers()->orWhere('id', auth()->user()->id);
    }

    public function checkAttemptsCount(): void
    {
        $this->attempts_count = ($this->attempts_count ?? 0) + 1;
        DB::commit();

        if ($this->attempts_count >= config('auth.login_attempt_before_warning')) {
            Mail::to($this->email)->send(new LoginAttemptWarning());
            $this->attempts_count = 0;
        }

        $this->save();
    }

    public function scopeSearch(Builder $query, $data): Builder
    {
        return $query->where('username', 'ILIKE', '%' . $data . '%')
            ->orWhere('email', 'ILIKE', '%' . $data . '%');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')
            ->singleFile();
    }

    public function trainees()
    {
        return $this->hasMany(Trainee::class);
    }

    public function trainings()
    {
        return $this->hasMany(Training::class);
    }

    public function trainingSign()
    {
        return $this->belongsToMany(Training::class, 'training_stage', 'signing_responsible', 'training_id');
    }
}
