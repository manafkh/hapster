<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Step extends Model implements TranslatableContract
{
    use CascadeSoftDeletes, HasFactory, SoftDeletes;
    use LogsActivity;
    use Translatable;

    public $translatedAttributes = ['name','audio'];

    protected $fillable = ['name', 'sort_order', 'skill_id'];

    protected $with = ['keypoints'];

    protected $cascadeDeletes = [
        'keypoints',
    ];
    protected $hidden = ['translations'];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    public function skill(): BelongsTo
    {
        return $this->belongsTo(Skill::class);
    }

    public function keypoints(): HasMany
    {
        return $this->hasMany(Keypoint::class);
    }
}
