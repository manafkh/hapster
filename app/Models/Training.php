<?php

namespace App\Models;

use App\Enums\TrainingStatusEnum;
use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Training extends Model implements HasMedia
{
    use CascadeSoftDeletes, HasFactory, SoftDeletes;
    use LogsActivity;
    use InteractsWithMedia;


    protected $cascadeDeletes = [
        'trainingStagePivot',
        'traineeTrainingPivot',
    ];

    protected $fillable = [
        'user_id', 'skill_id', 'date', 'from', 'to', 'status',
        'trainer_signature_date','responsible_signature_date'
    ];

    protected $appends = [
        'start_at',
        'end_at',
        'responsible_signature',
        'responsible_full_name',
        'trainee_signature',
        'trainer_full_name'
    ];

    protected $casts = [

        'status' => TrainingStatusEnum::class,
        'date' => 'timestamp',
        'from' => 'timestamp',
        'to' => 'timestamp',
    ];

    protected $hidden = [
        'media',
    ];
    protected function responsibleFullName(): Attribute
    {
        $responsible = $this->signingResponsible()->first();
        return Attribute::make(
            get: fn() => isset($responsible) ? $responsible->full_name : null,
        );
    }
    protected function trainerFullName(): Attribute
    {
        $trainer =  $this->user()->first();
        return Attribute::make(
            get: fn() => isset($trainer) ? $trainer->full_name : null,
        );
    }
    protected function responsibleSignature(): Attribute
    {
        return Attribute::make(
            get: fn() => $this->getFirstMediaUrl('responsible_signature'),
        );
    }

    protected function traineeSignature(): Attribute
    {
        return Attribute::make(
            get: fn() => $this->getFirstMediaUrl('trainee_signature'),
        );
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    protected function endAt(): Attribute
    {
        $time = date('H:i', $this->to);
        $date = date('Y-m-d', $this->date);

        return Attribute::make(
            get: fn() => date('Y-m-d H:i:s', strtotime("$date $time"))
        );
    }

    protected function startAt(): Attribute
    {

        $time = date('H:i', $this->from);
        $date = date('Y-m-d', $this->date);

        return Attribute::make(
            get: fn() => date('Y-m-d H:i:s', strtotime("$date $time"))
        );
    }

    public function trainingStagePivot(): HasMany
    {
        return $this->hasMany(TrainingStage::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function skill()
    {
        return $this->belongsTo(Skill::class)->withTrashed();
    }

    public function trainees()
    {
        return $this->belongsToMany(Trainee::class);
    }

    public function stages()
    {
        return $this->belongsToMany(Stage::class, 'training_stage')
            ->withPivot('signing_responsible');
    }

    public function signingResponsible()
    {
        return $this->belongsToMany(User::class, 'training_stage', 'training_id', 'signing_responsible');
    }


    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('responsible_signature')
            ->singleFile();

        $this->addMediaCollection('trainee_signature')
            ->singleFile();
    }
}
