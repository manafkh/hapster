<?php

namespace App\Models;

use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

use Staudenmeir\EloquentHasManyDeep\HasManyDeep;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

class Trainee extends Model implements HasMedia
{
    use CascadeSoftDeletes, HasFactory, Notifiable, SoftDeletes;
    use LogsActivity;
    use HasRelationships;


    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    use InteractsWithMedia;

    protected $cascadeDeletes = [
        'departmentTraineePivot',
    ];


    protected $appends = ['avatar', 'full_name','department_ids'];

    protected $hidden = [
        'media',
    ];

    protected function avatar(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->getFirstMediaUrl('avatar'),
        );
    }

    protected function departmentIds(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->departments->pluck('id')->toArray(),
        );
    }

    protected $fillable = [
        'first_name', 'last_name', 'email', 'phone_number', 'user_id', 'trainee_identify',
    ];

    public function departmentTraineePivot(): HasMany
    {
        return $this->hasMany(DepartmentTrainee::class);
    }


    public function usersByDepartment(): HasManyDeep
    {
        return $this->hasManyDeep(User::class, ['department_trainee', Department::class, 'department_user']);
    }


    public function departments()
    {
        return $this->belongsToMany(Department::class, 'department_trainee');
    }

    public function trainings()
    {
        return $this->belongsToMany(Training::class, 'trainee_training');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class);
    }

    public function getFullNameAttribute(): string
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')
            ->singleFile();
    }
}
