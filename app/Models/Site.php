<?php

namespace App\Models;

use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Site extends Model
{
    use CascadeSoftDeletes, HasFactory, SoftDeletes;
    use LogsActivity;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    protected $cascadeDeletes = ['departments', 'siteSkillPivot', 'userSitePivot'];

    public $timestamps = false;

    protected $fillable = [
        'name',
        'company_id',
    ];

    protected $hidden = [
        'pivot', 'company_id',
    ];

    public function siteSkillPivot(): HasMany
    {
        return $this->hasMany(SiteSkill::class);
    }

    public function userSitePivot(): HasMany
    {
        return $this->hasMany(UserSite::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_site');
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function departments(): HasMany
    {
        return $this->hasMany(Department::class);
    }

    public function skills(): BelongsToMany
    {
        return $this->belongsToMany(Skill::class);
    }
    public function licenses(){
        return $this->hasMany(License::class);
    }
}
