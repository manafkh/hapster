<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class StepTranslation extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = ['name','audio'];

    public $timestamps = false;

    // public function registerMediaCollections(): void
    // {
    //     foreach ($this->fillable as $item) {
    //         $this->addMediaCollection($item.'_audio')
    //             ->singleFile();
    //     }
    // }
}
