<?php

namespace App\Models;

use App\Enums\KeypointTypeEnum;
use App\Http\Resources\Media\MediaResource;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Keypoint extends Model implements HasMedia, TranslatableContract
{
    use HasFactory, SoftDeletes;
    use InteractsWithMedia;
    use LogsActivity;
    use Translatable;

    public $translatedAttributes = ['name','name_audio','reason_audio', 'reason'];
    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    protected $fillable = [
        'step_id',
        'name',
        'reason',
        'sort_order',
        'type',
    ];

    protected $hidden = ['media','translations'];

    protected $casts = [
        'type' => KeypointTypeEnum::class,
    ];

    protected function keypointMedia(): Attribute
    {

        return Attribute::make(
            get: fn () => $this->hasMedia('keypoint_media') ?
                MediaResource::collection($this->getMedia('keypoint_media')) : [],
        );
    }

    protected $appends = ['keypoint_media'];

    public function step(): BelongsTo
    {
        return $this->belongsTo(Step::class);
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('keypoint_media');
    }
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->width(62)
            ->height(62)
            ->nonQueued();
    }
}
