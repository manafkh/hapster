<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class DepartmentTrainee extends Pivot
{
    public $timestamps = false;
}
