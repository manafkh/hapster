<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registry extends Model
{
    use HasFactory;

    protected $table = 'registries';

    protected $fillable = [
        'ip_address',
        'ip_range',
        'mac_address',
        'required_ip',
        'token',
        'token_expired_at',
        'refresh_token',
        'refresh_token_expired_at',
        'node_id',
        'service_id',
    ];

    protected $casts = [
        'token_expired_at' => 'datetime',
        'refresh_token_expired_at' => 'datetime',
    ];
}
