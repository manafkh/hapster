<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class SkillTranslation extends Model implements HasMedia
{
    use InteractsWithMedia;

    public $timestamps = false;

    protected $fillable = ['name','audio'];

    // public function registerMediaCollections(): void
    // {
    //     foreach ($this->fillable as $item) {
    //         $this->addMediaCollection($item.'_audio')
    //             ->singleFile();
    //     }
    // }
}
