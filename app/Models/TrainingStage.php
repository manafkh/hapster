<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TrainingStage extends Pivot
{
    public $timestamps = false;
}
