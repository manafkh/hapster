<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class TraineeTraining extends Pivot
{
    public $timestamps = false;
}
