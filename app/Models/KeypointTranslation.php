<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class KeypointTranslation extends Model implements HasMedia
{
    use InteractsWithMedia;

    protected $fillable = ['name', 'reason','name_audio','reason_audio'];

    public $timestamps = false;

    // public function registerMediaCollections(): void
    // {
    //     foreach ($this->fillable as $item) {
    //         $this->addMediaCollection($item.'_audio')
    //             ->singleFile();
    //     }
    // }
}
