<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserSite extends Pivot
{
    public $timestamps = false;
}
