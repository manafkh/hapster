<?php

namespace App\Models;

use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Stage extends Model
{
    use CascadeSoftDeletes, HasFactory, SoftDeletes;
    use LogsActivity;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    public $timestamps = false;

    protected $fillable = ['name'];

    protected $cascadeDeletes = [
        'trainingStagePivot',
    ];

    public function trainings()
    {
        return $this->belognsToMany(Training::class);
    }

    public function trainingStagePivot(): HasMany
    {
        return $this->hasMany(TrainingStage::class);
    }
}
