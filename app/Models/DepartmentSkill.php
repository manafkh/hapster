<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class DepartmentSkill extends Pivot
{
    public $timestamps = false;
}
