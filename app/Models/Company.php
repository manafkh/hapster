<?php

namespace App\Models;

use Dyrynda\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\LogOptions;
use Spatie\Activitylog\Traits\LogsActivity;

class Company extends Model
{
    use CascadeSoftDeletes, HasFactory, SoftDeletes;
    use LogsActivity;

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable()
            ->logOnlyDirty();
    }

    protected $cascadeDeletes = ['sites'];

    public $timestamps = false;

    protected $fillable = ['name'];

    public function sites(): HasMany
    {
        return $this->hasMany(Site::class);
    }

    //    public function users(): HasMany
    //    {
    //        return $this->hasMany(User::class);
    //    }
}
