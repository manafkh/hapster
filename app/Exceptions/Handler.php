<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use KeycloakGuard\Exceptions\KeycloakGuardException;
use Response;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            $e->getMessage();
        });
    }

    public function render($request, Throwable $e)
    {
        report($e);

        if ($request->expectsJson() || $request->is('api/*')) {
            switch (true) {

                case $e instanceof ValidationException:
                    return Response::errorValidation($e->validator);
                    break;

                case $e instanceof UnauthorizedException:
                    return Response::error(
                        __('exception.unauthorized'),
                        \Illuminate\Http\Response::HTTP_FORBIDDEN
                    );
                    break;

                case $e instanceof ModelNotFoundException:
                    return Response::error(
                        __('exception.model_not_found'),
                        \Illuminate\Http\Response::HTTP_NOT_FOUND
                    );
                    break;

                case $e instanceof AuthenticationException:
                    return  Response::error(
                        __('exception.no_authenticate_user'),
                        \Illuminate\Http\Response::HTTP_FORBIDDEN
                    );
                    break;

                case $e instanceof KeycloakGuardException:
                    return Response::error(
                        __('exception.unauthorized_user'),
                        \Illuminate\Http\Response::HTTP_UNAUTHORIZED
                    );
                    break;

                default:
                    return parent::render($request, $e);
            }
        }
    }
}
