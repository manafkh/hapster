<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class MediaUploadIsNotExistException extends Exception
{
    public function render($request)
    {
        if ($request->expectsJson() || $request->is('api/*')) {
            return \Response::error(
                $this->getMessage() ?: __('exception.check_media_uploaded'),
                $this->getCode() ?: Response::HTTP_FAILED_DEPENDENCY
            );
        }
    }
}
