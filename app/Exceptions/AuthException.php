<?php

namespace App\Exceptions;

use Exception;
use Response;

class AuthException extends Exception
{
    public function render($request)
    {
        if ($request->expectsJson() || $request->is('api/*')) {
            return Response::error(
                $this->getMessage() ?? 'error in authentication.',
                $this->getCode() ?: \Illuminate\Http\Response::HTTP_UNAUTHORIZED
            );
        }
    }

    public static function wrongOTPCode(): static
    {
        return new static(__('auth.wrong_otp_code'));
    }

    public static function generateUniqueToken(): static
    {
        return new static(__('auth.generate_unique_token_error'), \Illuminate\Http\Response::HTTP_LOOP_DETECTED);
    }

    public static function invalidRegistryToken(): static
    {
        return new static(
            __('auth.invalid_registry_token'),
            \Illuminate\Http\Response::HTTP_PROXY_AUTHENTICATION_REQUIRED
        );
    }
}
