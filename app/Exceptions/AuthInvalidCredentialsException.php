<?php

namespace App\Exceptions;

class AuthInvalidCredentialsException extends AuthException
{
    public function __construct(string $message = '', int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
