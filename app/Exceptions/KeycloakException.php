<?php

namespace App\Exceptions;

use Exception;
use Response;

class KeycloakException extends Exception
{
    public function render($request)
    {
        if ($request->expectsJson() || $request->is('api/*')) {
            return Response::error(
                $this->getMessage(),
                $this->getCode() ?: \Illuminate\Http\Response::HTTP_FAILED_DEPENDENCY
            );
        }
    }

    public static function clientError($message, $code): static
    {
        $message = $message ?: __('exception.client_error_keycloak');

        return new static($message,$code);
    }

    public static function serverError($message): static
    {
        $message = $message ?: __('exception.server_error_keycloak');

        return new static($message,\Illuminate\Http\Response::HTTP_INTERNAL_SERVER_ERROR);
    }
}
