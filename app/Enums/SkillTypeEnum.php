<?php

namespace App\Enums;

use ArchTech\Enums\Names;
use ArchTech\Enums\Options;
use ArchTech\Enums\Values;

enum SkillTypeEnum: int
{
    use Names, Options, Values;

    case SUPPLEMENTARY = 1;
    case CORE = 2;

}
