<?php

namespace App\Enums;

use ArchTech\Enums\Names;
use ArchTech\Enums\Options;
use ArchTech\Enums\Values;

enum AccessTypeToSkillEnum: int
{
    use Names, Options, Values;

    case PERMANENT = 1;
    case TEMPORARY = 2;

}
