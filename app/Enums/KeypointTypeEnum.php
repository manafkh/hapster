<?php

namespace App\Enums;

use ArchTech\Enums\Names;
use ArchTech\Enums\Options;
use ArchTech\Enums\Values;

enum KeypointTypeEnum: int
{
    use Names, Options, Values;

    case EASE = 1;
    case SAFETY = 2;
    case QUALITY = 3;

}
