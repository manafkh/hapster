<?php

namespace App\Enums;

use ArchTech\Enums\Names;
use ArchTech\Enums\Options;
use ArchTech\Enums\Values;

enum UserRoleEnum: int
{
    use Names, Options, Values;

    case Admin = 1;
    case Capturer = 2;
    case Trainer = 3;
}
