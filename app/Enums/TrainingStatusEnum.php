<?php

namespace App\Enums;

use ArchTech\Enums\Names;
use ArchTech\Enums\Options;
use ArchTech\Enums\Values;

enum TrainingStatusEnum: int
{
    use Names, Options, Values;

    case SCHEDULED = 1;
    case CANCELED = 2;
    case ONGOING = 3;
    case DONE = 4;

}
