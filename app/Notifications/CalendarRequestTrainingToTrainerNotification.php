<?php

namespace App\Notifications;

use App\Mail\ScheduleNewTrainingToTrainer;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Spatie\IcalendarGenerator\Components\Calendar;
use Spatie\IcalendarGenerator\Components\Event;

class CalendarRequestTrainingToTrainerNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     */
    public function __construct(private $user, private $training, private $attendees)
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail()
    {

        $event = Event::create($this->training->skill->name)
            ->organizer(config('mail.from.address'), config('mail.from.name'))
            ->startsAt(Carbon::parse($this->training->start_at))
            ->endsAt(Carbon::parse($this->training->end_at))
            ->alertMinutesAfter(30, $this->training->skill->name)
            ->address($this->training->skill->location);

        foreach ($this->attendees as $attendee) {
            $event->attendee($attendee->email);
        }

        $calendar = Calendar::create()->event($event);

        return (new ScheduleNewTrainingToTrainer($this->user, $this->training))
            ->subject('Upcoming Training Session on ' . date('y/m/d', $this->training->date))
            ->to($this->user->email)
            ->attachData($calendar->get(), 'hapster.ics', [
                'mime' => 'text/calendar; charset=UTF-8',
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
