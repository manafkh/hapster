<?php

namespace App\Jobs;

use App\Services\ImportSkillService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
//TO DO: is there any reason we are sending file, location and userid as constructor parameters?
//If not, we can remove them and pass them as parameters to the handle method.
class ImportSkillJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $location;

    private $file;

    private $userId;

    public function __construct($file, $location, $userId)
    {
        $this->location = $location;
        $this->file = $file;
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     */
    public function handle(ImportSkillService $importSkillService): void
    {
     $importSkillService->prepareImportSkill($this->file, $this->location, $this->userId);
    }
}
