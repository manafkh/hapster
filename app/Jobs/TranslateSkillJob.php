<?php

namespace App\Jobs;

use App\Services\TranslateEngine\TranslateEngine;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
//TO DO: is there any reason we are sending source, skill, target and mode as constructor parameters?
//If not, we can remove them and pass them as parameters to the handle method.
class TranslateSkillJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    private $skill;
    private $source;
    private $target;
    private $mode;

    public function __construct($source,$skill,$target,$mode)
    {
      $this->source = $source;
      $this->skill = $skill;
      $this->target = $target;
      $this->mode = $mode;
    }

    /**
     * Execute the job.
     */
    public function handle(TranslateEngine $translateEngine): void
    {

      $request = $translateEngine->setRequest($this->source,$this->mode,$this->target,$this->skill);
      $response = $translateEngine->getData($request);

      $translateEngine->proccessData($response,$this->skill);
    }
}
