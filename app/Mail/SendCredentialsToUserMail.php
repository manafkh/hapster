<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendCredentialsToUserMail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $password;
    public $hasCredentials;

    /**
     * Create a new message instance.
     *
     * @param array $otpData
     */
    public function __construct($hasCredentials, $user, $password = null)
    {
        $this->user = $user;
        $this->password = $password;
        $this->hasCredentials = $hasCredentials;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.send_credentials_to_user');
    }
}
