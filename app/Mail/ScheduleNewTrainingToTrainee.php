<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ScheduleNewTrainingToTrainee extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $training;

    /**
     * Create a new message instance.
     *
     * @param  array  $otpData
     */
    public function __construct($user, $training)
    {
        $this->user = $user;
        $this->training = $training;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.scheduled_new_training_to_trainee');
    }
}
