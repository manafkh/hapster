<?php

namespace App\Imports;

use App\Models\Department;
use App\Models\Site;
use Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class DepartmentsImport implements ToModel, WithHeadingRow, WithValidation
{
    public function model(array $row)
    {
        $parentDepartment = isset($row['parent_name']) ?
            Department::where('name', 'ILIKE', $row['parent_name'])->firstOrFail() :
            null;

        return new Department([
            'parent_id' => $parentDepartment?->id,
            'name' => $row['name'],
            'description' => $row['description'],

            'site_id' => Site::where('name', 'ILIKE', $row['site_name'])->first()->id,
        ]);

    }
    //TO DO: localize this method
    public function rules(): array
    {
        return [
            'parent_name' => ['nullable', 'string',
                function ($attribute, $value, $onFailure) {
                    if (! Department::where('name', 'ILIKE', $value)->exists()) {
                        $onFailure("The {$attribute} is invalid.");
                    }
                }],
            'name' => ['required', 'unique:departments,name', 'string'],
            'description' => ['required', 'string'],

            'site_name' => ['required', function ($attribute, $value, $onFailure) {
                $user = Auth::guard('api')->user();
                $site = Site::where('name', 'ILIKE', $value)->first();

                if (! isset($site)) {
                    $onFailure("The {$attribute} is invalid.");
                } elseif (! $site->users()->where('users.id', $user->id)->exists()) {
                    $onFailure("The {$attribute} must be belongs to the user sites.");
                }

            }],
        ];
    }
}
