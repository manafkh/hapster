<?php

namespace App\Helpers;

class OperationResult
{
    public int $status = 200;

    public $data = null;

    public bool $isSuccess = true;

    public string $message = '';

    public function markAsFail(): self
    {
        $this->isSuccess = false;

        return $this;
    }

    public function setData($data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess === true;
    }

    public function isFail(): bool
    {
        return $this->isSuccess === false;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function isServerError(): bool
    {
        return $this->getStatus() >= 500 && $this->getStatus() < 600;
    }

    public function isClientError(): bool
    {
        return $this->getStatus() >= 400 && $this->getStatus() < 500;
    }

    public function markAsClientError(string $message = null, $data = null): self
    {
        $this->markAsFail()
            ->setStatus(400);

        if ($message !== null) {
            $this->setMessage($message);
        }

        if ($data !== null) {
            $this->setData($data);
        }

        return $this;
    }

    public function markAsInternalServerError(string $message = null, $data = null): self
    {
        $this->markAsFail()
            ->setStatus(500);

        if ($message !== null) {
            $this->setMessage($message);
        }

        if ($data !== null) {
            $this->setData($data);
        }

        return $this;
    }
}
