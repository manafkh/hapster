<?php

namespace Tests\Unit\Rules;

use App\Models\Department;
use App\Models\DepartmentSkill;
use App\Models\Skill;
use App\Models\User;
use App\Rules\SkillsBelongsToParentDepartmentRule;
use Tests\TestCase;

class SkillsBelongsToParentDepartmentRuleTest extends TestCase
{
    protected User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()
            ->create();

        $this->actingAs($this->user);
    }

    public function test_will_allow_skills_for_department_with_these_skills()
    {
        $passed = true;
        $fail = function () use (&$passed) {
            $passed = false;
        };

        $skills = Skill::factory()
            ->count(2)
            ->create();

        $department = Department::factory()
            ->create();

        $skillsBelongsToParentDepartmentRule = new SkillsBelongsToParentDepartmentRule($department->parent_id);
        $skillsBelongsToParentDepartmentRule->validate('skill_id', $skills->pluck('id')->toArray(), $fail);

        $this->assertTrue($passed);

        $passed = true;
        $childDepartment = Department::factory()
            ->for($department, 'parent')
            ->create();

        $skillsBelongsToParentDepartmentRule = new SkillsBelongsToParentDepartmentRule($childDepartment->parent_id);
        $skillsBelongsToParentDepartmentRule->validate('skill_id', $skills->pluck('id')->toArray(), $fail);

        $this->assertFalse($passed);

        $passed = true;
        DepartmentSkill::insert([
            [
                'department_id' => $department->id,
                'skill_id' => $skills[0]->id,
            ],
        ]);

        $skillsBelongsToParentDepartmentRule = new SkillsBelongsToParentDepartmentRule($childDepartment->parent_id);
        $skillsBelongsToParentDepartmentRule->validate('skill_id', $skills->pluck('id')->toArray(), $fail);

        $this->assertFalse($passed);

        $passed = true;
        $skillsBelongsToParentDepartmentRule = new SkillsBelongsToParentDepartmentRule($childDepartment->parent_id);
        $skillsBelongsToParentDepartmentRule->validate('skill_id', [$skills[0]->id], $fail);

        $this->assertTrue($passed);

        $passed = true;
        DepartmentSkill::insert([
            [
                'department_id' => $department->id,
                'skill_id' => $skills[1]->id,
            ],
        ]);

        $skillsBelongsToParentDepartmentRule = new SkillsBelongsToParentDepartmentRule($childDepartment->parent_id);
        $skillsBelongsToParentDepartmentRule->validate('skill_id', $skills->pluck('id')->toArray(), $fail);
        $this->assertTrue($passed);
    }
}
