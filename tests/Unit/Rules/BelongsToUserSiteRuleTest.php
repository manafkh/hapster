<?php

namespace Tests\Unit\Rules;

use App\Models\Site;
use App\Models\User;
use App\Models\UserSite;
use App\Rules\BelongsToUserSiteRule;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class BelongsToUserSiteRuleTest extends TestCase
{
    protected BelongsToUserSiteRule $BelongsToUserSiteRule;

    protected User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()
            ->create();

        $this->actingAs($this->user);

        $this->BelongsToUserSiteRule = App::make(BelongsToUserSiteRule::class);
    }

    public function test_will_allow_site_for_user_with_that_site()
    {
        $passed = true;
        $fail = function () use (&$passed) {
            $passed = false;
        };

        $site = Site::factory()
            ->create();

        $this->BelongsToUserSiteRule->validate('site_id', $site->id, $fail);

        $this->assertFalse($passed);

        $passed = true;

        UserSite::insert(['user_id' => $this->user->id, 'site_id' => $site->id]);

        $this->BelongsToUserSiteRule->validate('site_id', $site->id, $fail);

        $this->assertTrue($passed);
    }
}
