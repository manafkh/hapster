<?php

namespace Tests\Unit\Rules;

use App\Models\Department;
use App\Models\User;
use App\Rules\CheckDepartmentSameLevelRule;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Support\Facades\App;
use Tests\TestCase;

class CheckDepartmentSameLevelRuleTest extends TestCase
{
    protected CheckDepartmentSameLevelRule $checkDepartmentSameLevelRule;

    protected User $user;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()
            ->create();

        $this->actingAs($this->user);

        $this->checkDepartmentSameLevelRule = App::make(CheckDepartmentSameLevelRule::class);
    }

    public function test_will_allow_department_with_the_same_level_only()
    {
        $passed = true;
        $fail = function () use (&$passed) {
            $passed = false;
        };

        $parentDepartments = Department::factory()
            ->count(4)
            ->create();

        $this->checkDepartmentSameLevelRule->validate(
            'departments',
            $parentDepartments->pluck('id')->toArray(),
            $fail
        );
        $this->assertTrue($passed);

        $passed = true;
        $childDepartments = Department::factory()
            ->count(4)
            ->create(['parent_id' => $parentDepartments[0]->id]);

        $this->checkDepartmentSameLevelRule->validate(
            'departments',
            $childDepartments->pluck('id')->toArray(),
            $fail
        );
        $this->assertTrue($passed);

        $passed = true;
        $childDepartments = Department::factory()
            ->count(4)
            ->sequence(fn (Sequence $sequence) => ['parent_id' => $parentDepartments[$sequence->index]->id])
            ->create();

        $this->checkDepartmentSameLevelRule->validate(
            'departments',
            $childDepartments->pluck('id')->toArray(),
            $fail
        );
        $this->assertFalse($passed);
    }
}
