<?php

namespace Tests\Feature\Controller\V1;

use App\Enums\UserRoleEnum;
use App\Models\Department;
use App\Models\DepartmentSkill;
use App\Models\DepartmentUser;
use App\Models\Requests;
use App\Models\Site;
use App\Models\Skill;
use App\Models\User;
use App\Models\UserSite;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Tests\TestCase;

class DepartmentControllerTest extends TestCase
{
    protected User $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()
            ->create();

        DB::table('roles')
            ->updateOrInsert(
                ['id' => UserRoleEnum::Admin->value],
                ['name' => UserRoleEnum::Admin->name, 'guard_name' => 'api']
            );

        DB::table('model_has_roles')
            ->updateOrInsert([
                'model_id' => $this->user->id,
                'model_type' => User::class,
                'role_id' => UserRoleEnum::Admin->value,
            ], []);

        $this->actingAs($this->user);
    }

    public function test_can_get_all_departments()
    {
        $departments = Department::factory()
            ->count(4)
            ->state(new Sequence(
                ['sort_order' => 1],
                ['sort_order' => 2],
                ['sort_order' => 3],
                ['sort_order' => 4],
            ))
            ->hasAttached(Skill::factory())
            ->create();

        $subDepartments = Department::factory()
            ->count(3)
            ->sequence(fn (Sequence $sequence) => [
                'parent_id' => $departments[$sequence->index],
                'sort_order' => $sequence->index + 5,
            ])
            ->hasAttached(Skill::factory())
            ->create();

        DepartmentUser::insert([
            ['user_id' => $this->user->id, 'department_id' => $departments[0]->id],
            ['user_id' => $this->user->id, 'department_id' => $departments[1]->id],
            ['user_id' => $this->user->id, 'department_id' => $departments[3]->id],
            ['user_id' => $this->user->id, 'department_id' => $subDepartments[0]->id],
            ['user_id' => $this->user->id, 'department_id' => $subDepartments[2]->id],
        ]);

        $this->getJson(route('departments.index'))
            ->assertSuccessful()
            ->assertJsonCount(5, 'content.departments')
            ->assertJsonCount(1, 'content.departments.0.children')
            ->assertJsonCount(1, 'content.departments.0.skills')
            ->assertJsonCount(1, 'content.departments.0.children.0.skills');

        $departments = Department::factory()
            ->count(2)
            ->state(new Sequence(
                ['sort_order' => 8, 'name' => 'test_department_12345'],
                ['sort_order' => 9, 'name' => 'test_department_678'],
            ))
            ->create();

        DepartmentUser::insert([
            ['user_id' => $this->user->id, 'department_id' => $departments[0]->id],
            ['user_id' => $this->user->id, 'department_id' => $departments[1]->id],
        ]);

        $this->get(route('departments.index', [
            'filter' => ['id' => $departments[0]->id],
        ]))
            ->assertSuccessful()
            ->assertJsonCount(1, 'content.departments');

        $this->get(route('departments.index', [
            'filter' => ['search' => 'test_department'],
        ]))
            ->assertSuccessful()
            ->assertJsonCount(2, 'content.departments');

        $this->get(route('departments.index', [
            'filter' => ['is_parent' => false],
        ]))
            ->assertSuccessful()
            ->assertJsonCount(5, 'content.departments');

        $this->get(route('departments.index', [
            'filter' => ['is_parent' => true],
        ]))
            ->assertSuccessful()
            ->assertJsonCount(2, 'content.departments');
    }

    public function test_can_get_role_departments()
    {
        $sites = Site::factory()
            ->count(4)
            ->create();

        $departments = Department::factory()
            ->count(4)
            ->sequence(fn (Sequence $sequence) => ['site_id' => $sites[$sequence->index]->id])
            ->create();

        UserSite::insert([
            [
                'site_id' => $sites[0]->id,
                'user_id' => $this->user->id,
            ],
            [
                'site_id' => $sites[3]->id,
                'user_id' => $this->user->id,
            ],
        ]);

        DepartmentUser::insert([
            ['user_id' => $this->user->id, 'department_id' => $departments[0]->id],
            ['user_id' => $this->user->id, 'department_id' => $departments[1]->id],
        ]);

        $this->get(route('departments.index.role'))
            ->assertSuccessful()
            ->assertJsonCount(2, 'content.departments');
    }

    public function test_can_create_department_test()
    {
        $site = Site::factory()
            ->create();

        UserSite::insert(['site_id' => $site->id, 'user_id' => $this->user->id]);

        $skills = Skill::factory()
            ->count(3)
            ->create();

        $this->postJson(route('departments.create'), [
            'name' => 'department example',
            'site_id' => $site->id,
            'skills' => [$skills[0]->id, $skills[2]->id],
        ])
            ->assertSuccessful();

        $this->assertDatabaseHas(Department::class, [
            'name' => 'department example',
            'parent_id' => null,
            'description' => null,
            'site_id' => $site->id,
        ])
            ->assertDatabaseHas(DepartmentSkill::class, [
                'skill_id' => $skills[0]->id,
            ])
            ->assertDatabaseMissing(DepartmentSkill::class, [
                'skill_id' => $skills[1]->id,
            ])
            ->assertDatabaseHas(DepartmentSkill::class, [
                'skill_id' => $skills[2]->id,
            ]);
    }

    public function test_can_create_child_department_test()
    {
        $site = Site::factory()
            ->create();

        UserSite::insert(['site_id' => $site->id, 'user_id' => $this->user->id]);

        $skills = Skill::factory()
            ->count(3)
            ->create();

        $department = Department::factory()
            ->create();

        DepartmentSkill::insert([
            [
                'department_id' => $department->id,
                'skill_id' => $skills[0]->id,
            ],
            [
                'department_id' => $department->id,
                'skill_id' => $skills[2]->id,
            ],
        ]);

        $this->postJson(route('departments.create'), [
            'name' => 'child department example',
            'parent_id' => $department->id,
            'site_id' => $site->id,
            'skills' => [$skills[0]->id, $skills[2]->id],
            'description' => 'description',
        ])
            ->assertSuccessful();

        $this->assertDatabaseHas(Department::class, [
            'name' => 'child department example',
            'parent_id' => $department->id,
            'description' => 'description',
            'site_id' => $site->id,
        ]);

        $childDepartment = Department::query()
            ->where('name', 'child department example')
            ->first();

        $this->assertDatabaseHas(DepartmentSkill::class, [
            'skill_id' => $skills[0]->id,
            'department_id' => $childDepartment->id,
        ])
            ->assertDatabaseMissing(DepartmentSkill::class, [
                'skill_id' => $skills[1]->id,
                'department_id' => $childDepartment->id,
            ])
            ->assertDatabaseHas(DepartmentSkill::class, [
                'skill_id' => $skills[2]->id,
                'department_id' => $childDepartment->id,
            ]);
    }

    public function test_can_not_create_child_department_test_with_skill_not_on_parent()
    {
        $site = Site::factory()
            ->create();

        UserSite::insert(['site_id' => $site->id, 'user_id' => $this->user->id]);

        $skills = Skill::factory()
            ->count(3)
            ->create();

        $department = Department::factory()
            ->create();

        DepartmentSkill::insert([
            [
                'department_id' => $department->id,
                'skill_id' => $skills[0]->id,
            ],
            [
                'department_id' => $department->id,
                'skill_id' => $skills[2]->id,
            ],
        ]);

        $this->postJson(route('departments.create'), [
            'name' => 'child department example fail',
            'parent_id' => $department->id,
            'site_id' => $site->id,
            'skills' => [$skills[0]->id, $skills[1]->id],
            'description' => 'description',
        ])
            ->assertStatus(422);
    }

    public function test_can_reorder_departments()
    {
        $departments = Department::factory()
            ->count(4)
            ->sequence(fn (Sequence $sequence) => ['sort_order' => $sequence->index + 1])
            ->create();

        $this->postJson(route('departments.reorder'), [
            'department_sorted' => [
                $departments[3]->id,
                $departments[1]->id,
                $departments[2]->id,
                $departments[0]->id,

            ],
        ])
            ->assertSuccessful();

        $this->assertDatabaseHas(Department::class, [
            'id' => $departments[0]->id,
            'sort_order' => 4,
        ])
            ->assertDatabaseHas(Department::class, [
                'id' => $departments[1]->id,
                'sort_order' => 2,
            ])
            ->assertDatabaseHas(Department::class, [
                'id' => $departments[2]->id,
                'sort_order' => 3,
            ])
            ->assertDatabaseHas(Department::class, [
                'id' => $departments[3]->id,
                'sort_order' => 1,
            ]);
    }

    public function test_can_update_department_test()
    {
        $sites = Site::factory()
            ->count(2)
            ->create();

        UserSite::insert([
            ['site_id' => $sites[0]->id, 'user_id' => $this->user->id],
            ['site_id' => $sites[1]->id, 'user_id' => $this->user->id],
        ]);

        $department = Department::factory()
            ->create(['site_id' => $sites[0]->id, 'name' => 'example name']);

        $skills = Skill::factory()
            ->count(3)
            ->create();

        DepartmentSkill::insert([
            ['skill_id' => $skills[0]->id, 'department_id' => $department->id],
            ['skill_id' => $skills[2]->id, 'department_id' => $department->id],
        ]);

        $this->postJson(route('departments.update', ['department' => $department]), [
            'name' => 'department example',
            'site_id' => $sites[1]->id,
            'description' => null,
            'skills' => [$skills[0]->id, $skills[1]->id],
        ])
            ->assertSuccessful();

        $this->assertDatabaseHas(Department::class, [
            'name' => 'department example',
            'parent_id' => null,
            'description' => null,
            'site_id' => $sites[1]->id,
        ])
            ->assertDatabaseHas(DepartmentSkill::class, [
                'skill_id' => $skills[0]->id,
            ])
            ->assertDatabaseHas(DepartmentSkill::class, [
                'skill_id' => $skills[1]->id,
            ])
            ->assertDatabaseMissing(DepartmentSkill::class, [
                'skill_id' => $skills[2]->id,
            ]);
    }

    public function test_can_update_parent_department_to_child_department_test()
    {
        $sites = Site::factory()
            ->count(2)
            ->create();

        UserSite::insert([
            ['site_id' => $sites[0]->id, 'user_id' => $this->user->id],
            ['site_id' => $sites[1]->id, 'user_id' => $this->user->id],
        ]);

        $parentDepartment = Department::factory()
            ->create(['site_id' => $sites[0]->id]);

        $department = Department::factory()
            ->for($parentDepartment, 'parent')
            ->create(['site_id' => $sites[0]->id, 'name' => 'example name']);

        $skills = Skill::factory()
            ->count(5)
            ->create();

        DepartmentSkill::insert([
            ['skill_id' => $skills[0]->id, 'department_id' => $department->id],
            ['skill_id' => $skills[1]->id, 'department_id' => $department->id],
            ['skill_id' => $skills[2]->id, 'department_id' => $parentDepartment->id],
            ['skill_id' => $skills[3]->id, 'department_id' => $parentDepartment->id],
            ['skill_id' => $skills[4]->id, 'department_id' => $parentDepartment->id],
        ]);

        $this->postJson(route('departments.update', ['department' => $department]), [
            'name' => 'department example',
            'site_id' => $sites[1]->id,
            'description' => null,
            'skills' => [$skills[3]->id, $skills[4]->id],
        ])
            ->assertSuccessful();

        $this->assertDatabaseHas(Department::class, [
            'name' => 'department example',
            'parent_id' => $parentDepartment->id,
            'description' => null,
            'site_id' => $sites[1]->id,
        ])
            ->assertDatabaseHas(DepartmentSkill::class, [
                'skill_id' => $skills[3]->id,
            ])
            ->assertDatabaseHas(DepartmentSkill::class, [
                'skill_id' => $skills[4]->id,
            ])
            ->assertDatabaseMissing(DepartmentSkill::class, [
                'skill_id' => $skills[0]->id,
            ])
            ->assertDatabaseMissing(DepartmentSkill::class, [
                'skill_id' => $skills[1]->id,
            ]);
    }

    public function test_can_delete_department()
    {
        $department = Department::factory()
            ->create();

        $anotherDepartment = Department::factory()
            ->create();

        $skills = Skill::factory()
            ->count(5)
            ->create();

        DepartmentSkill::insert([
            ['skill_id' => $skills[0]->id, 'department_id' => $department->id],
            ['skill_id' => $skills[1]->id, 'department_id' => $department->id],
            ['skill_id' => $skills[2]->id, 'department_id' => $department->id],
            ['skill_id' => $skills[2]->id, 'department_id' => $anotherDepartment->id],
            ['skill_id' => $skills[3]->id, 'department_id' => $anotherDepartment->id],
            ['skill_id' => $skills[4]->id, 'department_id' => $anotherDepartment->id],
        ]);

        DepartmentUser::insert([
            ['user_id' => $this->user->id, 'department_id' => $department->id],
            ['user_id' => $this->user->id, 'department_id' => $anotherDepartment->id],
        ]);

        Requests::factory()
            ->for($this->user)
            ->count(4)
            ->state(new Sequence(
                ['skill_id' => $skills[0]->id],
                ['skill_id' => $skills[1]->id],
                ['skill_id' => $skills[2]->id],
                ['skill_id' => $skills[3]->id],
            ))
            ->create();

        $this->deleteJson(route('departments.destroy', ['department' => $department->id]))
            ->assertSuccessful();

        $this->assertSoftDeleted(Department::class, ['id' => $department->id])
            ->assertDatabaseMissing(DepartmentSkill::class, [
                'department_id' => $department->id,
            ])
            ->assertDatabaseHas(Requests::class, [
                'skill_id' => $skills[0]->id,
                'user_id' => $this->user->id,
                'is_access' => false,
            ])
            ->assertDatabaseHas(Requests::class, [
                'skill_id' => $skills[1]->id,
                'user_id' => $this->user->id,
                'is_access' => false,
            ])
            ->assertDatabaseHas(Requests::class, [
                'skill_id' => $skills[2]->id,
                'user_id' => $this->user->id,
                'is_access' => true,
            ])
            ->assertDatabaseHas(Requests::class, [
                'skill_id' => $skills[3]->id,
                'user_id' => $this->user->id,
                'is_access' => true,
            ]);
    }

    public function test_can_import_departments()
    {
        Excel::fake();

        $this->postJson(route('departments.import'), [
            'file' => UploadedFile::fake()->create('departments.xlsx'),
        ]);

        Excel::assertImported('departments.xlsx');
    }
}
