<?php

namespace Tests\Feature\Controller\V1;

use App\Enums\UserRoleEnum;
use App\Mail\OTPMail;
use App\Models\License;
use App\Models\Site;
use App\Models\User;
use App\Models\UserSite;
use App\Services\Keycloak\KeycloakEndPoints;
use Carbon\Carbon;
use Database\Seeders\RolePremissionSeeder;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    private string $keyClockBaseUrl;

    private KeycloakEndPoints $keyCloakEndPoints;

    public function setUp(): void
    {
        parent::setUp();
        $this->keyClockBaseUrl = config('keycloak.url');
        $this->keyCloakEndPoints = App::make(KeycloakEndPoints::class);
    }

    public function test_can_register()
    {
        $this->seed(RolePremissionSeeder::class);

        Http::fake([
            $this->keyClockBaseUrl.'/'.$this->keyCloakEndPoints->getUsers() => Http::response(
                [
                    'status' => true,
                    'access_token' => 'test_access_token',
                    'refresh_token' => 'test_refresh_token',
                ],
            ),
            $this->keyClockBaseUrl.'/'.$this->keyCloakEndPoints->token() => Http::response([
                'access_token' => 'test_access_token',
            ]),
        ]);

        $sites = Site::factory()
            ->count(3)
            ->create();

        $userData = [
            'first_name' => 'test_first_name',
            'last_name' => 'test_last_name',
            'username' => 'test',
            'email' => 'test@gmail.com',
            'phone_number' => '85214736',
            'password' => '0123456@passwordN',
            'password_confirmation' => '0123456@passwordN',
            'avatar' => UploadedFile::fake()->image('avatar.png'),
            'sites' => [$sites[0]->id],
        ];

        $this->postJson(route('user.register'), $userData)
            ->assertSuccessful();

        $this->assertDatabaseHas(User::class, [
            'first_name' => 'test_first_name',
            'last_name' => 'test_last_name',
            'username' => 'test',
            'email' => 'test@gmail.com',
            'phone_number' => '85214736',
        ]);

        $user = User::firstWhere('email', 'test@gmail.com');

        $this->assertDatabaseHas('user_site', [
            'user_id' => $user->id,
            'site_id' => $sites[0]->id,
        ])
            ->assertDatabaseMissing('user_site', [
                'user_id' => $user->id,
                'site_id' => $sites[1]->id,
            ])
            ->assertDatabaseMissing('user_site', [
                'user_id' => $user->id,
                'site_id' => $sites[2]->id,
            ])
            ->assertDatabaseHas('model_has_roles', [
                'model_id' => $user->id,
                'model_type' => User::class,
                'role_id' => UserRoleEnum::Admin->value,
            ]);
    }

    public function test_can_not_login_with_expired_license()
    {
        $user = User::factory()
            ->create(['email' => 'example@email.com']);

        $site = Site::factory()
            ->create();

        UserSite::insert([
            'site_id' => $site->id,
            'user_id' => $user->id,
        ]);

        License::factory()
            ->for($site)
            ->create(['end_date' => Carbon::now()->subMonth()]);

        Http::fake([
            $this->keyClockBaseUrl.'/'.$this->keyCloakEndPoints->token() => Http::response(
                [
                    'status' => true,
                    'access_token' => 'test_access_token',
                    'refresh_token' => 'test_refresh_token',
                ],
            ),
        ]);

        $this->postJson(route('user.login'), [
            'email' => 'example@email.com',
            'password' => 'test_password',
        ])
            ->assertStatus(408);
    }

    public function test_can_login_with_email()
    {
        $user = User::factory()
            ->create(['email' => 'example@email.com']);

        $site = Site::factory()
            ->create();

        UserSite::insert([
            'site_id' => $site->id,
            'user_id' => $user->id,
        ]);

        License::factory()
            ->for($site)
            ->create();

        Http::fake([
            $this->keyClockBaseUrl.'/'.$this->keyCloakEndPoints->token() => Http::response(
                [
                    'status' => true,
                    'access_token' => 'test_access_token',
                    'refresh_token' => 'test_refresh_token',
                ],
            ),
        ]);

        $this->postJson(route('user.login'), [
            'email' => 'example@email.com',
            'password' => 'test_password',
        ])
            ->assertSuccessful()
            ->assertJsonPath('content.token', 'test_access_token')
            ->assertJsonPath('content.refresh_token', 'test_refresh_token')
            ->assertJsonPath('content.user.id', $user->id);
    }

    public function test_can_send_email_to_not_verified_user()
    {
        $this->freezeTime();

        $user = User::factory()
            ->create([
                'email' => 'email@example.com',
                'email_verified_at' => null,
                'otp' => null,
                'otp_expiry' => null,
            ]);

        Mail::fake();

        $this->actingAs($user);

        $this->postJson(route('user.send-verification-email'), ['email' => 'email@example.com'])
            ->assertSuccessful();

        Mail::assertSent(function (OTPMail $mail) use ($user) {
            return $mail->user->id === $user->id;
        });

        $this->assertNotNull($user->refresh()->otp);
        $this->assertEquals(
            $user->otp_expiry->format('Y-m-d H:i:s'),
            Carbon::now()->addMinutes(3)->format('Y-m-d H:i:s')
        );
    }

    public function test_can_not_send_email_to_already_verified_user()
    {
        $this->freezeTime();

        $user = User::factory()
            ->create([
                'email' => 'email@example.com',
                'email_verified_at' => Carbon::now(),
                'otp' => null,
                'otp_expiry' => null,
            ]);

        Mail::fake();

        $this->actingAs($user);

        $this->postJson(route('user.send-verification-email'), ['email' => 'email@example.com'])
            ->assertStatus(400);

        Mail::assertNotSent(OTPMail::class);

        $this->assertNull($user->refresh()->otp);
        $this->assertNull($user->otp_expiry);
    }

    public function test_can_verify_email()
    {
        Http::fake([
            $this->keyClockBaseUrl.'/'.$this->keyCloakEndPoints->token() => Http::response(
                [
                    'status' => true,
                    'access_token' => 'test_access_token',
                    'refresh_token' => 'test_refresh_token',
                ],
            ),
        ]);

        $this->freezeTime();

        $user = User::factory()
            ->create([
                'email' => 'email@example.com',
                'email_verified_at' => null,
                'otp' => Hash::make('123456'),
                'otp_expiry' => Carbon::now()->addMinutes(12),
            ]);

        $this->actingAs($user);

        $this->postJson(
            route('user.verify-email'),
            [
                'otp' => '123456',
                'email' => 'email@example.com',
                'password' => 'secret',
            ])
            ->assertSuccessful();

        $this->assertNull($user->refresh()->otp);
        $this->assertNull($user->otp_expiry);
        $this->assertEquals(
            $user->refresh()->email_verified_at->format('Y-m-d H:i:s'),
            Carbon::now()->format('Y-m-d H:i:s')
        );
    }

    public function test_can_not_verify_email_with_wrong_otp_code()
    {
        $this->freezeTime();

        $user = User::factory()
            ->create([
                'email' => 'email@example.com',
                'email_verified_at' => null,
                'otp' => Hash::make('12345'),
                'otp_expiry' => Carbon::now()->addMinutes(12),
            ]);

        $this->actingAs($user);

        $this->postJson(
            route('user.verify-email'),
            [
                'otp' => '123456',
                'email' => 'email@example.com',
                'password' => 'secret',
            ])
            ->assertStatus(401);

        $this->assertNull($user->email_verified_at);
    }

    public function test_can_not_verify_email_with_expired_otp_code()
    {
        $this->freezeTime();

        $user = User::factory()
            ->create([
                'email' => 'email@example.com',
                'email_verified_at' => null,
                'otp' => Hash::make('123456'),
                'otp_expiry' => Carbon::now()->subMinutes(1),
            ]);

        $this->actingAs($user);

        $this->postJson(
            route('user.verify-email'),
            [
                'otp' => '123456',
                'email' => 'email@example.com',
                'password' => 'secret',
            ])
            ->assertStatus(401);

        $this->assertNull($user->email_verified_at);
    }
}
