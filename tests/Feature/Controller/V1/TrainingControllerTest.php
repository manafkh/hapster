<?php

namespace Tests\Feature\Controller\V1;

use App\Enums\TrainingStatusEnum;
use App\Enums\UserRoleEnum;
use App\Models\Department;
use App\Models\DepartmentTrainee;
use App\Models\DepartmentUser;
use App\Models\Requests;
use App\Models\Site;
use App\Models\SiteSkill;
use App\Models\Skill;
use App\Models\Stage;
use App\Models\Trainee;
use App\Models\TraineeTraining;
use App\Models\Training;
use App\Models\TrainingStage;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class TrainingControllerTest extends TestCase
{
    protected User $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()
            ->create();

        DB::table('roles')
            ->updateOrInsert(
                ['id' => UserRoleEnum::Admin->value],
                ['name' => UserRoleEnum::Admin->name, 'guard_name' => 'api']
            );

        DB::table('roles')
            ->updateOrInsert(
                ['id' => UserRoleEnum::Trainer->value],
                ['name' => UserRoleEnum::Trainer->name, 'guard_name' => 'api']
            );

        DB::table('model_has_roles')
            ->updateOrInsert([
                'model_id' => $this->user->id,
                'model_type' => User::class,
                'role_id' => UserRoleEnum::Trainer->value,
            ], []);


        $this->actingAs($this->user);
    }

    public function test_can_get_my_trainings()
    {
        Training::factory()
            ->for($this->user)
            ->has(Skill::factory())
            ->has(Stage::factory())
            ->has(Trainee::factory())
            ->count(5)
            ->create();

        Training::factory()
            ->for(User::factory())
            ->has(Skill::factory())
            ->has(Stage::factory())
            ->has(Trainee::factory())
            ->count(2)
            ->create();

        $this->getJson(route('trainings.my-trainings'))
            ->assertSuccessful()
            ->assertJsonCount(5, 'content.my_trainings');
    }

    public function test_can_get_training_schedule()
    {
        Training::factory()
            ->for($this->user)
            ->has(Skill::factory())
            ->has(Stage::factory())
            ->has(Trainee::factory())
            ->count(5)
            ->state(new Sequence(
                ['status' => TrainingStatusEnum::SCHEDULED],
                ['status' => TrainingStatusEnum::DONE],
                ['status' => TrainingStatusEnum::ONGOING],
            ))
            ->create();

        Training::factory()
            ->for(User::factory())
            ->has(Skill::factory())
            ->has(Stage::factory())
            ->has(Trainee::factory())
            ->state(new Sequence(
                ['status' => TrainingStatusEnum::SCHEDULED],
                ['status' => TrainingStatusEnum::DONE],
            ))
            ->count(2)
            ->create();

        $this->getJson(route('trainings.schedule', ['user_id' => $this->user->id]))
            ->assertSuccessful()
            ->assertJsonCount(2, 'content.training_times');
    }

    public function test_can_get_trainings_metadata()
    {
        Training::factory()
            ->for($this->user)
            ->has(Skill::factory())
            ->has(Stage::factory())
            ->has(Trainee::factory())
            ->count(5)
            ->create();

        Training::factory()
            ->for(User::factory())
            ->has(Skill::factory())
            ->has(Stage::factory())
            ->has(Trainee::factory())
            ->count(2)
            ->create();

        $this->getJson(route('trainings.metadata'))
            ->assertSuccessful()
            ->assertJsonCount(5, 'content.trainings');
    }

    public function test_can_get_stages()
    {
        Stage::factory()
            ->count(2)
            ->create();

        $this->getJson(route('trainings.stages'))
            ->assertSuccessful()
            ->assertJsonCount(2, 'content.trainings');
    }

    public function test_can_get_info()
    {
        $skill = Skill::factory()
            ->create();

        SiteSkill::insert([
            ['skill_id' => $skill->id, 'site_id' => Site::factory()->create()->id]
        ]);

        $training = Training::factory()
            ->for($this->user)
            ->for($skill)
            ->has(Skill::factory()->for(Site::factory()))
            ->has(Trainee::factory())
            ->create();

        $stages = Stage::factory()
            ->count(3)
            ->create();

        TrainingStage::insert([
            ['training_id' => $training->id, 'signing_responsible' => $this->user->id, 'stage_id' => $stages[0]->id],
            ['training_id' => $training->id, 'signing_responsible' => $this->user->id, 'stage_id' => $stages[1]->id],
        ]);

        $this->getJson(route('trainings.info', ['training' => $training->id]))
            ->assertSuccessful()
            ->assertJsonPath('content.trainings.id', $training->id);
    }

    public function test_can_create_training()
    {
        Notification::fake();

        $skill = Skill::factory()
            ->create();

        SiteSkill::insert([
            ['skill_id' => $skill->id, 'site_id' => Site::factory()->create()->id]
        ]);

        $trainees = Trainee::factory()
            ->count(3)
            ->create();

        $stages = Stage::factory()
            ->count(3)
            ->create();

        Stage::factory()
            ->create(['name' => 'Signature']);

        Requests::factory()
            ->for($this->user)
            ->for($skill)
            ->create();

        $department = Department::factory()
            ->create();

        DepartmentUser::insert([
            'user_id' => $this->user->id,
            'department_id' => $department->id
        ]);

        DepartmentTrainee::insert([
            ['trainee_id' => $trainees[0]->id, 'department_id' => $department->id],
            ['trainee_id' => $trainees[1]->id, 'department_id' => $department->id],
        ]);

        $this->postJson(route('trainings.create'), [
            'user_id' => $this->user->id,
            'skill_id' => $skill->id,
            'trainees' => [$trainees[0]->id, $trainees[1]->id],
            'trainings' => [
                [
                    'date' => '2023-12-12',
                    'from' => '12:10',
                    'to' => '14:10',
                    'stages' => [
                        [
                            'stage_id' => $stages[0]->id,
                        ],
                        [
                            'stage_id' => $stages[2]->id,
                        ]
                    ]
                ]
            ]
        ])
            ->assertSuccessful();

        $this->assertDatabaseHas(Training::class, [
            'date' => '2023-12-12',
            'from' => '12:10',
            'to' => '14:10',
            'skill_id' => $skill->id,
            'user_id' => $this->user->id,
        ]);

        $training = Training::query()
            ->where([
                'date' => '2023-12-12',
                'from' => '12:10',
                'to' => '14:10',
            ])
            ->first();

        $this->assertDatabaseHas(TraineeTraining::class, [
            'trainee_id' => $trainees[0]->id,
            'training_id' => $training->id,
        ])
            ->assertDatabaseHas(TraineeTraining::class, [
                'trainee_id' => $trainees[1]->id,
                'training_id' => $training->id,
            ])
            ->assertDatabaseHas(TrainingStage::class, [
                'stage_id' => $stages[0]->id,
                'training_id' => $training->id,
            ])
            ->assertDatabaseHas(TrainingStage::class, [
                'stage_id' => $stages[2]->id,
                'training_id' => $training->id,
            ]);
    }

    public function test_can_update_training_status()
    {
        $skill = Skill::factory()
            ->create();

        SiteSkill::insert([
            ['skill_id' => $skill->id, 'site_id' => Site::factory()->create()->id]
        ]);

        $training = Training::factory()
            ->for($this->user)
            ->for($skill)
            ->has(Skill::factory()->for(Site::factory()))
            ->has(Trainee::factory())
            ->create(['status' => TrainingStatusEnum::SCHEDULED]);

        $this->putJson(route('trainings.update.status'), [
            'training_id' => $training->id,
            'status' => TrainingStatusEnum::DONE
        ])
        ->assertSuccessful();

        $this->assertDatabaseHas(Training::class, [
            'id' => $training->id,
            'status' => TrainingStatusEnum::DONE
        ]);
    }
}
