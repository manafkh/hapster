<?php

namespace Tests\Feature\Controller\V1;

use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class PasswordControllerTest extends TestCase
{
    public function test_can_send_reset_link_to_exists_user()
    {
        Notification::fake();

        $user = User::factory()
            ->create(['email' => 'email@example.com']);

        $this->postJson(route('user.password.reset-email.send'), ['email' => 'email@example.com'])
            ->assertSuccessful();

        $this->assertTrue($user->refresh()->is_reset);

        Notification::assertSentToTimes($user, ResetPassword::class, 1);
    }

    public function test_can_not_send_reset_link_to_not_exists_user()
    {
        Notification::fake();

        $this->postJson(route('user.password.reset-email.send'), ['email' => 'email@example.com'])
            ->assertStatus(422);

        Notification::assertNothingSent();
    }
}
