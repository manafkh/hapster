<?php

namespace Tests\Feature\Controller\V1;

use App\Enums\UserRoleEnum;
use App\Models\Department;
use App\Models\DepartmentSkill;
use App\Models\DepartmentUser;
use App\Models\Requests;
use App\Models\Skill;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class SkillControllerTest extends TestCase
{
    protected User $user;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()
            ->create();

        DB::table('roles')
            ->updateOrInsert(
                ['id' => UserRoleEnum::Admin->value],
                ['name' => UserRoleEnum::Admin->name, 'guard_name' => 'api']
            );

        DB::table('model_has_roles')
            ->updateOrInsert([
                'model_id' => $this->user->id,
                'model_type' => User::class,
                'role_id' => UserRoleEnum::Admin->value,
            ], []);

        $this->actingAs($this->user);
    }

    public function test_can_get_skills()
    {
        Skill::factory()
            ->count(3)
            ->create();

        $this->getJson(route('skills.index'))
            ->assertSuccessful()
            ->assertJsonCount(3, 'content.skills');
    }

    public function test_can_show_skill()
    {
        $skill = Skill::factory()
            ->create();

        $this->getJson(route('skills.show', ['skill' => $skill->id]))
            ->assertSuccessful()
            ->assertJsonPath('content.skill.id', $skill->id);
    }

    public function test_can_get_accessible_skills()
    {
        $departments = Department::factory()
            ->count(3)
            ->create();

        DepartmentUser::insert([
            ['department_id' => $departments[0]->id, 'user_id' => $this->user->id],
            ['department_id' => $departments[1]->id, 'user_id' => $this->user->id],
        ]);

        $skills = Skill::factory()
            ->count(5)
            ->create();

        DepartmentSkill::insert([
            ['skill_id' => $skills[0]->id, 'department_id' => $departments[0]->id],
            ['skill_id' => $skills[0]->id, 'department_id' => $departments[1]->id],
            ['skill_id' => $skills[1]->id, 'department_id' => $departments[0]->id],
            ['skill_id' => $skills[2]->id, 'department_id' => $departments[1]->id],
            ['skill_id' => $skills[3]->id, 'department_id' => $departments[0]->id],
            ['skill_id' => $skills[4]->id, 'department_id' => $departments[2]->id],
        ]);

        Requests::factory()
            ->count(2)
            ->state(new Sequence(
                ['user_id' => $this->user->id, 'skill_id' => $skills[0]->id, 'is_access' => true],
                ['user_id' => $this->user->id, 'skill_id' => $skills[1]->id, 'is_access' => false],
                ['user_id' => $this->user->id, 'skill_id' => $skills[4]->id, 'is_access' => false],
            ))
            ->create();

        $this->getJson(route('skills.accessible'))
            ->assertSuccessful()
            ->assertJsonCount(2, 'content.accessible_skills')
            ->assertJsonCount(1, 'content.accessible_skills.0.skills')
            ->assertJsonCount(1, 'content.accessible_skills.1.skills')
            ->assertJsonPath('content.accessible_skills.0.skills.0.id', $skills[0]->id)
            ->assertJsonPath('content.accessible_skills.1.skills.0.id', $skills[0]->id);
    }
}
