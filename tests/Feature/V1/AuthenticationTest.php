<?php

namespace Tests\Feature\V1;

use App\Models\Site;
use App\Models\User;
use App\Services\Keycloak\KeycloakService;
use Database\Seeders\RolePremissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Mockery\MockInterface;
use Str;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;

    public function test_register_new_user_successfully()
    {
        $this->seed(RolePremissionSeeder::class);
        $site = Site::factory()->create();

        $user = [
            'first_name' => 'test_first_name',
            'last_name' => 'test_last_name',
            'username' => 'test',
            'email' => 'test@gmail.com',
            'phone_number' => '85214736',
            'password' => '0123456@passwordN',
            'password_confirmation' => '0123456@passwordN',
            'avatar' => UploadedFile::fake()->image('avatar.png'),
            'sites' => [$site->id],
        ];

        $this->partialMock(KeycloakService::class, function (MockInterface $mock) use ($user) {
            $mock->shouldReceive('addUserToKeycloak')
                ->with($user['username'], $user['email'], $user['password'])
                ->andReturn(true);
        });
        $this->app->forgetInstance(KeycloakService::class);

        $response = $this->postJson('/api/v1/oauth/user/register', $user);

        $response->assertOk();
        $response->assertJsonFragment([
            'user' => $user,
        ]);

        $response->assertJsonMissing(['token']);
    }

    public function test_login_user_with_email_successfully()
    {
        $user = [
            'first_name' => 'test_first_name',
            'last_name' => 'test_last_name',
            'username' => 'test',
            'email' => 'test@gmail.com',
            'phone_number' => '85214736',
        ];

        $user = User::create($user);

        $access_token = Str::random('100');
        $refresh_token = Str::random('100');

        $this->assertTrue(true);
        //
        //
        //        $response = $this->postJson('/api/v1/oauth/user/login', [
        //            'email' => 'test@gmail.com',
        //            'password' => 'password',
        //        ]);
        //
        //
        //        $response->assertOk();
        //        $response->assertJsonFragment([
        //            'token' => $this->access_token,
        //            'refresh_token' => $this->refresh_token,
        //        ]);
        ////
        //        $response->assertJsonFragment([
        //            'user' => $user,
        //        ]);
    }
}
