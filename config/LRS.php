<?php

return [
    'USER'=>env('LRS_USER') ,
    'PASSWORD'=>env('LRS_PASSWORD'),
    'URL'=> env('LRS_URL') ,
];