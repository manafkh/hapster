<?php

return [
    'engine'=>[
        'url' => env('TRANSLATE_ENGINE_URL','https://tts.staging-hsp.com/submit')
    ]
];
