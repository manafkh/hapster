<?php

return [
    'url' => env('KEYCLOAK_URL'),

    'client_id' => env('KEYCLOAK_CLIENT_ID'),
    'client_secret' => env('KEYCLOAK_CLIENT_SECRET'),
    'realm' => env('KEYCLOAK_REALM'),

    'realm_public_key' => env('KEYCLOAK_REALM_PUBLIC_KEY', null),
    'load_user_from_database' => env('KEYCLOAK_LOAD_USER_FROM_DATABASE', true),
    'user_provider_custom_retrieve_method' => null,
    'user_provider_credential' => env('KEYCLOAK_USER_PROVIDER_CREDENTIAL', 'username'),
    'token_principal_attribute' => env('KEYCLOAK_TOKEN_PRINCIPAL_ATTRIBUTE', 'preferred_username'),
    'append_decoded_token' => env('KEYCLOAK_APPEND_DECODED_TOKEN', false),
    'allowed_resources' => env('KEYCLOAK_ALLOWED_RESOURCES', null),
    'ignore_resources_validation' => env('KEYCLOAK_IGNORE_RESOURCES_VALIDATION', false),
    'leeway' => env('KEYCLOAK_LEEWAY', 0),
    'input_key' => env('KEYCLOAK_TOKEN_INPUT_KEY', null),
    'keycloak_admin_username' => env('KEYCLOAK_ADMIN_USERNAME', 'admin'),
    'keycloak_admin_password' => env('KEYCLOAK_ADMIN_PASSWORD', 'admin'),

    //    'routes' => [
    //        'token' => '/realms/{realm}/protocol/openid-connect/token',
    //        'users' => '/admin/realms/{realm}/users',
    //        'reset_password' => '/admin/realms/{realm}/users/{keycloak_user_id}/reset-password',
    //        'get_user_by_email' => '/admin/realms/{realm}/users?email={email}',
    //        'delete_user' => '/admin/realms/{realm}/users/{keycloak_user_id}',
    //    ],
];
