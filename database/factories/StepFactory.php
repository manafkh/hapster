<?php

namespace Database\Factories;

use App\Models\Step;
use Illuminate\Database\Eloquent\Factories\Factory;

class StepFactory extends Factory
{
    protected $model = Step::class;

    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'sort_order' => $this->faker->randomDigitNotNull,
            'skill_id' => null,
        ];
    }
}
