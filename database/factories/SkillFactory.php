<?php

namespace Database\Factories;

use App\Models\Skill;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SkillFactory extends Factory
{
    protected $model = Skill::class;

    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'confidentiality_level' => $this->faker->randomElement([1, 2, 3, 4, 5]),
            'type' => $this->faker->randomElement([1, 2]),
            'writing_date' => $this->faker->date(),
            'author' => $this->faker->name,
            'location' => $this->faker->city,
            'importance' => $this->faker->randomDigitNotNull,
            'required_skill' => $this->faker->word,
            'required_safety_equipments' => $this->faker->sentence,
            'required_materials_parts' => $this->faker->sentence,
            'tools_supplies_machinery' => $this->faker->sentence,
        ];
    }
}
