<?php

namespace Database\Factories;

use App\Models\Registry;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Registry>
 */
class RegistryFactory extends Factory
{
    protected $table = Registry::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'ip_address' => null,
            'ip_range' => null,
            'mac_address' => $this->faker->unique()->macAddress(),
            'required_ip' => null,
            'token' => null,
            'token_expired_at' => null,
            'refresh_token' => null,
            'refresh_token_expired_at' => null,
            'node_id' => null,
            'service_id' => null,
        ];
    }

    public function withToken()
    {
        return $this->state(function () {
            return [
                'token' => md5(Str::orderedUuid()->toString().microtime()),
                'token_expired_at' => Carbon::now()->addDays(30),
                'refresh_token' => md5(Str::orderedUuid()->toString().microtime()),
                'refresh_token_expired_at' => Carbon::now()->addDays(90),
            ];
        });
    }
}
