<?php

namespace Database\Factories;

use App\Models\Keypoint;
use Illuminate\Database\Eloquent\Factories\Factory;

class KeypointFactory extends Factory
{
    protected $model = Keypoint::class;

    public function definition()
    {
        return [
            'step_id' => null,
            'name' => $this->faker->word,
            // 'media' => 'http://hrmpractice.com/wp-content/uploads/2019/12/HR-Management-Key-Skills1.jpg',
            'reason' => $this->faker->sentence,
            'type' => $this->faker->randomElement([1, 2, 3]),
        ];
    }
}
