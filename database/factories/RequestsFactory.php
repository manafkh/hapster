<?php

namespace Database\Factories;

use App\Enums\AccessTypeToSkillEnum;
use App\Models\Requests;
use App\Models\Skill;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class RequestsFactory extends Factory
{
    protected $model = Requests::class;

    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'skill_id' => Skill::factory(),
            'access_type' => AccessTypeToSkillEnum::PERMANENT,
            'from_date' => Carbon::now()->format('Y-m-d'),
            'to_date' => Carbon::now()->addMonth()->format('Y-m-d'),
            'is_access' => true,
        ];

    }
}
