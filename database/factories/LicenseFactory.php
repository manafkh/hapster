<?php

namespace Database\Factories;

use App\Models\License;
use App\Models\Site;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\License>
 */
class LicenseFactory extends Factory
{
    protected $model = License::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'site_id' => Site::factory(),
            'start_date' => Carbon::now()->subYear(),
            'end_date' => Carbon::now()->addYear(),
        ];
    }
}
