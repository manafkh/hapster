<?php

namespace Database\Factories;

use App\Enums\TrainingStatusEnum;
use App\Models\Skill;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Training>
 */
class TrainingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'skill_id' => Skill::factory(),
            'date' => $this->faker->date(),
            'from' => $this->faker->time(),
            'to' => $this->faker->time(),
            'status' => TrainingStatusEnum::SCHEDULED,
        ];
    }
}
