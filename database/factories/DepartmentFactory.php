<?php

namespace Database\Factories;

use App\Models\Department;
use App\Models\Site;
use Illuminate\Database\Eloquent\Factories\Factory;

class DepartmentFactory extends Factory
{
    protected $model = Department::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name.random_int(1, 100000),
            'sort_order' => $this->faker->numberBetween(1, 100),
            'description' => $this->faker->sentence,
            'site_id' => Site::factory(), // Customize the site_id options
        ];

    }
}
