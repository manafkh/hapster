<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Trainee;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TraineeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $avatarBaseUrl = 'https://i.pravatar.cc/150?u=';

        if (! Trainee::exists()) {
            $departmentsSite1 = Department::where('site_id', 1)->pluck('id')->toArray();
            $departmentsSite2 = Department::where('site_id', 2)->pluck('id')->toArray();
            $user = User::where('username', 'oscar')->first();
            $tom = Trainee::create([
                'user_id' => $user->id,
                'trainee_identify' => '100220',
                'first_name' => 'tom',
                'last_name' => 'jon',
                'email' => 'tom@gmail.com',
                'phone_number' => '78954123',
            ]);

            $tom->addMediaFromUrl($avatarBaseUrl.Str::random(5))
                ->toMediaCollection('avatar');
            $tom->departments()->sync($departmentsSite1);

            $jone = Trainee::create([
                'user_id' => $user->id,
                'trainee_identify' => '104220',
                'first_name' => 'jone',
                'last_name' => 'jon',
                'email' => 'jone@gmail.com',
                'phone_number' => '45587845',
            ]);
            $jone->departments()->sync($departmentsSite1);
            $jone->addMediaFromUrl($avatarBaseUrl.Str::random(5))
                ->toMediaCollection('avatar');
            $vane = Trainee::create([
                'user_id' => $user->id,
                'trainee_identify' => '140220',
                'first_name' => 'vane',
                'last_name' => 'jon',
                'email' => 'vane@gmail.com',
                'phone_number' => '456456',
            ]);
            $vane->departments()->sync($departmentsSite1);
            $vane->addMediaFromUrl($avatarBaseUrl.Str::random(5))
                ->toMediaCollection('avatar');
            $saul = Trainee::create([
                'user_id' => $user->id,
                'trainee_identify' => '1410220',
                'first_name' => 'saul',
                'last_name' => 'jon',
                'email' => 'saul@gmail.com',
                'phone_number' => '456456',
            ]);
            $vane->addMediaFromUrl($avatarBaseUrl.Str::random(5))
                ->toMediaCollection('avatar');
            $saul->departments()->sync($departmentsSite2);
        }
    }
}
