<?php

namespace Database\Seeders;

use App\Models\Stage;
use Illuminate\Database\Seeder;

class StageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (! Stage::exists()) {
            Stage::create([
                'name' => 'Theory',
            ]);

            Stage::create([
                'name' => 'Autonomy',
            ]);

            Stage::create([
                'name' => 'Practice',
            ]);

            Stage::create([
                'name' => 'Signature',
            ]);
        }

    }
}
