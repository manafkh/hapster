<?php

namespace Database\Seeders;

use App\Enums\UserRoleEnum;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePremissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        foreach (UserRoleEnum::options() as $name => $id) {
            $user = Role::firstOrCreate(['id' => $id], [
                'name' => $name,
            ]);
        }

        //
        //        foreach (config('permission.capturer') as $permission) {
        //            Permission::findOrCreate($permission);
        //            $Capturer->givePermissionTo($permission);
        //        }
        //
        //        foreach (config('permission.admin') as $permission) {
        //            Permission::findOrCreate($permission);
        //            $Admin->givePermissionTo($permission);
        //        }
        //
        //        foreach (config('permission.trainer') as $permission) {
        //            Permission::findOrCreate($permission);
        //            $Trainer->givePermissionTo($permission);
        //        }
    }
}
