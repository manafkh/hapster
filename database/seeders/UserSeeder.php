<?php

namespace Database\Seeders;

use App\Enums\UserRoleEnum;
use App\Models\Department;
use App\Models\User;
use App\Services\Keycloak\KeycloakService;
use Arr;
use Illuminate\Database\Seeder;
use Throwable;

class UserSeeder extends Seeder
{
    //    public function __construct(private KeycloakService $keycloakService)
    //    {
    //    }
    //
    //    public function run()
    //    {
    //
    //        $admins = $this->AdminsArray();
    //
    //
    //        $this->deleteUserInKeycloak();
    //
    //        try {
    //            foreach ($admins as $adminData) {
    //
    //                $admin = User::firstOrCreate(
    //                    [
    //                        'username' => $adminData['username'],
    //                    ],
    //                    Arr::except($adminData, ['role', 'sites', 'password'])
    //                );
    //
    //                $admin->addMediaFromUrl('https://i.pravatar.cc/150')
    //                    ->toMediaCollection('avatar');
    //
    //                $admin->syncRoles($adminData['role']);
    //                $admin->email_verified_at = '2023-09-02';
    //                $admin->save();
    //                $admin->sites()->sync($adminData['sites']);
    //                $depratment_ids = $admin->departmentsBelongsToUserSites->pluck('id')->toArray();
    //                $admin->departments()->sync($depratment_ids);
    //                $this->keycloakService->addUserToKeycloak(
    //                    username: $adminData['username'],
    //                    email: $adminData['email'],
    //                    password: $adminData['password'],
    //                );
    //
    //                if ($adminData['role'] == UserRoleEnum::Trainer->value) {
    //                    $admin->departments()->sync(Department::where('site_id', 2)->pluck('id')->toArray());
    //                }
    //            }
    //        } catch (Throwable $e) {
    //            $this->keycloakService
    //                ->deleteAllUsersWithoutAdmin();
    //            throw $e;
    //        }
    //    }
    //
    //    private function AdminsArray()
    //    {
    //        $password = '0123456AAzz@0123456';
    //
    //        return [
    //
    //            [
    //                'first_name' => 'Oscar',
    //                'last_name' => 'Eleanor',
    //                'username' => 'oscar',
    //                'phone_number' => '74445550',
    //                'email' => 'luciehapster.p@gmail.com',
    //                'password' => $password,
    //                'sites' => [1],
    //                'role' => UserRoleEnum::Admin->value,
    //            ],
    //
    //            [
    //                'first_name' => 'James',
    //                'last_name' => 'Graham',
    //                'username' => 'james',
    //                'phone_number' => '74445551',
    //                'email' => 'angehapster.b@gmail.com',
    //                'password' => $password,
    //                'sites' => [1],
    //                'role' => UserRoleEnum::Capturer->value,
    //
    //            ],
    //
    //            [
    //                'first_name' => 'Ellis',
    //                'last_name' => 'Audrey',
    //                'username' => 'ellis',
    //                'phone_number' => '74445552',
    //                'email' => 'sachahapster.b@gmail.com',
    //                'password' => $password,
    //                'sites' => [1],
    //                'role' => UserRoleEnum::Trainer->value,
    //            ],
    //            [
    //                'first_name' => 'Dany',
    //                'last_name' => 'Audrey',
    //                'username' => 'dany',
    //                'phone_number' => '744445552',
    //                'email' => 'manafalkhteeb.b@gmail.com',
    //                'password' => $password,
    //                'sites' => [1],
    //                'role' => UserRoleEnum::Trainer->value,
    //            ],
    //        ];
    //    }
    //
    //
    //    public function deleteUserInKeycloak()
    //    {
    //
    //        $userEmails = array_column($this->AdminsArray(), 'email');
    ////        $userEmails = User::all()->pluck('username');
    //
    //        foreach ($userEmails as $userEmail) {
    //            $this->keycloakService->deleteUserByEmail($userEmail);
    //        }
    //    }
}
