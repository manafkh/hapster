<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Department;
use App\Models\License;
use App\Models\Site;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
class SiteCompanyDepartmentSeeder extends Seeder
{
    public function run()
    {
        if (! Company::exists()) {
            $company1 = Company::create(['name' => 'Acme Corp']);
            $company2 = Company::create(['name' => 'Tech Solutions Ltd']);
        }
        if (! Site::exists()) {
            $site1 = Site::create(['name' => 'Acme HQ', 'company_id' => $company1->id]);
            $site2 = Site::create(['name' => 'Acme Branch 1', 'company_id' => $company1->id]);
            Site::create(['name' => 'Tech Solutions Office', 'company_id' => $company2->id]);

        }
        if (! Department::exists()) {
            $department = Department::create([
                'name' => 'HR',
                'description' => 'HR Department',
                'sort_order' => 1,
                'site_id' => $site1->id,
            ]);
            Department::create([
                'name' => 'QA',
                'description' => 'QA Department',

                'sort_order' => 2,
                'site_id' => $site1->id,
                'parent_id' => $department->id,
            ]);
            Department::create([
                'name' => 'IT',
                'description' => 'IT Department',
                'sort_order' => 3,
                'site_id' => $site2->id,
            ]);

        }
        if (! License::exists()) {
            $siteHQ  = Site::query()->where('name','Acme HQ')->first();
            $siteHQ->licenses()->create([
                'start_date'=>Carbon::now()->subDay(),
                'end_date'=>Carbon::now()->addYear(),
            ]);
            $siteLtd  = Site::query()->where('name','Acme Branch 1')->first();
            $siteLtd->licenses()->create([
                'start_date'=>Carbon::now()->subYear(),
                'end_date'=>Carbon::now(),
            ]);
            $siteTech  = Site::query()->where('name','Tech Solutions Office')->first();
            $siteTech->licenses()->create([
                'start_date'=>Carbon::now()->subYear(),
                'end_date'=>Carbon::now(),
            ]);

        }
    }
}
