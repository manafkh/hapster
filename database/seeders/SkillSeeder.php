<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Keypoint;
use App\Models\Site;
use App\Models\Skill;
use App\Models\SkillTranslation;
use App\Models\Step;
use App\Models\User;
use Illuminate\Database\Seeder;

class SkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $industrialSkillsData = $this->skillData();


        foreach ($industrialSkillsData as $skillData) {
            if (! $skill = Skill::whereTranslation('name', $skillData['name'])->first()) {
                $skill = Skill::factory()->create([
                    'user_id'=>User::where('username','=','oscar')->first()->id,
                    'name' => $skillData['name'],
                ]);
            }

            $skill->departments()->sync(Department::all()->pluck('id')->toArray());
            $skill->sites()->sync(Site::all()->pluck('id')->toArray());

            $skill->addMediaFromUrl($skillData['image'])
                ->toMediaCollection('image');

            $skill->addMediaFromUrl($skillData['methods_and_documentation'])
                ->toMediaCollection('methods_and_documentation');

            foreach ($skillData['steps'] as $step) {
                $step = Step::factory()->create([
                    'name' => $step['name'],
                    'sort_order' => $step['sort_order'],
                    'skill_id' => $skill->id,
                ]);

                foreach ($step['keypoints'] as $keypoint) {
                    $newKeypoint = Keypoint::factory()->create([
                        'step_id' => $step->id,
                        'name' => $keypoint['name'],
                    ]);

                    $newKeypoint->addMedia($keypoint['keypoint_media'])
                        ->preservingOriginal()
                        ->toMediaCollection('keypoint_media');
                }
            }
        }

    }

    private function skillData()
    {

         return [
            [
                'name' => 'Steps For Welding Metal Structures',
                'image' => 'https://www.lincolntech.edu/sites/default/files/styles/960x_webp_/public/2019-10/types-of-welding-header-2019.jpg.webp?itok=5QArUNAZ',
                'methods_and_documentation' => 'https://metals.ovako.com/494a37/globalassets/downloads/tools---metal/welding_guide.pdf',
                'steps' => [
                    [
                        'name' => 'Preparation of welding equipment',
                        'sort_order' => 1,
                        'keypoints' => [
                            [
                                'name' => 'Choose the right welding rod or wire',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/images.jpeg',
                            ],
                            [
                                'name' => 'Set up the welding machine according to the metal type',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/stick-welding-machine-parts.jpg',
                            ],

                        ],
                    ],
                    [
                        'name' => 'Welding technique and safety',
                        'sort_order' => 2,
                        'keypoints' => [
                            [
                                'name' => 'Always wear protective gear',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/construction-worker.jpg',
                            ],
                            [
                                'name' => 'Ensure proper ventilation in the welding area',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/Ventilation-in-the-workplace.mp4',
                            ],

                        ],
                    ],

                ],
            ],
            [
                'name' => 'Mastering Industrial Packaging Engineering',
                'image' => 'https://www.shutterstock.com/shutterstock/photos/2335777443/display_1500/stock-photo-packaging-machine-packaging-system-packaging-engineer-polyethylene-fabric-engineer-and-machines-2335777443.jpg',
                'methods_and_documentation' => 'https://metals.ovako.com/494a37/globalassets/downloads/tools---metal/welding_guide.pdf',
                'steps' => [
                    [
                        'name' => 'Preparation of welding equipment',
                        'sort_order' => 1,
                        'keypoints' => [
                            [
                                'name' => 'Choose the right welding rod or wire',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/images.jpeg',
                            ],
                            [
                                'name' => 'Set up the welding machine according to the metal type',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/stick-welding-machine-parts.jpg',
                            ],
                        ],

                    ],
                    [
                        'name' => 'Welding technique and safety',
                        'sort_order' => 1,
                        'keypoints' => [
                            [
                                'name' => 'Always wear protective gear',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/construction-worker.jpg',
                            ],
                            [
                                'name' => 'Ensure proper ventilation in the welding area',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/Ventilation-in-the-workplace.mp4',
                            ],

                        ],
                    ],
                ],
            ],
            [
                'name' => 'Guide on Automating with Robotics',
                'image' => 'https://howtorobot.com/sites/default/files/inline-images/shutterstock_1908818611.jpg',
                'methods_and_documentation' => 'https://metals.ovako.com/494a37/globalassets/downloads/tools---metal/welding_guide.pdf',
                'steps' => [
                    [
                        'name' => 'Preparation of welding equipment',
                        'sort_order' => 1,
                        'keypoints' => [

                            [
                                'name' => 'Choose the right welding rod or wire',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/images.jpeg',
                            ],
                            [
                                'name' => 'Set up the welding machine according to the metal type',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/stick-welding-machine-parts.jpg',
                            ],
                        ],

                    ],
                    [
                        'name' => 'Welding technique and safety',
                        'sort_order' => 1,
                        'keypoints' => [
                            [
                                'name' => 'Always wear protective gear',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/construction-worker.jpg',
                            ],
                            [
                                'name' => 'Ensure proper ventilation in the welding area',
                                'reason' => 'Choosing the right welding rod or wire is vital for ensuring compatibility with the base material and achieving high-quality, strong welds.',
                                'keypoint_media' => 'public/fake_media/skills/Ventilation-in-the-workplace.mp4',
                            ],
                        ],

                    ],
                ],
            ],
        ];


    }
}
