<?php

namespace Database\Seeders;

use App\Models\Department;
use App\Models\Requests;
use App\Models\User;
use Illuminate\Database\Seeder;

class AccessSkillToTrainerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if (! Requests::exists()) {
            $ellisTrainer = User::where('username', 'ellis')->first();
            $danyTrainer = User::where('username', 'dany')->first();
            $ellisSkills = Department::where('id', 2)->with('skills')->first()->skills;
            $danySkills = Department::where('id', 3)->with('skills')->first()->skills;
            foreach ($ellisSkills as $skill) {
                Requests::create([
                    'user_id' => $ellisTrainer->id,
                    'skill_id' => $skill->id,
                    'access_type' => 1,
                    'is_access' => true,
                ]);
            }
            foreach ($danySkills as $skill) {
                Requests::create([
                    'user_id' => $danyTrainer->id,
                    'skill_id' => $skill->id,
                    'access_type' => 1,
                    'is_access' => true,
                ]);
            }

        }
    }
}
