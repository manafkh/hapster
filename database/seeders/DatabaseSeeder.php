<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(RolePremissionSeeder::class);
        $this->call(SiteCompanyDepartmentSeeder::class);
//        $this->call(UserSeeder::class);
//        $this->call(SkillSeeder::class);
        $this->call(StageSeeder::class);
//        $this->call(TraineeSeeder::class);
//        $this->call(AccessSkillToTrainerSeeder::class);
    }
}
