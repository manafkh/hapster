<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('companies', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('sites', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('departments', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('requests', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('skills', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('steps', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('keypoints', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('stages', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('trainees', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('trainings', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('companies', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('sites', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('departments', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('requests', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('skills', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('steps', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('keypoints', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('stages', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('trainees', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('trainings', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
};
