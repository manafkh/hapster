<?php

use App\Models\Step;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('step_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->index();

            $table->string('name');
            $table->string('audio')->nullable();
            $table->foreignIdFor(Step::class)->constrained()->cascadeOnDelete();
            $table->unique(['step_id', 'locale']);

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('step_translations');
    }
};
