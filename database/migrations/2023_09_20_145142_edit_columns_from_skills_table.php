<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('skills', function (Blueprint $table) {
            $table->string('importance')->nullable()->change();
            $table->string('required_skill')->nullable()->change();
            $table->string('required_safety_equipments')->nullable()->change();
            $table->string('required_materials_parts')->nullable()->change();
            $table->string('tools_supplies_machinery')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('skills', function (Blueprint $table) {
            $table->string('importance');
            $table->string('required_skill');
            $table->string('required_safety_equipments');
            $table->string('required_materials_parts');
            $table->string('tools_supplies_machinery');
        });
    }
};
