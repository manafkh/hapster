
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('registries', function (Blueprint $table) {
            $table->id();
            $table->ipAddress()->nullable();
            $table->string('ip_range')->nullable();
            $table->macAddress()->nullable();
            $table->ipAddress('required_ip')->nullable();
            $table->string('token', 50)->nullable()->unique();
            $table->timestamp('token_expired_at')->nullable();
            $table->string('refresh_token', 50)->unique()->nullable();
            $table->timestamp('refresh_token_expired_at')->nullable();
            $table->string('node_id')->nullable();
            $table->string('service_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('registries');
    }
};
