<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('keypoint_translations', function (Blueprint $table) {
            $table->id();
            $table->string('locale')->index();

            $table->string('name');
            $table->string('name_audio')->nullable();
            $table->string('reason');
            $table->string('reason_audio')->nullable();

            $table->foreignIdFor(\App\Models\Keypoint::class)->constrained()->cascadeOnDelete();
            $table->unique(['keypoint_id', 'locale']);

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('keypoint_translations');
    }
};
