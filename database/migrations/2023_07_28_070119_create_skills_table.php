<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('skills', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->cascadeOnDelete();
            $table->string('name');
            $table->tinyInteger('type'); // SkillTypeEnum
            $table->tinyInteger('confidentiality_level');
            $table->date('writing_date');
            $table->string('author');
            $table->string('location');
            $table->string('importance');
            $table->string('required_skill');
            $table->string('required_safety_equipments');
            $table->string('required_materials_parts');
            $table->string('tools_supplies_machinery');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('skills');
    }
};
